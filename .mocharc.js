module.exports = {
  diff: true,
  extension: ['ts'],
  opts: false,
  package: './package.json',
  reporter: 'spec',
  slow: 75,
  timeout: 3000,
  sort: true,
  require: [
    'ts-node/register',
    'tsconfig-paths/register',
    'configuration/mocha.setup.js',
  ],
  spec: ['src/**/1-tests/**/*.test.ts'],
  ui: 'bdd',
  exit: true,
}
