import { ReadyStatus } from './ready-status';

export class ReadyStatusUp extends ReadyStatus {
  constructor() {
    super(ReadyStatus.UP);
  }

  get isUp(): boolean {
    return true;
  }
}
