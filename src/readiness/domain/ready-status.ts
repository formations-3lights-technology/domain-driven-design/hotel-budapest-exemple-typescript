enum ReadyStatuses {
  UP = 'up',
  DOWN = 'down'
}

export abstract class ReadyStatus {
  static readonly UP = ReadyStatuses.UP;
  static readonly DOWN = ReadyStatuses.DOWN;

  protected constructor(protected _status: string) {}

  static get statuses(): string[] {
    return [ReadyStatuses.UP, ReadyStatuses.DOWN];
  }

  get status(): string {
    return this._status;
  }

  abstract get isUp(): boolean;
}
