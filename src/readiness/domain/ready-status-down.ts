import { ReadyStatus } from './ready-status';

export class ReadyStatusDown extends ReadyStatus {
  constructor() {
    super(ReadyStatus.DOWN);
  }

  get isUp(): boolean {
    return false;
  }
}
