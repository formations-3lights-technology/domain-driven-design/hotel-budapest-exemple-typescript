import packageJson from '../../../package.json';
import { ReadyStatus } from './ready-status';

export type ReadyGlobalAllStatus = Readonly<{
  apiVersion: string;
  apiStatus: string;
  databaseStatus: string;
}>;

export class ReadyGlobalStatus {
  constructor(private readonly databaseStatus: ReadyStatus) {}

  get status(): ReadyGlobalAllStatus {
    return {
      apiVersion: packageJson.version,
      apiStatus: ReadyStatus.UP,
      databaseStatus: this.databaseStatus.status
    };
  }

  get isUp(): boolean {
    return this.databaseStatus.isUp;
  }
}
