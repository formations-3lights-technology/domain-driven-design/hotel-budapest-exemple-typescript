import { ReadyStatus } from './ready-status';

export interface ReadyChecker {
  ready(): Promise<ReadyStatus>;
}
