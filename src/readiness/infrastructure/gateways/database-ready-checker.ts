import { ReadyChecker } from '@/readiness/domain/Ready-checker';
import { ReadyStatus } from '@/readiness/domain/ready-status';
import { ReadyStatusDown } from '@/readiness/domain/ready-status-down';
import { ReadyStatusUp } from '@/readiness/domain/ready-status-up';
import { TypeORMClient } from '@/shared/infrastructure/gateways/db-client/type-orm/type-orm-client';

export class DatabaseReadyChecker implements ReadyChecker {
  constructor(private readonly typeORMClient: TypeORMClient) {}

  async ready(): Promise<ReadyStatus> {
    return this.typeORMClient
      .executeQuery((db) => db.manager.query('select 1+1 as status'))
      .then((_) => new ReadyStatusUp())
      .catch((_) => new ReadyStatusDown());
  }
}
