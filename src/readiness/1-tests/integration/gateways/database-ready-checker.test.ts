import { expect, fakeDependencies } from '@/1-tests/utils';
import { ReadyStatusDown } from '@/readiness/domain/ready-status-down';
import { ReadyStatusUp } from '@/readiness/domain/ready-status-up';
import { DatabaseReadyChecker } from '@/readiness/infrastructure/gateways/database-ready-checker';
import { TypeORMClient } from '@/shared/infrastructure/gateways/db-client/type-orm/type-orm-client';
import { typeORMDbClient } from '@/shared/infrastructure/gateways/db-client/type-orm/type-orm-db-client';
import { NodeEnvironmentVariables } from '@/shared/infrastructure/gateways/node-environment-variables';

describe('Integration | Readiness | Database Ready checker', () => {
  it('Database est ok', async () => {
    // GIVEN
    const environmentVariables = new NodeEnvironmentVariables();
    const dbClient = typeORMDbClient(environmentVariables);
    const typeORMClient = new TypeORMClient(
      dbClient,
      fakeDependencies().apiLogger,
      fakeDependencies().contexteService,
      environmentVariables
    );

    // WHEN
    const result = await new DatabaseReadyChecker(typeORMClient).ready();

    // THEN
    expect(result).to.eql(new ReadyStatusUp());
  });

  it('Database est ko', async () => {
    // given
    const environmentVariables = {
      ...Object.assign({}, new NodeEnvironmentVariables()),
      APPLICATION_REPLICA: 1,
      DATABASE_HOST: 'fake',
      isProduction: false
    };
    const dbClient = typeORMDbClient(environmentVariables);
    const typeORMClient = new TypeORMClient(
      dbClient,
      fakeDependencies().apiLogger,
      fakeDependencies().contexteService,
      environmentVariables
    );

    // WHEN
    const result = await new DatabaseReadyChecker(typeORMClient).ready();

    // THEN
    expect(result).to.eql(new ReadyStatusDown());
  });
});
