import { expect } from '@/1-tests/utils';
import { ReadyChecker } from '@/readiness/domain/Ready-checker';
import { ReadyStatusDown } from '@/readiness/domain/ready-status-down';
import { ReadyStatusUp } from '@/readiness/domain/ready-status-up';
import { ReadyCheck } from '@/readiness/use-cases/ready-check';
import { StubbedType, stubInterface } from '@salesforce/ts-sinon';
import sinon from 'sinon';

describe('Unit | Readiness | Ready check', () => {
  let readyCheck: ReadyCheck;
  let databaseReadyChecker: StubbedType<ReadyChecker>;

  beforeEach(() => {
    databaseReadyChecker = stubInterface<ReadyChecker>(sinon);
    databaseReadyChecker.ready.resolves(new ReadyStatusUp());

    readyCheck = new ReadyCheck(databaseReadyChecker);
  });

  describe('Tout est ok', () => {
    it('api est ok', async () => {
      // WHEN
      const result = await readyCheck.handle();

      // THEN
      expect(result.isUp).to.be.true();
    });
  });

  describe('Au moins une dépendance est ko', async () => {
    describe('Database est ko', () => {
      it('api est ko', async () => {
        // GIVEN
        databaseReadyChecker.ready.resolves(new ReadyStatusDown());

        // WHEN
        const result = await readyCheck.handle();

        // THEN
        expect(databaseReadyChecker.ready).to.have.been.called();
        expect(result.isUp).to.be.false();
      });
    });
  });
});
