import { fakeEventPublisher } from '@/1-tests/utils';
import { ReadinessDependenciesContainer } from '@/readiness/2-configuration/root-readiness-dependencies-container';
import { CommandDispatcher } from '@/shared/command-dispatcher/internal-command-dispatcher';
import { stubInterface } from '@salesforce/ts-sinon';
import sinon from 'sinon';

export const readinessFakeDependencies = (): ReadinessDependenciesContainer => ({
  commandDispatcher: stubInterface<CommandDispatcher>(sinon),
  eventPublisher: fakeEventPublisher(),
  queries: {
    // @ts-ignore
    readyCheck: {
      handle: sinon.stub()
    }
  }
});
