import { getReadyCheck } from '@/readiness/infrastructure/endpoints/get-ready-check-endpoint';
import { Router } from 'express';
import asyncHandler from 'express-async-handler';
import { ReadinessDependenciesContainer } from './root-readiness-dependencies-container';

export interface RootEndpoints {
  unprotected: (router: Router, dependencies: ReadinessDependenciesContainer) => void;
}

export const rootReadinessEndpoints: RootEndpoints = {
  unprotected: (router: Router, { queries }: ReadinessDependenciesContainer): void => {
    router.get('/ready', asyncHandler(getReadyCheck(queries.readyCheck)));
  }
};
