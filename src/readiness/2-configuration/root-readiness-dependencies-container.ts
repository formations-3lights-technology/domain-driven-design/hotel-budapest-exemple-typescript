import { DependenciesContainerRead } from '@/2-configuration/dependencies-container';
import { DatabaseReadyChecker } from '@/readiness/infrastructure/gateways/database-ready-checker';
import { ReadyCheck } from '@/readiness/use-cases/ready-check';
import { TypeORMClient } from '@/shared/infrastructure/gateways/db-client/type-orm/type-orm-client';

export type ReadinessDependenciesContainer = DependenciesContainerRead<{
  readyCheck: ReadyCheck;
}>;

export const rootReadinessDependenciesContainer = (typeORMClient: TypeORMClient): ReadinessDependenciesContainer => {
  const databaseReadyChecker = new DatabaseReadyChecker(typeORMClient);

  return {
    queries: {
      readyCheck: new ReadyCheck(databaseReadyChecker)
    }
  };
};
