import { ReadyChecker } from '@/readiness/domain/Ready-checker';
import { ReadyGlobalStatus } from '@/readiness/domain/ready-global-status';

export class ReadyCheck {
  constructor(private readonly databaseReadyChecker: ReadyChecker) {}

  async handle(): Promise<ReadyGlobalStatus> {
    const [databaseStatus] = await Promise.all([this.databaseReadyChecker.ready()]);

    return new ReadyGlobalStatus(databaseStatus);
  }
}
