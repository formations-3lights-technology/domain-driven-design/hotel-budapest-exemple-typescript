import { defaultConfiguration } from '@/chambre-reservation/2-configuration/default-configuration';
import { ReservationSnapshot } from '@/chambre-reservation/domain/reservation/reservation';
import { ChambreTypeOrmEntity } from '@/chambre-reservation/infrastructure/gateways/chambre-type-orm-entity';
import { ApiCrypt } from '@/shared/domain/gateways/Api-crypt';
import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn } from 'typeorm';

@Entity({
  schema: defaultConfiguration.db.schema,
  name: defaultConfiguration.db.reservation
})
export class ReservationTypeOrmEntity {
  @PrimaryColumn()
  readonly id: string;

  @OneToOne(() => ChambreTypeOrmEntity, { orphanedRowAction: 'delete' })
  @JoinColumn({ name: 'numero_de_la_chambre', referencedColumnName: 'numero' })
  readonly chambre: ChambreTypeOrmEntity;

  @Column('varchar', { name: 'numero_de_la_chambre' })
  readonly numeroDeLaChambre: string;

  @Column('varchar', { name: 'mail_de_reservation' })
  readonly mailDeReservation: string;

  @Column('integer', { name: 'nombre_de_personnes' })
  readonly nombreDePersonnes: number;

  @Column('integer', { name: 'prix_par_nuit' })
  readonly prixParNuit: number;

  @Column('timestamp with time zone', { name: 'date_de_debut' })
  readonly dateDeDebut: Date;

  @Column('timestamp with time zone', { name: 'date_de_fin' })
  readonly dateDeFin: Date;

  constructor(
    private readonly _id: string,
    private readonly _numeroDeLaChambre: string,
    private readonly _mailDeReservation: string,
    private readonly _nombreDePersonnes: number,
    private readonly _prixParNuit: number,
    private readonly _dateDeDebut: Date,
    private readonly _dateDeFin: Date
  ) {
    this.id = this._id;
    this.numeroDeLaChambre = this._numeroDeLaChambre;
    this.mailDeReservation = this._mailDeReservation;
    this.nombreDePersonnes = this._nombreDePersonnes;
    this.prixParNuit = this._prixParNuit;
    this.dateDeDebut = this._dateDeDebut;
    this.dateDeFin = this._dateDeFin;
  }

  static mapDepuisSnapshot(snapshot: ReservationSnapshot, apiCrypt: ApiCrypt): ReservationTypeOrmEntity {
    return new ReservationTypeOrmEntity(
      snapshot.id,
      snapshot.numeroDeLaChambre,
      apiCrypt.encrypt(snapshot.mailDeReservation),
      snapshot.nombreDePersonnes,
      snapshot.prixParNuit,
      snapshot.dateDeDebut,
      snapshot.dateDeFin
    );
  }

  mapVersSnapshot(apiCrypt: ApiCrypt): ReservationSnapshot {
    return {
      id: this.id,
      numeroDeLaChambre: this.numeroDeLaChambre,
      mailDeReservation: apiCrypt.decrypt(this.mailDeReservation),
      nombreDePersonnes: this.nombreDePersonnes,
      prixParNuit: this.prixParNuit,
      dateDeDebut: this.dateDeDebut,
      dateDeFin: this.dateDeFin
    };
  }
}
