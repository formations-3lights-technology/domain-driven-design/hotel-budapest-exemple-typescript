import { Chambre } from '@/chambre-reservation/domain/chambre/chambre';
import { ChambreLoader } from '@/chambre-reservation/domain/chambre/Chambre-loader';
import { Erreur } from '@/chambre-reservation/domain/erreurs';
import { ChambreTypeOrmEntity } from '@/chambre-reservation/infrastructure/gateways/chambre-type-orm-entity';
import { ApiCrypt } from '@/shared/domain/gateways/Api-crypt';
import { TypeORMClient } from '@/shared/infrastructure/gateways/db-client/type-orm/type-orm-client';
import { areIntervalsOverlapping, startOfDay } from 'date-fns';
import { MoreThanOrEqual } from 'typeorm';

export class PostgresChambreLoader implements ChambreLoader {
  constructor(private readonly typeORMClient: TypeORMClient, private readonly apiCrypt: ApiCrypt) {}

  async getParNumeroOuErreur(numero: string): Promise<Chambre> {
    const result = await this.typeORMClient.executeQuery((q) =>
      q.getRepository(ChambreTypeOrmEntity).findOne({ where: { numero } })
    );

    if (!result) {
      throw new Erreur.Chambre.NonTrouvee();
    }

    return result.mapVersSnapshot(this.apiCrypt);
  }

  async getPar(capacite: number, intervalle: { dateDeDebut: Date; dateDeFin: Date }): Promise<Chambre[]> {
    return await this.typeORMClient.executeQuery(async (q) => {
      const chambres = await q.getRepository(ChambreTypeOrmEntity).find({
        where: { capacite: MoreThanOrEqual(capacite) },
        order: { numero: 'ASC' }
      });

      return chambres
        .map((c) => c.mapVersSnapshot(this.apiCrypt))
        .filter(
          (c) =>
            !c.reservations.some((r) =>
              areIntervalsOverlapping(
                { start: startOfDay(r.dateDeDebut), end: startOfDay(r.dateDeFin) },
                { start: startOfDay(intervalle.dateDeDebut), end: startOfDay(intervalle.dateDeFin) }
              )
            )
        );
    });
  }
}
