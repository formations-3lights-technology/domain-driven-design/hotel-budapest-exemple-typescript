import { Erreur } from '@/chambre-reservation/domain/erreurs';
import { ReservationSnapshot } from '@/chambre-reservation/domain/reservation/reservation';
import { ReservationRepository } from '@/chambre-reservation/domain/reservation/Reservation-repository';
import { ReservationTypeOrmEntity } from '@/chambre-reservation/infrastructure/gateways/reservation-type-orm-entity';
import { ApiCrypt } from '@/shared/domain/gateways/Api-crypt';
import { IdentityGenerator } from '@/shared/domain/gateways/Identity-generator';
import { TypeORMClient } from '@/shared/infrastructure/gateways/db-client/type-orm/type-orm-client';

export class PostgresReservationRepository implements ReservationRepository {
  constructor(
    private readonly typeORMClient: TypeORMClient,
    private readonly identityGenerator: IdentityGenerator,
    private readonly apiCrypt: ApiCrypt
  ) {}

  async nextId(): Promise<string> {
    return this.identityGenerator.generate();
  }

  async save(reservation: ReservationSnapshot): Promise<void> {
    await this.typeORMClient.executeTransaction((q) =>
      q
        .getRepository(ReservationTypeOrmEntity)
        .save(ReservationTypeOrmEntity.mapDepuisSnapshot(reservation, this.apiCrypt))
    );
  }

  async getParIdOuErreur(id: string): Promise<ReservationSnapshot> {
    try {
      const reservation = await this.typeORMClient.executeQuery((q) =>
        q.getRepository(ReservationTypeOrmEntity).findOneByOrFail({ id })
      );
      return reservation.mapVersSnapshot(this.apiCrypt);
    } catch (_) {
      throw new Erreur.Reservation.NonTrouvee();
    }
  }
}
