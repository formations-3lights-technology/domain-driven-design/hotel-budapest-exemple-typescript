import { defaultConfiguration } from '@/chambre-reservation/2-configuration/default-configuration';
import { Chambre } from '@/chambre-reservation/domain/chambre/chambre';
import { ReservationTypeOrmEntity } from '@/chambre-reservation/infrastructure/gateways/reservation-type-orm-entity';
import { ApiCrypt } from '@/shared/domain/gateways/Api-crypt';
import { Column, Entity, OneToMany, PrimaryColumn } from 'typeorm';

@Entity({
  schema: defaultConfiguration.db.schema,
  name: defaultConfiguration.db.chambre
})
export class ChambreTypeOrmEntity {
  @PrimaryColumn()
  readonly numero: string;

  @Column('varchar')
  readonly nom: string;

  @Column('integer')
  readonly capacite: number;

  @Column('integer', { name: 'prix_par_nuit' })
  readonly prixParNuit: number;

  @OneToMany(() => ReservationTypeOrmEntity, (r) => r.chambre, {
    cascade: false,
    eager: true
  })
  readonly reservations: ReservationTypeOrmEntity[];

  constructor(
    private readonly _numero: string,
    private readonly _nom: string,
    private readonly _capacite: number,
    private readonly _prixParNuit: number,
    private readonly _reservations: ReservationTypeOrmEntity[]
  ) {
    this.numero = this._numero;
    this.nom = this._nom;
    this.capacite = this._capacite;
    this.prixParNuit = this._prixParNuit;
    this.reservations = this._reservations;
  }

  mapVersSnapshot(apiCrypt: ApiCrypt): Chambre {
    return {
      numero: this.numero,
      nom: this.nom,
      capacite: this.capacite,
      prixParNuit: this.prixParNuit,
      reservations: this.reservations.map((r) => r.mapVersSnapshot(apiCrypt))
    };
  }
}
