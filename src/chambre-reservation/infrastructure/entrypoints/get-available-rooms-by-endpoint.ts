import { Chambre } from '@/chambre-reservation/domain/chambre/chambre';
import { GetChambresDisponiblesPar } from '@/chambre-reservation/use-cases/get-chambres-disponibles-par';
import { Request, RequestHandler, Response } from 'express';
import { checkSchema, ValidationChain } from 'express-validator';

export const getAvailableRoomsByValidator = (): ValidationChain[] => {
  return checkSchema({
    capacity: { in: 'params', isNumeric: true, errorMessage: 'is required' },
    startDate: { in: 'params', isDate: true, errorMessage: 'is required' },
    endDate: { in: 'params', isDate: true, errorMessage: 'is required' }
  });
};

export const getAvailableRoomsBy = (query: GetChambresDisponiblesPar): RequestHandler => {
  return async (request: Request, response: Response) => {
    const { capacity, startDate, endDate } = request.params;

    const result = await query.handle(+capacity, new Date(startDate), new Date(endDate));

    response.status(200).send({ data: result.map(format) });
  };
};

type ViewModel = Readonly<{
  number: string;
  name: string;
  capacity: number;
  priceByNight: number;
}>;

const format = (viewModel: Chambre): ViewModel => ({
  number: viewModel.numero,
  name: viewModel.nom,
  capacity: viewModel.capacite,
  priceByNight: viewModel.prixParNuit
});
