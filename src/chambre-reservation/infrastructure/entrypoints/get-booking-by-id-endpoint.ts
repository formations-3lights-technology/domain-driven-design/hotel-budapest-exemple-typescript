import { ReservationSnapshot } from '@/chambre-reservation/domain/reservation/reservation';
import { GetReservationParId } from '@/chambre-reservation/use-cases/get-reservation-par-id';
import { Request, RequestHandler, Response } from 'express';
import { checkSchema, ValidationChain } from 'express-validator';

export const getBookingByIdValidator = (): ValidationChain[] => {
  return checkSchema({
    id: { in: 'params', isString: true, errorMessage: 'is required' }
  });
};

export const getBookingById = (query: GetReservationParId): RequestHandler => {
  return async (request: Request, response: Response) => {
    const { id } = request.params;

    const result = await query.handle(id);

    response.status(200).send({ data: format(result) });
  };
};

type ViewModel = Readonly<{
  id: string;
  bookingEmail: string;
  roomNumber: string;
  totalPerson: number;
  priceByNight: number;
  startDate: string;
  endDate: string;
}>;

const format = (viewModel: ReservationSnapshot): ViewModel => ({
  id: viewModel.id,
  bookingEmail: viewModel.mailDeReservation,
  roomNumber: viewModel.numeroDeLaChambre,
  totalPerson: viewModel.nombreDePersonnes,
  priceByNight: viewModel.prixParNuit,
  startDate: viewModel.dateDeDebut.toISOString(),
  endDate: viewModel.dateDeFin.toISOString()
});
