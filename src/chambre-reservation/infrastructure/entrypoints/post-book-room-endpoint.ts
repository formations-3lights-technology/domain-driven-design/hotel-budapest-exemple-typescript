import { ReserverUneChambreCommand } from '@/chambre-reservation/use-cases/reserver-une-chambre/reserver-une-chambre-command';
import { CommandDispatcher } from '@/shared/command-dispatcher/internal-command-dispatcher';
import { Request, RequestHandler, Response } from 'express';
import { checkSchema, ValidationChain } from 'express-validator';

export const postBookRoomValidator = (): ValidationChain[] => {
  return checkSchema({
    bookingEmail: { in: 'body', isEmail: true, notEmpty: true, errorMessage: 'is required' },
    roomNumber: { in: 'body', isString: true, notEmpty: true, errorMessage: 'is required' },
    totalPerson: { in: 'body', isNumeric: true, notEmpty: true, errorMessage: 'is required' },
    startDate: { in: 'body', isDate: true, notEmpty: true, errorMessage: 'is required' },
    endDate: { in: 'body', isDate: true, notEmpty: true, errorMessage: 'is required' }
  });
};

export const postBookRoom = (commandDispatcher: CommandDispatcher): RequestHandler => {
  return async (request: Request, response: Response) => {
    const { bookingEmail, roomNumber, totalPerson, startDate, endDate } = request.body;

    await commandDispatcher.dispatch(
      new ReserverUneChambreCommand(bookingEmail, roomNumber, +totalPerson, new Date(startDate), new Date(endDate))
    );

    response.status(201).send();
  };
};
