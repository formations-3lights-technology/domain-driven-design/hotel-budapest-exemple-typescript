import { Chambre } from '@/chambre-reservation/domain/chambre/chambre';
import { ChambreLoader } from '@/chambre-reservation/domain/chambre/Chambre-loader';

export class GetChambresDisponiblesPar {
  constructor(private readonly chambreLoader: ChambreLoader) {}

  handle(capacite: number, dateDeDebut: Date, dateDeFin: Date): Promise<Chambre[]> {
    return this.chambreLoader.getPar(capacite, { dateDeDebut, dateDeFin });
  }
}
