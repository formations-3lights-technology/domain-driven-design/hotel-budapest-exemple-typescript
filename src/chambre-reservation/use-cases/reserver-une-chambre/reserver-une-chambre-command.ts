import { defaultConfiguration } from '@/chambre-reservation/2-configuration/default-configuration';
import { Command } from '@/shared/command-dispatcher/Command';

type CommandSerialized = Readonly<{
  bookingEmail: string;
  roomNumber: string;
  totalPerson: number;
  startDate: string;
  endDate: string;
}>;

export class ReserverUneChambreCommand extends Command<CommandSerialized> {
  static readonly label = 'BookRoomCommand';
  protected readonly boundedContext = defaultConfiguration.boundedContextName;

  constructor(
    readonly mailDeReservation: string,
    readonly numeroDeLaChambre: string,
    readonly nombreDePersonnes: number,
    readonly dateDeDebut: Date,
    readonly dateDeFin: Date
  ) {
    super();
  }

  isPublic(): boolean {
    return false;
  }

  label(): string {
    return ReserverUneChambreCommand.label;
  }

  protected serializedData(): CommandSerialized {
    return {
      bookingEmail: this._apiCrypt.encrypt(this.mailDeReservation),
      roomNumber: this.numeroDeLaChambre,
      totalPerson: this.nombreDePersonnes,
      startDate: this.dateDeDebut.toISOString(),
      endDate: this.dateDeFin.toISOString()
    };
  }
}
