import { ChambreLoader } from '@/chambre-reservation/domain/chambre/Chambre-loader';
import { ReservationRepository } from '@/chambre-reservation/domain/reservation/Reservation-repository';
import { ReservationService } from '@/chambre-reservation/domain/reservation/reservation-service';
import { ReserverUneChambreCommand } from '@/chambre-reservation/use-cases/reserver-une-chambre/reserver-une-chambre-command';
import { CommandHandler } from '@/shared/command-dispatcher/Command-handler';
import { EventPublisher } from '@/shared/event-publisher/internal-event-publisher';

export class ReserverUneChambreCommandHandler implements CommandHandler {
  constructor(
    private readonly reservationRepository: ReservationRepository,
    private readonly chambreLoader: ChambreLoader,
    private readonly eventPublisher: EventPublisher
  ) {}

  async handle(command: ReserverUneChambreCommand): Promise<void> {
    const reservation = await new ReservationService(this.reservationRepository, this.chambreLoader).reserverUneChambre(
      command.mailDeReservation,
      command.numeroDeLaChambre,
      command.nombreDePersonnes,
      command.dateDeDebut,
      command.dateDeFin
    );

    await this.reservationRepository.save(reservation.snapshot);
    await this.eventPublisher.publish(reservation.raisedEvents);
  }
}
