import { ReservationSnapshot } from '@/chambre-reservation/domain/reservation/reservation';
import { ReservationRepository } from '@/chambre-reservation/domain/reservation/Reservation-repository';

export class GetReservationParId {
  constructor(private readonly reservationRepository: ReservationRepository) {}

  handle(id: string): Promise<ReservationSnapshot> {
    return this.reservationRepository.getParIdOuErreur(id);
  }
}
