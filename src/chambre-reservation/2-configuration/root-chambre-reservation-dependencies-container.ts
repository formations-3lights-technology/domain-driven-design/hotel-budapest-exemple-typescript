import { DependenciesContainer } from '@/2-configuration/dependencies-container';
import { PostgresChambreLoader } from '@/chambre-reservation/infrastructure/gateways/postgres-chambre-loader';
import { PostgresReservationRepository } from '@/chambre-reservation/infrastructure/gateways/postgres-reservation-repository';
import { GetChambresDisponiblesPar } from '@/chambre-reservation/use-cases/get-chambres-disponibles-par';
import { GetReservationParId } from '@/chambre-reservation/use-cases/get-reservation-par-id';
import { ReserverUneChambreCommand } from '@/chambre-reservation/use-cases/reserver-une-chambre/reserver-une-chambre-command';
import { ReserverUneChambreCommandHandler } from '@/chambre-reservation/use-cases/reserver-une-chambre/reserver-une-chambre-command-handler';
import { CommandDispatcherMiddleware } from '@/shared/command-dispatcher/Command-dispatcher-middleware';
import { InternalCommandDispatcher } from '@/shared/command-dispatcher/internal-command-dispatcher';
import { ApiCrypt } from '@/shared/domain/gateways/Api-crypt';
import { ApiLogger } from '@/shared/domain/gateways/Api-logger';
import { IdentityGenerator } from '@/shared/domain/gateways/Identity-generator';
import { TransactionPerformer } from '@/shared/domain/gateways/Transaction-performer';
import { EventPublisherMiddleware } from '@/shared/event-publisher/Event-publisher-middleware';
import { InternalEventPublisher } from '@/shared/event-publisher/internal-event-publisher';
import { TypeORMClient } from '@/shared/infrastructure/gateways/db-client/type-orm/type-orm-client';

export type ChambreReservationDependenciesContainer = DependenciesContainer<{
  getChambresDisponiblesPar: GetChambresDisponiblesPar;
  getReservationParId: GetReservationParId;
}>;

export const rootChambreReservationDependenciesContainer = (
  apiLogger: ApiLogger,
  apiCrypt: ApiCrypt,
  identityGenerator: IdentityGenerator,
  transactionPerformer: TransactionPerformer,
  middlewares: {
    commandDispatcher: CommandDispatcherMiddleware[];
    eventPublisher: EventPublisherMiddleware[];
  },
  typeORMClient: TypeORMClient
): ChambreReservationDependenciesContainer => {
  const reservationRepository = new PostgresReservationRepository(typeORMClient, identityGenerator, apiCrypt);
  const chambreLoader = new PostgresChambreLoader(typeORMClient, apiCrypt);

  const eventPublisher = new InternalEventPublisher(apiLogger).registerMiddleware(middlewares.eventPublisher);
  const commandDispatcher = new InternalCommandDispatcher(transactionPerformer)
    .registerMiddleware(middlewares.commandDispatcher)
    .registerHandlers({
      [ReserverUneChambreCommand.label]: new ReserverUneChambreCommandHandler(
        reservationRepository,
        chambreLoader,
        eventPublisher
      )
    });

  return {
    commandDispatcher,
    eventPublisher,
    queries: {
      getChambresDisponiblesPar: new GetChambresDisponiblesPar(chambreLoader),
      getReservationParId: new GetReservationParId(reservationRepository)
    }
  };
};
