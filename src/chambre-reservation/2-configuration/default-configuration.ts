export const defaultConfiguration = {
  boundedContextName: 'RoomBooking',
  aggregatesName: {
    reservation: 'Booking'
  },
  db: {
    schema: 'chambre_reservation',
    reservation: 'reservation',
    chambre: 'chambre'
  }
};
