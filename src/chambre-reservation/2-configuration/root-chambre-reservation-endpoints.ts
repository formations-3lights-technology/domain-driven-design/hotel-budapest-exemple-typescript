import { expressMiddlewares } from '@/2-configuration/express-middlewares';
import {
  getAvailableRoomsBy,
  getAvailableRoomsByValidator
} from '@/chambre-reservation/infrastructure/entrypoints/get-available-rooms-by-endpoint';
import {
  getBookingById,
  getBookingByIdValidator
} from '@/chambre-reservation/infrastructure/entrypoints/get-booking-by-id-endpoint';
import {
  postBookRoom,
  postBookRoomValidator
} from '@/chambre-reservation/infrastructure/entrypoints/post-book-room-endpoint';
import { Router } from 'express';
import asyncHandler from 'express-async-handler';
import { ChambreReservationDependenciesContainer } from './root-chambre-reservation-dependencies-container';

export interface RootEndpoints {
  unprotected: (router: Router, dependencies: ChambreReservationDependenciesContainer) => void;
}

export const rootChambreReservationEndpoints: RootEndpoints = {
  unprotected: (router: Router, { commandDispatcher, queries }: ChambreReservationDependenciesContainer): void => {
    /* READ */
    router.get(
      '/search/rooms/:startDate/:endDate/:capacity',
      getAvailableRoomsByValidator(),
      expressMiddlewares.preErrorHandling(),
      asyncHandler(getAvailableRoomsBy(queries.getChambresDisponiblesPar))
    );

    router.get(
      '/booking/rooms/:id',
      getBookingByIdValidator(),
      expressMiddlewares.preErrorHandling(),
      asyncHandler(getBookingById(queries.getReservationParId))
    );

    /* WRITE */
    router.post(
      '/booking/rooms',
      postBookRoomValidator(),
      expressMiddlewares.preErrorHandling(),
      asyncHandler(postBookRoom(commandDispatcher))
    );
  }
};
