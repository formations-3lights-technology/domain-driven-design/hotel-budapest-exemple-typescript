import { ChambreTypeOrmEntity } from '@/chambre-reservation/infrastructure/gateways/chambre-type-orm-entity';
import { ReservationTypeOrmEntity } from '@/chambre-reservation/infrastructure/gateways/reservation-type-orm-entity';

export const rootChambreReservationTypeOrmEntities = [ChambreTypeOrmEntity, ReservationTypeOrmEntity];
