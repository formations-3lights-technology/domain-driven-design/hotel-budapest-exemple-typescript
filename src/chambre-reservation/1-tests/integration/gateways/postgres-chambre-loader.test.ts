import { dropAllTableTypeORM, expect, fakeApiCrypt, fakeDependencies } from '@/1-tests/utils';
import { Erreur } from '@/chambre-reservation/domain/erreurs';
import { PostgresChambreLoader } from '@/chambre-reservation/infrastructure/gateways/postgres-chambre-loader';
import { ReservationTypeOrmEntity } from '@/chambre-reservation/infrastructure/gateways/reservation-type-orm-entity';
import { TypeORMClient } from '@/shared/infrastructure/gateways/db-client/type-orm/type-orm-client';
import { typeORMDbClient } from '@/shared/infrastructure/gateways/db-client/type-orm/type-orm-db-client';
import { NodeEnvironmentVariables } from '@/shared/infrastructure/gateways/node-environment-variables';
import assert from 'assert';

describe('Integration | Chambre réservation | Postgres Chambre loader', () => {
  const environmentVariables = new NodeEnvironmentVariables();
  const dbClient = typeORMDbClient(environmentVariables);
  let underTest: PostgresChambreLoader;

  beforeEach(() => {
    const typeORMClient = new TypeORMClient(
      dbClient,
      fakeDependencies().apiLogger,
      fakeDependencies().contexteService,
      environmentVariables
    );
    underTest = new PostgresChambreLoader(typeORMClient, fakeApiCrypt);
  });

  afterEach(async () => {
    await dropAllTableTypeORM(dbClient);
  });

  describe('Récupérer la chambre par son numéro', () => {
    it('Elle existe', async () => {
      // GIVEN
      await (await dbClient).getRepository(ReservationTypeOrmEntity).save(
        ReservationTypeOrmEntity.mapDepuisSnapshot(
          {
            id: 'reservation-id',
            numeroDeLaChambre: '11',
            mailDeReservation: 'mailDeReservation',
            nombreDePersonnes: 1,
            prixParNuit: 10,
            dateDeDebut: new Date('2023-10-15T19:09:00+02:00'),
            dateDeFin: new Date('2023-10-16T19:09:00+02:00')
          },
          fakeApiCrypt
        )
      );

      // WHEN
      const result = await underTest.getParNumeroOuErreur('11');

      // THEN
      expect(result).to.eql({
        numero: '11',
        nom: 'Chambre Standard',
        capacite: 2,
        prixParNuit: 80,
        reservations: [
          {
            id: 'reservation-id',
            numeroDeLaChambre: '11',
            mailDeReservation: 'mailDeReservation',
            nombreDePersonnes: 1,
            prixParNuit: 10,
            dateDeDebut: new Date('2023-10-15T19:09:00+02:00'),
            dateDeFin: new Date('2023-10-16T19:09:00+02:00')
          }
        ]
      });
    });

    it("Elle n'existe pas", async () => {
      try {
        // WHEN
        await underTest.getParNumeroOuErreur('inconnue');
        assert.fail();
      } catch (e) {
        // THEN
        expect(e).to.instanceof(Erreur.Chambre.NonTrouvee);
        expect(e.message).to.eql('Room is not found');
      }
    });
  });

  describe('Récupérer la liste des chambres correspondantes', () => {
    it('Tout se passe bien', async () => {
      // GIVEN
      await (await dbClient).getRepository(ReservationTypeOrmEntity).save([
        ReservationTypeOrmEntity.mapDepuisSnapshot(
          {
            id: 'reservation-id-11',
            numeroDeLaChambre: '11',
            mailDeReservation: 'mailDeReservation',
            nombreDePersonnes: 1,
            prixParNuit: 10,
            dateDeDebut: new Date('2023-10-15T19:09:00+02:00'),
            dateDeFin: new Date('2023-10-16T19:09:00+02:00')
          },
          fakeApiCrypt
        ),
        ReservationTypeOrmEntity.mapDepuisSnapshot(
          {
            id: 'reservation-id-21-1',
            numeroDeLaChambre: '21',
            mailDeReservation: 'mailDeReservation',
            nombreDePersonnes: 1,
            prixParNuit: 10,
            dateDeDebut: new Date('2023-10-10T19:09:00+02:00'),
            dateDeFin: new Date('2023-10-15T10:09:00+02:00')
          },
          fakeApiCrypt
        ),
        ReservationTypeOrmEntity.mapDepuisSnapshot(
          {
            id: 'reservation-id-31-1',
            numeroDeLaChambre: '31',
            mailDeReservation: 'mailDeReservation',
            nombreDePersonnes: 1,
            prixParNuit: 10,
            dateDeDebut: new Date('2023-10-17T19:09:00+02:00'),
            dateDeFin: new Date('2023-10-19T10:09:00+02:00')
          },
          fakeApiCrypt
        )
      ]);

      // WHEN
      const result = await underTest.getPar(2, {
        dateDeDebut: new Date('2023-10-15T00:09:00+02:00'),
        dateDeFin: new Date('2023-10-17T00:09:00+02:00')
      });

      // THEN
      expect(result).to.eql([
        {
          numero: '12',
          nom: 'Chambre Familiale',
          capacite: 4,
          prixParNuit: 150,
          reservations: []
        },
        {
          numero: '14',
          nom: 'Chambre vue Jardin',
          capacite: 3,
          prixParNuit: 170,
          reservations: []
        },
        {
          numero: '21',
          nom: 'Chambre Standard',
          capacite: 2,
          prixParNuit: 80,
          reservations: [
            {
              id: 'reservation-id-21-1',
              numeroDeLaChambre: '21',
              mailDeReservation: 'mailDeReservation',
              nombreDePersonnes: 1,
              prixParNuit: 10,
              dateDeDebut: new Date('2023-10-10T19:09:00+02:00'),
              dateDeFin: new Date('2023-10-15T10:09:00+02:00')
            }
          ]
        },
        {
          numero: '22',
          nom: 'Suite Familiale',
          capacite: 5,
          prixParNuit: 285,
          reservations: []
        },
        {
          numero: '31',
          nom: 'Chambre Standard',
          capacite: 2,
          prixParNuit: 80,
          reservations: [
            {
              id: 'reservation-id-31-1',
              numeroDeLaChambre: '31',
              mailDeReservation: 'mailDeReservation',
              nombreDePersonnes: 1,
              prixParNuit: 10,
              dateDeDebut: new Date('2023-10-17T19:09:00+02:00'),
              dateDeFin: new Date('2023-10-19T10:09:00+02:00')
            }
          ]
        },
        {
          numero: '32',
          capacite: 3,
          nom: 'Chambre Deluxe vue Jardin',
          prixParNuit: 235,
          reservations: []
        }
      ]);
    });
  });
});
