import { dropAllTableTypeORM, expect, fakeApiCrypt, fakeDependencies, fakeIdentityGenerator } from '@/1-tests/utils';
import { Erreur } from '@/chambre-reservation/domain/erreurs';
import { ReservationSnapshot } from '@/chambre-reservation/domain/reservation/reservation';
import { ReservationRepository } from '@/chambre-reservation/domain/reservation/Reservation-repository';
import { PostgresReservationRepository } from '@/chambre-reservation/infrastructure/gateways/postgres-reservation-repository';
import { ReservationTypeOrmEntity } from '@/chambre-reservation/infrastructure/gateways/reservation-type-orm-entity';
import { IdentityGenerator } from '@/shared/domain/gateways/Identity-generator';
import { TypeORMClient } from '@/shared/infrastructure/gateways/db-client/type-orm/type-orm-client';
import { typeORMDbClient } from '@/shared/infrastructure/gateways/db-client/type-orm/type-orm-db-client';
import { NodeEnvironmentVariables } from '@/shared/infrastructure/gateways/node-environment-variables';
import { StubbedType } from '@salesforce/ts-sinon';
import assert from 'assert';

describe('Integration | Chambre réservation | Postgres Réservation repository', () => {
  const environmentVariables = new NodeEnvironmentVariables();
  const dbClient = typeORMDbClient(environmentVariables);
  let identityGenerator: StubbedType<IdentityGenerator>;
  let underTest: ReservationRepository;

  beforeEach(() => {
    const typeORMClient = new TypeORMClient(
      dbClient,
      fakeDependencies().apiLogger,
      fakeDependencies().contexteService,
      environmentVariables
    );
    identityGenerator = fakeIdentityGenerator();
    underTest = new PostgresReservationRepository(typeORMClient, identityGenerator, fakeApiCrypt);
  });

  afterEach(async () => {
    await dropAllTableTypeORM(dbClient);
  });

  describe('Next id', () => {
    it('Tout se passe bien', async () => {
      // GIVEN
      identityGenerator.generate.returns('reservation-id');

      // WHEN
      const result = await underTest.nextId();

      // THEN
      expect(result).to.eql('reservation-id');
    });
  });

  describe('Save', () => {
    it('Tout se passe bien', async () => {
      // GIVEN
      const reservation: ReservationSnapshot = {
        id: 'reservation-id',
        mailDeReservation: 'email@reservation.com',
        numeroDeLaChambre: '11',
        nombreDePersonnes: 2,
        prixParNuit: 10,
        dateDeDebut: new Date('2023-10-15T09:18:00+02:00'),
        dateDeFin: new Date('2023-10-16T09:18:00+02:00')
      };

      // WHEN
      await underTest.save(reservation);

      // THEN
      const result = await (await dbClient).createQueryBuilder(ReservationTypeOrmEntity, 'r').select().getRawMany();
      expect(result).to.eql([
        {
          r_id: 'reservation-id',
          r_numero_de_la_chambre: '11',
          r_mail_de_reservation: '#email@reservation.com#',
          r_nombre_de_personnes: 2,
          r_prix_par_nuit: 10,
          r_date_de_debut: new Date('2023-10-15T09:18:00+02:00'),
          r_date_de_fin: new Date('2023-10-16T09:18:00+02:00')
        }
      ]);
    });
  });

  describe('Get', () => {
    it('Tout se passe bien', async () => {
      // GIVEN
      const reservation: ReservationSnapshot = {
        id: 'reservation-id',
        mailDeReservation: 'email@reservation.com',
        numeroDeLaChambre: '11',
        nombreDePersonnes: 2,
        prixParNuit: 10,
        dateDeDebut: new Date('2023-10-15T09:18:00+02:00'),
        dateDeFin: new Date('2023-10-16T09:18:00+02:00')
      };
      await underTest.save(reservation);

      // WHEN
      const result = await underTest.getParIdOuErreur('reservation-id');

      // THEN
      expect(result).to.eql(reservation);
    });

    it('Non trouvée', async () => {
      try {
        // WHEN
        await underTest.getParIdOuErreur('inconnue');
        assert.fail();
      } catch (e) {
        // THEN
        expect(e).to.instanceof(Erreur.Reservation.NonTrouvee);
        expect(e.message).to.eql('Reservation is not found');
      }
    });
  });
});
