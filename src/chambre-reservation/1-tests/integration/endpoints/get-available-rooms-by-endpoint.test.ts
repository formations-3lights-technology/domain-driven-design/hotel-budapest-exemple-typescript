import { expect, fakeDependencies, FakeExpressServer } from '@/1-tests/utils';
import supertest from 'supertest';

describe('Integration | Chambre réservation | GET /search/rooms/:startDate/:endDate/:capacity', () => {
  let fakeApi: supertest.SuperTest<supertest.Test>;
  let query;
  const baseUrl = '/v1/search/rooms/2022-11-09/2022-11-12/3';

  beforeEach(async () => {
    const dependencies = fakeDependencies();
    query = dependencies.chambreReservationDependenciesContainer.queries.getChambresDisponiblesPar;
    const fakeServer = await new FakeExpressServer(dependencies).create();
    fakeApi = fakeServer.api;
  });

  it('Retourne 200', () => {
    // GIVEN
    // @ts-ignore
    query.handle.resolves([
      {
        numero: '11',
        nom: 'nom',
        capacite: 2,
        prixParNuit: 20,
        reservations: []
      }
    ]);

    // WHEN
    return fakeApi
      .get(baseUrl)
      .expect(200)
      .then((response) => {
        // THEN
        expect(query.handle).to.have.been.calledOnceWithExactly(3, new Date('2022-11-09'), new Date('2022-11-12'));
        expect(response.body).to.eql({
          data: [
            {
              number: '11',
              name: 'nom',
              capacity: 2,
              priceByNight: 20
            }
          ]
        });
      });
  });
});
