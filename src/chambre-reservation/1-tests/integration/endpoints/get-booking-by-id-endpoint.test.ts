import { expect, fakeDependencies, FakeExpressServer } from '@/1-tests/utils';
import supertest from 'supertest';

describe('Integration | Chambre réservation | GET /booking/rooms/:id', () => {
  let fakeApi: supertest.SuperTest<supertest.Test>;
  let query;
  const baseUrl = '/v1/booking/rooms/3';

  beforeEach(async () => {
    const dependencies = fakeDependencies();
    query = dependencies.chambreReservationDependenciesContainer.queries.getReservationParId;
    const fakeServer = await new FakeExpressServer(dependencies).create();
    fakeApi = fakeServer.api;
  });

  it('Retourne 200', () => {
    // GIVEN
    // @ts-ignore
    query.handle.resolves({
      id: 'reservation-id',
      mailDeReservation: 'email@reservation.com',
      numeroDeLaChambre: '11',
      nombreDePersonnes: 2,
      prixParNuit: 10,
      dateDeDebut: new Date('2023-10-15T09:18:00+02:00'),
      dateDeFin: new Date('2023-10-16T09:18:00+02:00')
    });

    // WHEN
    return fakeApi
      .get(baseUrl)
      .expect(200)
      .then((response) => {
        // THEN
        expect(query.handle).to.have.been.calledOnceWithExactly('3');
        expect(response.body).to.eql({
          data: {
            id: 'reservation-id',
            bookingEmail: 'email@reservation.com',
            roomNumber: '11',
            totalPerson: 2,
            priceByNight: 10,
            startDate: '2023-10-15T07:18:00.000Z',
            endDate: '2023-10-16T07:18:00.000Z'
          }
        });
      });
  });
});
