import { expect, fakeDependencies, FakeExpressServer } from '@/1-tests/utils';
import { ReserverUneChambreCommand } from '@/chambre-reservation/use-cases/reserver-une-chambre/reserver-une-chambre-command';
import { CommandDispatcher } from '@/shared/command-dispatcher/internal-command-dispatcher';
import supertest from 'supertest';

describe('Integration | Chambre réservation | POST /booking/rooms', () => {
  let fakeApi: supertest.SuperTest<supertest.Test>;
  let commandDispatcher: CommandDispatcher;
  const baseUrl = '/v1/booking/rooms';

  beforeEach(async () => {
    const dependencies = fakeDependencies();
    commandDispatcher = dependencies.chambreReservationDependenciesContainer.commandDispatcher;
    const fakeServer = await new FakeExpressServer(dependencies).create();
    fakeApi = fakeServer.api;
  });

  describe('Retourne 201', () => {
    it('Tout est renseigné', () => {
      // WHEN
      return fakeApi
        .post(baseUrl)
        .send({
          bookingEmail: 'email@test.com',
          roomNumber: '11',
          totalPerson: 2,
          startDate: '2022-11-16',
          endDate: '2022-11-17'
        })
        .expect(201)
        .then((_) => {
          // THEN
          expect(commandDispatcher.dispatch).to.have.been.calledOnceWithExactly(
            new ReserverUneChambreCommand('email@test.com', '11', 2, new Date('2022-11-16'), new Date('2022-11-17'))
          );
        });
    });
  });
});
