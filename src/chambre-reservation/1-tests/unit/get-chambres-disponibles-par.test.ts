import { expect } from '@/1-tests/utils';
import { fakeChambreLoader } from '@/chambre-reservation/1-tests/chambre-reservation-fake-dependencies';
import { ChambreLoader } from '@/chambre-reservation/domain/chambre/Chambre-loader';
import { GetChambresDisponiblesPar } from '@/chambre-reservation/use-cases/get-chambres-disponibles-par';
import { StubbedType } from '@salesforce/ts-sinon';

describe('Unit | Chambre réservation | Get chambres disponibles par', () => {
  let chambreLoader: StubbedType<ChambreLoader>;
  let underTest: GetChambresDisponiblesPar;

  beforeEach(() => {
    chambreLoader = fakeChambreLoader();
    underTest = new GetChambresDisponiblesPar(chambreLoader);
  });

  it('Tout se passe bien', async () => {
    // GIVEN
    chambreLoader.getPar.resolves([
      {
        numero: '11',
        nom: 'nom',
        capacite: 2,
        prixParNuit: 20,
        reservations: []
      }
    ]);

    // WHEN
    const result = await underTest.handle(
      2,
      new Date('2023-10-15T09:18:00+02:00'),
      new Date('2023-10-16T09:18:00+02:00')
    );

    // THEN
    expect(chambreLoader.getPar).to.have.been.calledWith(2, {
      dateDeDebut: new Date('2023-10-15T09:18:00+02:00'),
      dateDeFin: new Date('2023-10-16T09:18:00+02:00')
    });
    expect(result).to.eql([
      {
        numero: '11',
        nom: 'nom',
        capacite: 2,
        prixParNuit: 20,
        reservations: []
      }
    ]);
  });
});
