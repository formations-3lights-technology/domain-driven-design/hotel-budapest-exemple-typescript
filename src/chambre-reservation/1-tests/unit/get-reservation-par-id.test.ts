import { expect } from '@/1-tests/utils';
import { fakeReservationRepository } from '@/chambre-reservation/1-tests/chambre-reservation-fake-dependencies';
import { Erreur } from '@/chambre-reservation/domain/erreurs';
import { ReservationRepository } from '@/chambre-reservation/domain/reservation/Reservation-repository';
import { GetReservationParId } from '@/chambre-reservation/use-cases/get-reservation-par-id';
import { StubbedType } from '@salesforce/ts-sinon';
import assert from 'assert';

describe('Unit | Chambre réservation | Get réservation par id', () => {
  let reservationRepository: StubbedType<ReservationRepository>;
  let underTest: GetReservationParId;

  beforeEach(() => {
    reservationRepository = fakeReservationRepository();
    underTest = new GetReservationParId(reservationRepository);
  });

  it('Tout se passe bien', async () => {
    // GIVEN
    reservationRepository.getParIdOuErreur.resolves({
      id: 'reservation-id',
      mailDeReservation: 'email@reservation.com',
      numeroDeLaChambre: '11',
      nombreDePersonnes: 2,
      prixParNuit: 10,
      dateDeDebut: new Date('2023-10-15T09:18:00+02:00'),
      dateDeFin: new Date('2023-10-16T09:18:00+02:00')
    });

    // WHEN
    const result = await underTest.handle('reservation-id');

    // THEN
    expect(reservationRepository.getParIdOuErreur).to.have.been.calledWith('reservation-id');
    expect(result).to.eql({
      id: 'reservation-id',
      mailDeReservation: 'email@reservation.com',
      numeroDeLaChambre: '11',
      nombreDePersonnes: 2,
      prixParNuit: 10,
      dateDeDebut: new Date('2023-10-15T09:18:00+02:00'),
      dateDeFin: new Date('2023-10-16T09:18:00+02:00')
    });
  });

  describe('Informer quand il y a une erreur', () => {
    it("La réservation n'existe pas", async () => {
      // GIVEN
      reservationRepository.getParIdOuErreur.rejects(new Erreur.Reservation.NonTrouvee());

      try {
        // WHEN
        await underTest.handle('inconnue');
        assert.fail();
      } catch (e) {
        // THEN
        expect(e).to.instanceof(Erreur.Reservation.NonTrouvee);
        expect(e.message).to.eql('Booking is not found');
      }
    });
  });
});
