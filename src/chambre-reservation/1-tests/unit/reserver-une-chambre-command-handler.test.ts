import { expect, fakeEventPublisher } from '@/1-tests/utils';
import {
  fakeChambreLoader,
  fakeReservationRepository
} from '@/chambre-reservation/1-tests/chambre-reservation-fake-dependencies';
import { ChambreLoader } from '@/chambre-reservation/domain/chambre/Chambre-loader';
import { Erreur } from '@/chambre-reservation/domain/erreurs';
import { ReservationEffectueeEvent } from '@/chambre-reservation/domain/reservation/reservation-effectuee-event';
import { ReservationRepository } from '@/chambre-reservation/domain/reservation/Reservation-repository';
import { ReserverUneChambreCommand } from '@/chambre-reservation/use-cases/reserver-une-chambre/reserver-une-chambre-command';
import { ReserverUneChambreCommandHandler } from '@/chambre-reservation/use-cases/reserver-une-chambre/reserver-une-chambre-command-handler';
import { CommandHandler } from '@/shared/command-dispatcher/Command-handler';
import { EventPublisher } from '@/shared/event-publisher/internal-event-publisher';
import { StubbedType } from '@salesforce/ts-sinon';
import assert from 'assert';

describe('Unit | Chambre réservation | Réserver une chambre', () => {
  let reservationRepository: StubbedType<ReservationRepository>;
  let chambreLoader: StubbedType<ChambreLoader>;
  let eventPublisher: StubbedType<EventPublisher>;
  let underTest: CommandHandler;

  beforeEach(() => {
    reservationRepository = fakeReservationRepository();
    chambreLoader = fakeChambreLoader();
    eventPublisher = fakeEventPublisher();
    underTest = new ReserverUneChambreCommandHandler(reservationRepository, chambreLoader, eventPublisher);
  });

  it('Tout se passe bien', async () => {
    // GIVEN
    reservationRepository.nextId.resolves('next-id');
    chambreLoader.getParNumeroOuErreur.resolves({
      numero: '11',
      nom: 'nom',
      capacite: 2,
      prixParNuit: 10,
      reservations: []
    });
    const command = new ReserverUneChambreCommand(
      'email@reservation.com',
      '11',
      2,
      new Date('2023-10-15T09:18:00+02:00'),
      new Date('2023-10-16T09:18:00+02:00')
    );

    // WHEN
    await underTest.handle(command);

    // THEN
    expect(chambreLoader.getParNumeroOuErreur).to.have.been.calledWith('11');
    expect(reservationRepository.save).to.have.been.calledWith({
      id: 'next-id',
      mailDeReservation: 'email@reservation.com',
      numeroDeLaChambre: '11',
      nombreDePersonnes: 2,
      prixParNuit: 10,
      dateDeDebut: new Date('2023-10-15T09:18:00+02:00'),
      dateDeFin: new Date('2023-10-16T09:18:00+02:00')
    });
    expect(eventPublisher.publish).to.have.been.calledWith([
      new ReservationEffectueeEvent(
        'next-id',
        'email@reservation.com',
        '11',
        2,
        10,
        new Date('2023-10-15T09:18:00+02:00'),
        new Date('2023-10-16T09:18:00+02:00')
      )
    ]);
  });

  describe('Informer quand il y a une erreur', () => {
    beforeEach(() => {
      chambreLoader.getParNumeroOuErreur.resolves({
        numero: '11',
        nom: 'nom',
        capacite: 2,
        prixParNuit: 10,
        reservations: [
          {
            dateDeDebut: new Date('2023-10-15T09:18:00+02:00'),
            dateDeFin: new Date('2023-10-17T09:18:00+02:00')
          }
        ]
      });
    });

    it("La chambre n'existe pas", async () => {
      // GIVEN
      chambreLoader.getParNumeroOuErreur.rejects(new Erreur.Chambre.NonTrouvee());
      const command = new ReserverUneChambreCommand(
        'email@reservation.com',
        'inconnue',
        2,
        new Date('2023-10-15T09:18:00+02:00'),
        new Date('2023-10-16T09:18:00+02:00')
      );

      try {
        // WHEN
        await underTest.handle(command);
        assert.fail();
      } catch (e) {
        // THEN
        expect(e).to.instanceof(Erreur.Chambre.NonTrouvee);
        expect(e.message).to.eql('Room is not found');
      }
    });

    it("Moins d'une nuit", async () => {
      // GIVEN
      const command = new ReserverUneChambreCommand(
        'email@reservation.com',
        '11',
        2,
        new Date('2023-10-15T09:18:00+02:00'),
        new Date('2023-10-15T09:18:00+02:00')
      );

      try {
        // WHEN
        await underTest.handle(command);
        assert.fail();
      } catch (e) {
        // THEN
        expect(e).to.instanceof(Erreur.Reservation.AuMoinsUneNuit);
        expect(e.message).to.eql('The booking must be at least for one night');
      }
    });

    describe('La chambre ne correspond pas à la reservation', () => {
      it('Elle est trop petite', async () => {
        // GIVEN
        const command = new ReserverUneChambreCommand(
          'email@reservation.com',
          '11',
          3,
          new Date('2023-10-15T09:18:00+02:00'),
          new Date('2023-10-16T09:18:00+02:00')
        );

        try {
          // WHEN
          await underTest.handle(command);
          assert.fail();
        } catch (e) {
          // THEN
          expect(e).to.instanceof(Erreur.Reservation.ChambreTropPetite);
          expect(e.message).to.eql('The capacity of the room is not enough');
        }
      });

      it('Elle est déjà réservée', async () => {
        // GIVEN
        const command = new ReserverUneChambreCommand(
          'email@reservation.com',
          '11',
          2,
          new Date('2023-10-14T09:18:00+02:00'),
          new Date('2023-10-16T09:18:00+02:00')
        );

        try {
          // WHEN
          await underTest.handle(command);
          assert.fail();
        } catch (e) {
          // THEN
          expect(e).to.instanceof(Erreur.Reservation.ChambreDejaReservee);
          expect(e.message).to.eql('Room is already booked');
        }
      });
    });
  });
});
