import { fakeEventPublisher } from '@/1-tests/utils';
import { ChambreReservationDependenciesContainer } from '@/chambre-reservation/2-configuration/root-chambre-reservation-dependencies-container';
import { ChambreLoader } from '@/chambre-reservation/domain/chambre/Chambre-loader';
import { ReservationRepository } from '@/chambre-reservation/domain/reservation/Reservation-repository';
import { GetChambresDisponiblesPar } from '@/chambre-reservation/use-cases/get-chambres-disponibles-par';
import { GetReservationParId } from '@/chambre-reservation/use-cases/get-reservation-par-id';
import { CommandDispatcher } from '@/shared/command-dispatcher/internal-command-dispatcher';
import { StubbedType, stubInterface } from '@salesforce/ts-sinon';
import sinon from 'sinon';

export const fakeReservationRepository: () => StubbedType<ReservationRepository> = () =>
  stubInterface<ReservationRepository>(sinon);
export const fakeChambreLoader: () => StubbedType<ChambreLoader> = () => stubInterface<ChambreLoader>(sinon);

export const chambreReservationFakeDependencies = (): ChambreReservationDependenciesContainer => ({
  commandDispatcher: stubInterface<CommandDispatcher>(sinon),
  eventPublisher: fakeEventPublisher(),
  queries: {
    // @ts-ignore
    getChambresDisponiblesPar: stubInterface<GetChambresDisponiblesPar>(sinon),
    // @ts-ignore
    getReservationParId: stubInterface<GetReservationParId>(sinon)
  }
});
