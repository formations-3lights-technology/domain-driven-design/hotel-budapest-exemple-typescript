export type Chambre = Readonly<{
  numero: string;
  nom: string;
  capacite: number;
  prixParNuit: number;
  reservations: ReadonlyArray<{
    dateDeDebut: Date;
    dateDeFin: Date;
  }>;
}>;
