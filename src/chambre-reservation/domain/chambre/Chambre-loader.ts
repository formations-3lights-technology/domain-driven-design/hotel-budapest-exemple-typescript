import { Chambre } from '@/chambre-reservation/domain/chambre/chambre';

export interface ChambreLoader {
  getParNumeroOuErreur(numero: string): Promise<Chambre>;

  getPar(capacite: number, intervalle: { dateDeDebut: Date; dateDeFin: Date }): Promise<Chambre[]>;
}
