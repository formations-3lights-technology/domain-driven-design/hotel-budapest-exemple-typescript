import { ElementNonTrouve } from '@/shared/domain/erreurs/element-non-trouve';
import { ErreurDeValidation } from '@/shared/domain/erreurs/erreur-de-validation';

export namespace Erreur {
  export namespace Reservation {
    export class NonTrouvee extends ElementNonTrouve {
      constructor() {
        super('Booking is not found');
      }
    }

    export class AuMoinsUneNuit extends ErreurDeValidation {
      constructor() {
        super('The booking must be at least for one night');
      }
    }

    export class ChambreTropPetite extends ErreurDeValidation {
      constructor() {
        super('The capacity of the room is not enough');
      }
    }

    export class ChambreDejaReservee extends ErreurDeValidation {
      constructor() {
        super('Room is already booked');
      }
    }
  }

  export namespace Chambre {
    export class NonTrouvee extends ElementNonTrouve {
      constructor() {
        super('Room is not found');
      }
    }
  }
}
