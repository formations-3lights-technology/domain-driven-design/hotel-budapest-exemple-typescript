import { defaultConfiguration } from '@/chambre-reservation/2-configuration/default-configuration';
import { DomainEvent } from '@/shared/event-publisher/Domain-event';

type EventSerialized = Readonly<{
  id: string;
  bookingEmail: string;
  roomNumber: string;
  totalPerson: number;
  priceByNight: number;
  startDate: string;
  endDate: string;
}>;

export class ReservationEffectueeEvent extends DomainEvent<EventSerialized> {
  static readonly label = 'BookingDoneEvent';
  protected boundedContext = defaultConfiguration.boundedContextName;
  protected aggregate = defaultConfiguration.aggregatesName.reservation;

  constructor(
    readonly id: string,
    readonly mailDeReservation: string,
    readonly numeroDeLaChambre: string,
    readonly nombreDePersonnes: number,
    readonly prixParNuit: number,
    readonly dateDeDebut: Date,
    readonly dateDeFin: Date
  ) {
    super(id);
  }

  isPublic(): boolean {
    return false;
  }

  label(): string {
    return ReservationEffectueeEvent.label;
  }

  version(): string {
    return 'v1';
  }

  protected serializedData(): EventSerialized {
    return {
      id: this.id,
      bookingEmail: this._apiCrypt.encrypt(this.mailDeReservation),
      roomNumber: this.numeroDeLaChambre,
      totalPerson: this.nombreDePersonnes,
      priceByNight: this.prixParNuit,
      startDate: this.dateDeDebut.toISOString(),
      endDate: this.dateDeFin.toISOString()
    };
  }
}
