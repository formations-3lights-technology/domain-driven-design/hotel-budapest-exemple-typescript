import { Erreur } from '@/chambre-reservation/domain/erreurs';
import { differenceInCalendarDays } from 'date-fns';

export class ReservationIntervalle {
  private constructor(readonly dateDeDebut: Date, readonly dateDeFin: Date) {}

  static init(dateDeDebut: Date, dateDeFin: Date): ReservationIntervalle {
    const differenceEnJours = differenceInCalendarDays(dateDeDebut, dateDeFin);
    if (differenceEnJours === 0) {
      throw new Erreur.Reservation.AuMoinsUneNuit();
    }

    return new ReservationIntervalle(dateDeDebut, dateDeFin);
  }
}
