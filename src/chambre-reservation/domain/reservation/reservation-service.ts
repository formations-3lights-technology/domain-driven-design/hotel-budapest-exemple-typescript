import { Chambre } from '@/chambre-reservation/domain/chambre/chambre';
import { ChambreLoader } from '@/chambre-reservation/domain/chambre/Chambre-loader';
import { Erreur } from '@/chambre-reservation/domain/erreurs';
import { Reservation } from '@/chambre-reservation/domain/reservation/reservation';
import { ReservationRepository } from '@/chambre-reservation/domain/reservation/Reservation-repository';
import { areIntervalsOverlapping } from 'date-fns';

export class ReservationService {
  constructor(
    private readonly reservationRepository: ReservationRepository,
    private readonly chambreLoader: ChambreLoader
  ) {}

  async reserverUneChambre(
    mailDeReservation: string,
    numeroDeLaChambre: string,
    nombreDePersonnes: number,
    dateDeDebut: Date,
    dateDeFin: Date
  ): Promise<Reservation> {
    const chambre = await this.chambreLoader.getParNumeroOuErreur(numeroDeLaChambre);
    await this.verifierLaChambre(chambre, nombreDePersonnes, dateDeDebut, dateDeFin);

    const reservationId = await this.reservationRepository.nextId();
    return Reservation.effectueePour(
      reservationId,
      mailDeReservation,
      numeroDeLaChambre,
      nombreDePersonnes,
      chambre.prixParNuit,
      dateDeDebut,
      dateDeFin
    );
  }

  private async verifierLaChambre(
    chambre: Chambre,
    nombreDePersonnes: number,
    dateDeDebut: Date,
    dateDeFin: Date
  ): Promise<void> {
    if (chambre.capacite < nombreDePersonnes) {
      throw new Erreur.Reservation.ChambreTropPetite();
    }

    if (
      chambre.reservations.some((r) =>
        areIntervalsOverlapping({ start: r.dateDeDebut, end: r.dateDeFin }, { start: dateDeDebut, end: dateDeFin })
      )
    ) {
      throw new Erreur.Reservation.ChambreDejaReservee();
    }
  }
}
