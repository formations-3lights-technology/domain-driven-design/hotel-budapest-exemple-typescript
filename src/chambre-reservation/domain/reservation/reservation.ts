import { ReservationEffectueeEvent } from '@/chambre-reservation/domain/reservation/reservation-effectuee-event';
import { ReservationIntervalle } from '@/chambre-reservation/domain/reservation/reservation-intervalle';
import { Aggregate } from '@/shared/domain/aggregate';

export type ReservationSnapshot = Readonly<{
  id: string;
  mailDeReservation: string;
  numeroDeLaChambre: string;
  nombreDePersonnes: number;
  prixParNuit: number;
  dateDeDebut: Date;
  dateDeFin: Date;
}>;

export class Reservation extends Aggregate {
  private constructor(
    private readonly id: string,
    private readonly mailDeReservation: string,
    private readonly numeroDeLaChambre: string,
    private readonly nombreDePersonnes: number,
    private readonly prixParNuit: number,
    private readonly reservationIntervalle: ReservationIntervalle
  ) {
    super();
  }

  get snapshot(): ReservationSnapshot {
    return {
      id: this.id,
      mailDeReservation: this.mailDeReservation,
      numeroDeLaChambre: this.numeroDeLaChambre,
      nombreDePersonnes: this.nombreDePersonnes,
      prixParNuit: this.prixParNuit,
      dateDeDebut: this.reservationIntervalle.dateDeDebut,
      dateDeFin: this.reservationIntervalle.dateDeFin
    };
  }

  static effectueePour(
    id: string,
    mailDeReservation: string,
    numeroDeLaChambre: string,
    nombreDePersonnes: number,
    prixParNuit: number,
    dateDeDebut: Date,
    dateDeFin: Date
  ): Reservation {
    const reservationIntervalle = ReservationIntervalle.init(dateDeDebut, dateDeFin);
    const reservation = new Reservation(
      id,
      mailDeReservation,
      numeroDeLaChambre,
      nombreDePersonnes,
      prixParNuit,
      reservationIntervalle
    );

    reservation.apply(
      new ReservationEffectueeEvent(
        id,
        mailDeReservation,
        numeroDeLaChambre,
        nombreDePersonnes,
        prixParNuit,
        dateDeDebut,
        dateDeFin
      )
    );

    return reservation;
  }
}
