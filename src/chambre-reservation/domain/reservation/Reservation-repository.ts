import { ReservationSnapshot } from '@/chambre-reservation/domain/reservation/reservation';

export interface ReservationRepository {
  nextId(): Promise<string>;

  save(reservation: ReservationSnapshot): Promise<void>;

  getParIdOuErreur(id: string): Promise<ReservationSnapshot>;
}
