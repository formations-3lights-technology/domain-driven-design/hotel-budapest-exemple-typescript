import { Contexte, ContexteService } from '@/shared/domain/gateways/Contexte-service';
import { AsyncLocalStorage } from 'async_hooks';

export class AsyncLocalContexteService implements ContexteService {
  private storage = new AsyncLocalStorage<Map<string, Contexte>>();
  private readonly CONTEXTE_CLEF = 'context';

  store(
    correlationId: string,
    userId: string,
    authorization: Nullable<string>,
    scopes: string[],
    cb: () => void
  ): void {
    this.storage.run(new Map(), () => {
      this.storage.getStore()!.set(this.CONTEXTE_CLEF, {
        correlationId,
        userId,
        authorization: authorization || '',
        scopes
      });
      cb();
    });
  }

  get(): Nullable<Contexte> {
    return this.storage.getStore() ? this.storage.getStore()?.get(this.CONTEXTE_CLEF) : null;
  }
}
