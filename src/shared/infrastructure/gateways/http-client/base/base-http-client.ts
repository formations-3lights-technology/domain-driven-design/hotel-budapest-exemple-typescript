import { EchecDependance } from '@/shared/domain/erreurs/echec-dependance';
import { ApiLogger } from '@/shared/domain/gateways/Api-logger';
import { Headers, HttpClient, QueryParams } from '@/shared/domain/gateways/Http-client';
import { MetricsCollector } from '@/shared/infrastructure/gateways/metrics-collector/Metrics-collector';
import { Got, HTTPError, Response } from 'got';
import { omit } from 'ramda';

export type ApiResponse<R, H = {}> = Readonly<{
  headers: H;
  response: R;
}>;

export abstract class BaseHttpClient implements HttpClient {
  protected readonly GET = 'GET';
  protected readonly POST = 'POST';
  protected readonly client: Got;
  protected configuration: { baseUrl: string; token: string } = { baseUrl: '', token: '' };

  protected abstract readonly LOG_HEADER: string;

  protected constructor(
    protected readonly apiLogger: ApiLogger,
    protected readonly metricsCollector: MetricsCollector
  ) {}

  async get<R, Q extends QueryParams = any, H extends Headers = any>(
    endpoint: string,
    queryParams?: Q,
    headers?: H
  ): Promise<R> {
    const correlationId = await this.getCorrelationId();

    try {
      const url = await this.constructFullUrl(endpoint, queryParams);
      const response = await this.client.get<R>(url, await this.constructRequestOptions(undefined, headers));
      return this.handleApiSuccess(this.GET, endpoint, correlationId, response, queryParams);
    } catch (error) {
      return this.handleApiError(this.GET, endpoint, correlationId, error, queryParams);
    }
  }

  async post<B, R = void, Q extends QueryParams = any>(endpoint: string, payload: B, queryParams?: Q): Promise<R> {
    const correlationId = await this.getCorrelationId();

    try {
      const url = await this.constructFullUrl(endpoint, queryParams);
      const response = await this.client.post<R>(url, await this.constructRequestOptions<B>(payload));
      return this.handleApiSuccess(this.POST, endpoint, correlationId, response, queryParams, payload);
    } catch (error) {
      return this.handleApiError(this.POST, endpoint, correlationId, error, queryParams, payload);
    }
  }

  protected abstract constructFullUrl(endpoint: string, queryParams?: QueryParams): Promise<string>;

  protected abstract constructRequestOptions<P = any, H extends Headers = any>(payload?: P, headers?: H): Promise<any>;

  protected abstract getCorrelationId(): Promise<string>;

  protected async getAuthorization(): Promise<string> {
    return '';
  }

  protected collectMetrics(endpoint: string, method: string, statusCode: number, phasesTotal?: number): void {
    this.metricsCollector.collectMetrics(method, endpoint, statusCode, this.log(phasesTotal, 0));
  }

  protected handleApiSuccess<R, Q, B>(
    method: string,
    endpoint: string,
    correlationId: string,
    response: Response<R>,
    queryParams?: Q,
    payload?: B
  ): R {
    this.logResponse<R, Q, B>(
      `${this.LOG_HEADER} ${method} ${endpoint}`,
      correlationId,
      queryParams,
      payload
    )(response);
    this.collectMetrics(endpoint, method, response.statusCode, response.timings.phases.total);
    return { headers: response.headers, response: response.body } as unknown as R;
  }

  protected handleApiError<Q, B>(
    method: string,
    endpoint: string,
    correlationId: string,
    error: HTTPError,
    queryParams?: Q,
    payload?: B
  ) {
    this.logError<Q, B>(`${this.LOG_HEADER} ${method} ${endpoint}`, correlationId, queryParams, payload)(error);
    this.collectMetrics(endpoint, method, error.response.statusCode, error.response.timings.phases.total);
    return Promise.reject(
      new EchecDependance(
        error.response.statusCode,
        `${this.LOG_HEADER} - ${error.response.statusMessage || 'statusMessage'}`,
        `${this.LOG_HEADER} - ${errorMessage(error)}`,
        error.response.body
      )
    );

    function errorMessage(error: any): string {
      if (error.response && error.response.body && error.response.body.message) {
        return error.response.body.message;
      }
      return 'message';
    }
  }

  protected logResponse<R, Q, B>(
    message: string,
    correlationId: string,
    queryParams?: Q,
    payload?: B
  ): (response: Response<R>) => void {
    return (response: Response<R>) =>
      this.apiLogger.info(message, {
        correlationId,
        queryParams: this.log(queryParams),
        payload: this.log(payload),
        statusCode: response.statusCode,
        response: { body: this.log(response.body) },
        timing: response.timings,
        elapsedTime: this.log(response.timings.phases.total, 0),
        timingPhases: response.timings.phases
      });
  }

  protected logError<Q, B>(
    message: string,
    correlationId: string,
    queryParams?: Q,
    payload?: B
  ): (error: HTTPError) => void {
    return (error: HTTPError) =>
      this.apiLogger.error(`${message} failed`, {
        correlationId,
        queryParams: this.log(queryParams),
        payload: this.log(payload),
        statusCode: error.response.statusCode,
        response: { body: error.response && error.response.body ? this.log(error.response.body) : {} },
        error: {
          name: error.name,
          message: error.message
        },
        timing: error.response.timings,
        elapsedTime: this.log(error.response.timings.phases.total, 0),
        timingPhases: error.response.timings.phases
      });
  }

  protected log(value: any, d: any = {}): any {
    if (value && value.file) {
      return { ...value, file: omit(['data'], value.file) };
    }

    const arrayFields = [];
    arrayFields.forEach((field) => {
      if (value && value[field]) {
        value = truncateArray(value, field, 2);
      }
    });

    if (Array.isArray(value) && value.length > 2) {
      value = value.slice(0, 2).concat(`...${value.length - 2} more`);
    }

    return value ?? d;

    function truncateArray(response: any, field: string, total: number): any {
      if (Array.isArray(response[field]) && response[field].length > total) {
        return {
          ...response,
          [field]: response[field].slice(0, total).concat(`...${response[field].length - total} more`)
        };
      }

      return response;
    }
  }
}
