import { ApiLogger } from '@/shared/domain/gateways/Api-logger';
import { ContexteService } from '@/shared/domain/gateways/Contexte-service';
import { HttpClient } from '@/shared/domain/gateways/Http-client';
import { MetricsCollector } from '@/shared/infrastructure/gateways/metrics-collector/Metrics-collector';
import { Got } from 'got';
import { BaseHttpClient } from './base-http-client';

export abstract class WithContextBaseHttpClient extends BaseHttpClient implements HttpClient {
  protected abstract readonly LOG_HEADER: string;
  protected readonly client: Got;
  protected configuration: { baseUrl: string; token: string } = { baseUrl: '', token: '' };

  protected constructor(
    protected readonly apiLogger: ApiLogger,
    protected readonly contexteService: ContexteService,
    protected readonly metricsCollector: MetricsCollector
  ) {
    super(apiLogger, metricsCollector);
  }

  protected async getCorrelationId(): Promise<string> {
    const contexte = this.contexteService.get();
    return contexte ? contexte.correlationId : '';
  }

  protected async getAuthorization(): Promise<string> {
    return this.configuration.token;
  }

  protected async constructRequestOptions<P = any, H = any>(payload?: P, headers?: H): Promise<any> {
    return {
      headers: {
        authorization: await this.getAuthorization(),
        ...headers,
        'x-correlation-id': await this.getCorrelationId()
      },
      json: payload
    };
  }
}
