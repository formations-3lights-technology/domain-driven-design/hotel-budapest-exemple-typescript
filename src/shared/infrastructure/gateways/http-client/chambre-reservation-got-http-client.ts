import { ApiLogger } from '@/shared/domain/gateways/Api-logger';
import { ContexteService } from '@/shared/domain/gateways/Contexte-service';
import { EnvironmentVariables } from '@/shared/domain/gateways/Environment-variables';
import { HttpClient, QueryParams } from '@/shared/domain/gateways/Http-client';
import { MetricsCollector } from '@/shared/infrastructure/gateways/metrics-collector/Metrics-collector';
import buildUrl from 'build-url';
import got, { Got } from 'got';
import { WithContextBaseHttpClient } from './base/with-context-base-http-client';

export class ChambreReservationGotHttpClient extends WithContextBaseHttpClient implements HttpClient {
  protected readonly LOG_HEADER = 'Room Booking Service';
  protected readonly client: Got;

  constructor(
    protected readonly apiLogger: ApiLogger,
    protected readonly contextService: ContexteService,
    protected readonly metricsCollector: MetricsCollector,
    private readonly environmentVariables: EnvironmentVariables
  ) {
    super(apiLogger, contextService, metricsCollector);
    this.client = got.extend({
      responseType: 'json'
    });
  }

  protected async getAuthorization(): Promise<string> {
    const contexte = this.contextService.get();
    return contexte ? contexte.authorization : '';
  }

  protected async constructFullUrl(endpoint: string, queryParams: QueryParams = {}): Promise<string> {
    return buildUrl(this.environmentVariables.ROOM_BOOKING_SERVICE_BASE_URL, {
      disableCSV: true,
      path: endpoint,
      queryParams
    });
  }
}
