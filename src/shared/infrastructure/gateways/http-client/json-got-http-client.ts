import { ApiLogger } from '@/shared/domain/gateways/Api-logger';
import { ContexteService } from '@/shared/domain/gateways/Contexte-service';
import { HttpClient, QueryParams } from '@/shared/domain/gateways/Http-client';
import { MetricsCollector } from '@/shared/infrastructure/gateways/metrics-collector/Metrics-collector';
import buildUrl from 'build-url';
import got, { Got } from 'got';
import { WithContextBaseHttpClient } from './base/with-context-base-http-client';

export class JsonGotHttpClient extends WithContextBaseHttpClient implements HttpClient {
  protected readonly LOG_HEADER = 'External API';
  protected readonly client: Got;

  constructor(
    protected readonly apiLogger: ApiLogger,
    protected readonly contextService: ContexteService,
    protected readonly metricsCollector: MetricsCollector
  ) {
    super(apiLogger, contextService, metricsCollector);
    this.client = got.extend({
      responseType: 'json'
    });
  }

  protected async constructFullUrl(endpoint: string, queryParams: QueryParams = {}): Promise<string> {
    return buildUrl(this.configuration.baseUrl, {
      disableCSV: true,
      path: endpoint,
      queryParams
    });
  }
}
