export interface MetricsCollector {
  collectMetrics(method: string, path: string, statusCode: number, elapsedTime: number): void;
}
