import { PrometheusMetricsCollector } from '@/shared/infrastructure/gateways/metrics-collector/prometheus-metrics-collector';

export class FacturationServiceMetricsCollector extends PrometheusMetricsCollector {
  constructor() {
    super('http_request_duration_invoicing_service_ms', 'Duration of HTTP requests in ms');
  }
}
