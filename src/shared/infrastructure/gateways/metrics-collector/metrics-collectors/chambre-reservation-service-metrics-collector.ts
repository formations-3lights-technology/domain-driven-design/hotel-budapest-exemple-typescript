import { PrometheusMetricsCollector } from '@/shared/infrastructure/gateways/metrics-collector/prometheus-metrics-collector';

export class ChambreReservationServiceMetricsCollector extends PrometheusMetricsCollector {
  constructor() {
    super('http_request_duration_room_booking_service_ms', 'Duration of HTTP requests in ms');
  }
}
