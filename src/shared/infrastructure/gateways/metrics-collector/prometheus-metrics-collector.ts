import prometheusClient, { Histogram } from 'prom-client';
import { MetricsCollector } from './Metrics-collector';

export abstract class PrometheusMetricsCollector implements MetricsCollector {
  private readonly prometheus: Histogram<string>;
  private readonly buckets: number[] = [
    50, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 2000, 3000, 4000, 5000, 10000, 20000, 40000
  ];
  private readonly labelNames: string[] = ['method', 'statusCode'];

  protected constructor(name: string, help: string) {
    this.prometheus = new Histogram({
      name,
      help,
      buckets: this.buckets,
      labelNames: this.labelNames
    });
  }

  static collectDefaultMetrics() {
    return prometheusClient.collectDefaultMetrics();
  }

  collectMetrics(method: string, path: string, statusCode: number, elapsedTime = 0): void {
    const formatMethod = method.toUpperCase();
    this.prometheus.labels(formatMethod, statusCode.toString()).observe(elapsedTime);
  }
}
