import { rootChambreReservationTypeOrmEntities } from '@/chambre-reservation/2-configuration/db/root-chambre-reservation-type-orm-entities';
import { rootDeroulementSejourTypeOrmEntities } from '@/deroulement-sejour/2-configuration/db/root-deroulement-sejour-type-orm-entities';
import { EnvironmentVariables } from '@/shared/domain/gateways/Environment-variables';
import { DataSource } from 'typeorm';

const CONNECTION_NAME = 'default';

export async function typeORMDbClient(environmentVariables: EnvironmentVariables): Promise<DataSource> {
  const poolMaxForWriteSide = Math.trunc(environmentVariables.DATABASE_MAX_POOL_SIZE);

  const dataSource = new DataSource({
    name: CONNECTION_NAME,
    type: 'postgres',
    host: environmentVariables.DATABASE_HOST,
    port: environmentVariables.DATABASE_PORT,
    username: environmentVariables.DATABASE_USERNAME,
    password: environmentVariables.DATABASE_USER_PASSWORD,
    database: environmentVariables.DATABASE_NAME,
    connectTimeoutMS: environmentVariables.DATABASE_CONNECTION_TIMEOUT_IN_MS,
    ssl:
      environmentVariables.isProduction && environmentVariables.DATABASE_SSL
        ? {
            rejectUnauthorized: false
          }
        : false,
    extra: {
      connectionTimeoutMillis: environmentVariables.DATABASE_CONNECTION_TIMEOUT_IN_MS,
      idleTimeoutMillis: environmentVariables.DATABASE_CONNECTION_TIMEOUT_IN_MS,
      max: poolMaxForWriteSide,
      allowExitOnIdle: true
    },
    entities: [...rootChambreReservationTypeOrmEntities, ...rootDeroulementSejourTypeOrmEntities]
  });
  return dataSource.initialize();
}
