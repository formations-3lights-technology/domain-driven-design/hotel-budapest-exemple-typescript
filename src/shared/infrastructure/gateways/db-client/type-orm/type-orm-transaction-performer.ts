import { TransactionPerformer } from '@/shared/domain/gateways/Transaction-performer';
import { TypeORMClient } from './type-orm-client';

export class TypeORMTransactionPerformer implements TransactionPerformer {
  constructor(private typeORMClient: TypeORMClient) {}

  async process<R>(transactionToProcess: () => Promise<R>, retries = 0): Promise<R> {
    return this.typeORMClient.processTransaction(transactionToProcess, retries);
  }
}
