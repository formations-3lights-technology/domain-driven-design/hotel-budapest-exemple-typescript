import { ActionNonAutorisee } from '@/shared/domain/erreurs/action-non-autorisee';
import { EchecDependance } from '@/shared/domain/erreurs/echec-dependance';
import { ElementEnConflit } from '@/shared/domain/erreurs/element-en-conflit';
import { ElementNonTrouve } from '@/shared/domain/erreurs/element-non-trouve';
import { ErreurDeTraitement } from '@/shared/domain/erreurs/erreur-de-traitement';
import { ErreurDeValidation } from '@/shared/domain/erreurs/erreur-de-validation';
import { OptimisticConcurrency } from '@/shared/domain/erreurs/optimistic-concurrency';
import { ServiceIndisponible } from '@/shared/domain/erreurs/service-indisponible';
import { ApiLogger } from '@/shared/domain/gateways/Api-logger';
import { ContexteService } from '@/shared/domain/gateways/Contexte-service';
import { EnvironmentVariables } from '@/shared/domain/gateways/Environment-variables';
import retry from 'async-retry';
import { DataSource, EntityManager, EntityTarget } from 'typeorm';

export class TypeORMClient {
  private db: Nullable<DataSource> = null;
  private transactions: Map<string, Nullable<EntityManager>> = new Map();

  constructor(
    private readonly connection: Promise<DataSource>,
    private readonly apiLogger: ApiLogger,
    private readonly contexteService: ContexteService,
    private readonly environmentVariables: EnvironmentVariables
  ) {}

  async executeQuery<T = any>(query: (db: DataSource) => Promise<T>): Promise<T> {
    return query(await this.getInstance());
  }

  executeTransaction<T = void>(query: (trx: EntityManager) => Promise<T>): Promise<T> {
    const currentTransaction = this.transactions.get(this.getCorrelationId());
    return currentTransaction ? query(currentTransaction) : this.executeQuery((db) => db.transaction(query));
  }

  async processTransaction<R>(transaction: () => Promise<R>, retries = 0): Promise<R> {
    const correlationId = this.getCorrelationId();

    try {
      if (this.transactions.get(correlationId)) {
        return retry(() => transaction(), { retries });
      }

      return await (
        await this.getInstance()
      ).transaction((trx) => {
        this.transactions.set(correlationId, trx);
        return retry(() => transaction(), { retries });
      });
    } catch (error) {
      if (
        error instanceof ActionNonAutorisee ||
        error instanceof ErreurDeValidation ||
        error instanceof ElementNonTrouve ||
        error instanceof ElementEnConflit ||
        error instanceof ErreurDeTraitement ||
        error instanceof EchecDependance
      ) {
        throw error;
      }
      if (this.environmentVariables.SQL_LOG) {
        throw new ErreurDeTraitement(error.message);
      }
      this.apiLogger.error(error.message);
      throw new ErreurDeTraitement('SQL error');
    } finally {
      if (this.transactions.get(correlationId)) {
        this.transactions.delete(correlationId);
      }
    }
  }

  encryptValue(value: string, schema = 'public'): string {
    return `${schema}.pgp_sym_encrypt('${this.escapeSingleQuotes(value)}','${
      this.environmentVariables.ENCRYPTION_KEY
    }')`;
  }

  decryptColumn(column: string, schema = 'public'): string {
    return `${schema}.pgp_sym_decrypt(${column}::bytea,'${this.environmentVariables.ENCRYPTION_KEY}')`;
  }

  async optimisticConcurrency(
    nom: string,
    typeORMEntity: EntityTarget<any>,
    version: Nullable<number>,
    where: Record<string, string>
  ): Promise<Nullable<number>> {
    const entity = await this.executeTransaction((db) =>
      db.createQueryBuilder().select('version').from(typeORMEntity, 't').where(where).getRawOne()
    );

    if (version && entity && entity.version !== version) {
      throw new OptimisticConcurrency(nom, version);
    }

    return entity ? entity.version : null;
  }

  private async getInstance(): Promise<DataSource> {
    if (this.db) {
      return this.db;
    }

    try {
      this.db = await this.connection;
      return this.db;
    } catch (e) {
      if (this.environmentVariables.SQL_LOG) {
        this.apiLogger.error(e);
      }
      throw new ServiceIndisponible();
    }
  }

  private getCorrelationId() {
    return this.contexteService.get() ? this.contexteService.get()!.correlationId : '';
  }

  private escapeSingleQuotes(value: string): string {
    // https://docs.postgresql.fr/9.2/release-8-1-4.html section "Reject unsafe uses of \' in string literals"
    return value.replace(/'/g, "''");
  }
}
