import { DateProvider } from '@/shared/domain/gateways/Date-provider';

export class RealDateProvider implements DateProvider {
  now(): Date {
    return new Date();
  }
}
