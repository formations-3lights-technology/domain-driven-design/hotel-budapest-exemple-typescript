import { IdentityGenerator } from '@/shared/domain/gateways/Identity-generator';
import { v4 as uuidv4 } from 'uuid';

export class UuidIdentityGenerator implements IdentityGenerator {
  generate(): string {
    return uuidv4();
  }
}
