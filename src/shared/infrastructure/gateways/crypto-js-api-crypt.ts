import { ApiCrypt } from '@/shared/domain/gateways/Api-crypt';
import { EnvironmentVariables } from '@/shared/domain/gateways/Environment-variables';
import CryptoJS from 'crypto-js';

export class CryptoJsApiCrypt implements ApiCrypt {
  constructor(private environmentVariables: EnvironmentVariables) {}

  encrypt(value: string | number | boolean): string {
    const encryptionKey = this.environmentVariables.ENCRYPTION_KEY;
    return CryptoJS.AES.encrypt(value.toString(), encryptionKey).toString();
  }

  decrypt(value: string): string {
    const encryptionKey = this.environmentVariables.ENCRYPTION_KEY;
    return CryptoJS.AES.decrypt(value, encryptionKey).toString(CryptoJS.enc.Utf8);
  }
}
