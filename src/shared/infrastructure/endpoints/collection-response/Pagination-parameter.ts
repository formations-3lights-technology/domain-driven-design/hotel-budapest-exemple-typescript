export type PaginationParameter = Readonly<{
  limit: number;
  page: number;
}>;
