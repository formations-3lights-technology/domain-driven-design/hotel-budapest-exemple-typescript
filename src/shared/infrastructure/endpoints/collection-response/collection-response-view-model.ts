import { PaginationParameter } from './Pagination-parameter';

export class CollectionResponseViewModel<D> {
  private readonly PARTIAL_CONTENT_CODE = 206;
  private readonly OK_CODE = 200;

  private readonly INDEX_START = this.paginationParameter.limit * (this.paginationParameter.page - 1);
  private readonly INDEX_END = this.paginationParameter.limit * this.paginationParameter.page;

  constructor(
    private collectionName: string,
    private total: number,
    private collection: D[],
    private paginationParameter: PaginationParameter
  ) {}

  get contentRange(): string {
    return `${this.collectionName} ${this.currentRange}/${this.total}`;
  }

  get statusCode(): number {
    return this.total > this.collection.length ? this.PARTIAL_CONTENT_CODE : this.OK_CODE;
  }

  get data(): D[] {
    return this.collection;
  }

  private get currentRange(): string {
    if (this.containsOnlyOneItem) {
      return '1';
    }
    if (this.isEmpty || this.isOutOfBound) {
      return '0';
    }

    const start = this.INDEX_START + 1;
    const end = this.total > this.INDEX_END ? this.INDEX_END : this.total;

    return `${start}-${end}`;
  }

  private get containsOnlyOneItem(): boolean {
    return this.collection.length === 1 && this.total === 1;
  }

  private get isEmpty(): boolean {
    return this.total === 0;
  }

  private get isOutOfBound(): boolean {
    return this.total < this.INDEX_START;
  }
}
