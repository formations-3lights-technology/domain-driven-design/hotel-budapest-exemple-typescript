import { Express, Request, Response } from 'express';
import asyncHandler from 'express-async-handler';
import prometheusClient from 'prom-client';

export const getMetricsEndpoint = (server: Express): void => {
  server.get(
    '/metrics',
    asyncHandler(async (request: Request, response: Response) => {
      response.send(await prometheusClient.register.metrics());
    })
  );
};
