import { Express, Request, Response } from 'express';
import packageJson from '../../../../package.json';

export const getHealthCheckEndpoint = (server: Express): void => {
  server.get('/health', (request: Request, response: Response) => {
    response.json({ data: { status: 'up', apiVersion: packageJson.version } });
  });
};
