type ValueObjectValue<ValueType> = Readonly<{ value: ValueType }>;

export abstract class ValueObject<VOType extends ValueObjectValue<ValueType>, ValueType> {
  protected constructor(private readonly _value: ValueType) {}

  get value(): ValueType {
    return this._value;
  }

  equals(value: ValueType): boolean {
    return value === this.value;
  }

  includes(values: ValueType[]): boolean {
    return values.includes(this.value);
  }

  is(valueObject: VOType): boolean {
    return valueObject.value === this.value;
  }

  among(valueObjects: VOType[]): boolean {
    return valueObjects.map((v) => v.value).includes(this.value);
  }

  notEquals(value: ValueType): boolean {
    return !this.equals(value);
  }

  notIncludes(values: ValueType[]): boolean {
    return !this.includes(values);
  }

  isNot(valueObject: VOType): boolean {
    return !this.is(valueObject);
  }

  notAmong(valueObjects: VOType[]): boolean {
    return !this.among(valueObjects);
  }
}
