import { ApiCrypt } from './gateways/Api-crypt';

type SerializedMessageMetadata = Readonly<{
  id: string;
  type: string;
  label: string;
  correlation_id: string;
  timestamp: string;
  sender_id: string;
  bounded_context: string;
}>;
export type SerializedMessage<M, D> = Readonly<{
  metadata: SerializedMessageMetadata & M;
  data: D;
}>;
export type MessageType = 'command' | 'event';

class DefaultApiCrypt implements ApiCrypt {
  encrypt(value: string | number | boolean): string {
    return value.toString();
  }

  decrypt(value: string): string {
    return value.toString();
  }
}

export abstract class Message<M = SerializedMessageMetadata, D = any> {
  protected abstract boundedContext: string;

  protected constructor(protected messageType: MessageType) {}

  protected _apiCrypt: ApiCrypt = new DefaultApiCrypt();

  set apiCrypt(value: ApiCrypt) {
    this._apiCrypt = value;
  }

  protected _messageId = '';

  set messageId(value: string) {
    this._messageId = value;
  }

  protected _senderId = '';

  set senderId(value: string) {
    this._senderId = value;
  }

  protected _correlationId = '';

  set correlationId(value: string) {
    this._correlationId = value;
  }

  protected _timestamp = '';

  set timestamp(value: string) {
    this._timestamp = value;
  }

  abstract label(): string;

  abstract isPublic(): boolean;

  abstract serialize(): SerializedMessage<M, D>;
}
