export class ErreurDeValidation extends Error {
  constructor(readonly message: string) {
    super(message);
  }
}
