export class LimiteAppel extends Error {
  constructor(readonly appelMax: number, readonly intervalEnMilliseconde: number) {
    super(`Rate limit: ${appelMax} per ${intervalEnMilliseconde}ms`);
  }
}
