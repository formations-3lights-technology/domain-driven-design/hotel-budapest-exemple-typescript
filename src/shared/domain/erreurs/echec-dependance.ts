export class EchecDependance extends Error {
  constructor(
    readonly statusCode: number,
    readonly statusMessage: string,
    readonly message: string,
    readonly body: Nullable<any> = null
  ) {
    super(message);
  }
}
