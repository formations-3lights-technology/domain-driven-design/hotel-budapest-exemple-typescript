export class ActionNonAutorisee extends Error {
  constructor() {
    super('Current user does not have good rights');
  }
}
