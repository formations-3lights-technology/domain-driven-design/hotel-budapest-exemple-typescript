export class ElementEnConflit extends Error {
  constructor(readonly message: string) {
    super(message);
  }
}
