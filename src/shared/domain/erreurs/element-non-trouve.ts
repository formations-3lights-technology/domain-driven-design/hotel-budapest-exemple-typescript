export class ElementNonTrouve extends Error {
  constructor(readonly message: string) {
    super(message);
  }
}
