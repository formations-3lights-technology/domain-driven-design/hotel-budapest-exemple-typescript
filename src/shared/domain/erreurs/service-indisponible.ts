export class ServiceIndisponible extends Error {
  constructor() {
    super('Service unavailable');
  }
}
