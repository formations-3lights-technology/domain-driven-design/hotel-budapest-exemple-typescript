import uuidValidator from 'uuid-validate';
import { ValueObject } from './value-object';

type ValueObjectValue<ValueType> = Readonly<{ value: ValueType }>;

export abstract class ValueObjectId<VOType extends ValueObjectValue<string>> extends ValueObject<VOType, string> {
  protected readonly VERSION_UUID_SUPPORTEE = 4;

  protected get estValide(): boolean {
    return uuidValidator(this.value, this.VERSION_UUID_SUPPORTEE);
  }

  protected get nEstPasValide(): boolean {
    return !uuidValidator(this.value, this.VERSION_UUID_SUPPORTEE);
  }
}
