import { DomainEvent } from '@/shared/event-publisher/Domain-event';

export abstract class Aggregate {
  protected events: DomainEvent[] = [];

  get raisedEvents(): DomainEvent[] {
    return this.events;
  }

  protected apply(event: DomainEvent) {
    this.events.push(event);
  }
}
