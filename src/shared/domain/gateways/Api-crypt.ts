export interface ApiCrypt {
  encrypt(value: string | number | boolean): string;

  decrypt(value: string): string;
}
