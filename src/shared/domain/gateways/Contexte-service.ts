export type Contexte = Readonly<{
  correlationId: string;
  userId: string;
  authorization: string;
  scopes: string[];
}>;

export interface ContexteService {
  store(correlationId: string, userId: string, authorization: Nullable<string>, scopes: string[], cb: () => void): void;

  get(): Nullable<Contexte>;
}
