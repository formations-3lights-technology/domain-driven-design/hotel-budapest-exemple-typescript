export interface EnvironmentVariables {
  readonly NODE_ENV: string;
  readonly SQL_LOG: boolean;
  readonly LOG_LEVEL: string;
  readonly isProduction: boolean;

  readonly API_PORT: number;
  readonly API_CURRENT_VERSION: string;
  readonly APPLICATION_ID: string;
  readonly X_ENV_HEADER: string;
  readonly ENCRYPTION_KEY: string;

  readonly DATABASE_HOST: string;
  readonly DATABASE_PORT: number;
  readonly DATABASE_NAME: string;
  readonly DATABASE_USERNAME: string;
  readonly DATABASE_USER_PASSWORD: string;
  readonly DATABASE_MAX_POOL_SIZE: number;
  readonly DATABASE_SSL: boolean;
  readonly DATABASE_CONNECTION_TIMEOUT_IN_MS: number;

  readonly ROOM_BOOKING_SERVICE_BASE_URL: string;
  readonly INVOICING_SERVICE_BASE_URL: string;
}
