export type QueryParams = Record<string, string | string[]>;
export type Headers = Record<string, string>;

export interface HttpClient {
  get<R, Q extends QueryParams = any, H extends Headers = any>(
    endpoint: string,
    queryParams?: Q,
    headers?: H
  ): Promise<R>;

  post<B, R = void, Q extends QueryParams = any>(endpoint: string, payload: B, queryParams?: Q): Promise<R>;
}
