export interface TransactionPerformer {
  process<R>(transactionToProcess: () => Promise<R>, retries?: number): Promise<R>;
}
