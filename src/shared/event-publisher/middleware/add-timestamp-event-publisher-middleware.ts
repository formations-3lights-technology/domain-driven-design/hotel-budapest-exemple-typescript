import { DateProvider } from '@/shared/domain/gateways/Date-provider';
import { DomainEvent } from '@/shared/event-publisher/Domain-event';
import { EventPublisherMiddleware } from '@/shared/event-publisher/Event-publisher-middleware';

export class AddTimestampEventPublisherMiddleware implements EventPublisherMiddleware {
  readonly label = AddTimestampEventPublisherMiddleware.name;

  constructor(private dateProvider: DateProvider) {}

  isSilent(): boolean {
    return true;
  }

  async listen(event: DomainEvent): Promise<void> {
    event.timestamp = this.dateProvider.now().toISOString();
  }
}
