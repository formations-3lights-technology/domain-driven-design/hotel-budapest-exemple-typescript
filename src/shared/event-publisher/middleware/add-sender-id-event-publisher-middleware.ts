import { EnvironmentVariables } from '@/shared/domain/gateways/Environment-variables';
import { DomainEvent } from '@/shared/event-publisher/Domain-event';
import { EventPublisherMiddleware } from '@/shared/event-publisher/Event-publisher-middleware';

export class AddSenderIdEventPublisherMiddleware implements EventPublisherMiddleware {
  readonly label = AddSenderIdEventPublisherMiddleware.name;

  constructor(private environmentVariables: EnvironmentVariables) {}

  isSilent(): boolean {
    return true;
  }

  async listen(event: DomainEvent): Promise<void> {
    event.senderId = this.environmentVariables.APPLICATION_ID;
  }
}
