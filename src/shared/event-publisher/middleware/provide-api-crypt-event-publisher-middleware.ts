import { ApiCrypt } from '@/shared/domain/gateways/Api-crypt';
import { DomainEvent } from '@/shared/event-publisher/Domain-event';
import { EventPublisherMiddleware } from '@/shared/event-publisher/Event-publisher-middleware';

export class ProvideApiCryptEventPublisherMiddleware implements EventPublisherMiddleware {
  readonly label = ProvideApiCryptEventPublisherMiddleware.name;

  constructor(private apiCrypt: ApiCrypt) {}

  isSilent(): boolean {
    return true;
  }

  async listen(event: DomainEvent): Promise<void> {
    event.apiCrypt = this.apiCrypt;
  }
}
