import { ContexteService } from '@/shared/domain/gateways/Contexte-service';
import { DomainEvent } from '@/shared/event-publisher/Domain-event';
import { EventPublisherMiddleware } from '@/shared/event-publisher/Event-publisher-middleware';

export class AddCorrelationIdEventPublisherMiddleware implements EventPublisherMiddleware {
  readonly label = AddCorrelationIdEventPublisherMiddleware.name;

  constructor(private contextService: ContexteService) {}

  isSilent(): boolean {
    return true;
  }

  async listen(event: DomainEvent): Promise<void> {
    const contexte = this.contextService.get();
    event.correlationId = contexte ? contexte.correlationId : '';
  }
}
