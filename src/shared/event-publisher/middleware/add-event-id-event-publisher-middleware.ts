import { IdentityGenerator } from '@/shared/domain/gateways/Identity-generator';
import { DomainEvent } from '@/shared/event-publisher/Domain-event';
import { EventPublisherMiddleware } from '@/shared/event-publisher/Event-publisher-middleware';

export class AddEventIdEventPublisherMiddleware implements EventPublisherMiddleware {
  readonly label = AddEventIdEventPublisherMiddleware.name;

  constructor(private identityGenerator: IdentityGenerator) {}

  isSilent(): boolean {
    return true;
  }

  async listen(event: DomainEvent): Promise<void> {
    event.messageId = this.identityGenerator.generate();
  }
}
