import { ApiLogger } from '@/shared/domain/gateways/Api-logger';
import { DomainEvent } from '@/shared/event-publisher/Domain-event';
import { EventPublisherMiddleware } from '@/shared/event-publisher/Event-publisher-middleware';

export class LoggerEventPublisherMiddleware implements EventPublisherMiddleware {
  readonly label = LoggerEventPublisherMiddleware.name;

  constructor(private logger: ApiLogger) {}

  isSilent(): boolean {
    return true;
  }

  async listen(event: DomainEvent): Promise<void> {
    this.logger.info(event.label(), event.serialize());
  }
}
