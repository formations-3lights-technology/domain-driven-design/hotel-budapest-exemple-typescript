import { ApiLogger } from '@/shared/domain/gateways/Api-logger';
import { DomainEvent } from './Domain-event';
import { EventListener } from './Event-listener';
import { EventPublisherMiddleware } from './Event-publisher-middleware';

export type RegisteredEventListeners = Record<string, EventListener[]>;

export interface EventPublisher {
  registerListeners(...listeners: RegisteredEventListeners[]): EventPublisher;

  registerMiddleware(middleware: EventPublisherMiddleware[]): EventPublisher;

  publish(events: DomainEvent[]): Promise<void>;
}

export class InternalEventPublisher implements EventPublisher {
  private listeners: RegisteredEventListeners = {};
  private middleware: EventPublisherMiddleware[] = [];

  constructor(private apiLogger: ApiLogger) {}

  registerListeners(...listeners: RegisteredEventListeners[]): EventPublisher {
    this.listeners = listeners.reduce((acc, listeners) => {
      const eventsLabel = Object.keys(listeners);
      return eventsLabel.reduce((registeredEventListeners, eventLabel) => {
        if (registeredEventListeners[eventLabel]) {
          return {
            ...registeredEventListeners,
            [eventLabel]: registeredEventListeners[eventLabel].concat(listeners[eventLabel])
          };
        } else {
          return { ...registeredEventListeners, [eventLabel]: listeners[eventLabel] };
        }
      }, acc);
    }, {});
    return this;
  }

  registerMiddleware(middleware: EventPublisherMiddleware[]): EventPublisher {
    this.middleware = middleware;
    return this;
  }

  async publish(events: DomainEvent[]): Promise<void> {
    for (const event of events) {
      for (const m of this.middleware) {
        await this.doAsync(m.listen(event), m.label, m.isSilent());
      }

      if (this.listeners[event.label()]) {
        await Promise.all(this.listeners[event.label()].map((l) => this.doAsync(l.listen(event), l.label)));
      }
    }
  }

  async doAsync(promise: Promise<void>, label: string, isSilent = true): Promise<void> {
    try {
      await promise;
    } catch (e) {
      this.apiLogger.error(`${label}: ${e.message}`);
      if (!isSilent) {
        throw e;
      }
    }
  }
}
