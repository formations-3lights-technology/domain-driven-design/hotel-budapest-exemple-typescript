import { DomainEvent } from './Domain-event';

export interface EventPublisherMiddleware {
  readonly label: string;

  listen(event: DomainEvent): Promise<void>;

  isSilent(): boolean;
}
