import { DomainEvent } from './Domain-event';

export interface EventListener {
  readonly label: string;

  listen(event: DomainEvent): Promise<void>;
}
