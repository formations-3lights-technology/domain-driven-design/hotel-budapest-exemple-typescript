import { TransactionPerformer } from '@/shared/domain/gateways/Transaction-performer';
import { Command } from './Command';
import { CommandDispatcherMiddleware } from './Command-dispatcher-middleware';
import { CommandHandler } from './Command-handler';
import { ErrorCommandNotRegistered } from './errors/error-command-not-registered';

export type RegisteredCommandHandlers = Record<string, CommandHandler>;

export interface CommandDispatcher {
  registerHandlers(handlers: RegisteredCommandHandlers): CommandDispatcher;

  registerMiddleware(middleware: CommandDispatcherMiddleware[]): CommandDispatcher;

  dispatch<R = void>(command: Command): Promise<R>;
}

class DefaultCommandHandler implements CommandHandler {
  async handle(_: Command): Promise<void> {}
}

export class InternalCommandDispatcher implements CommandDispatcher {
  private handlers: RegisteredCommandHandlers = {};
  private middleware: CommandDispatcherMiddleware[] = [];

  constructor(private readonly transactionPerformer: TransactionPerformer) {}

  registerHandlers(handlers: RegisteredCommandHandlers): CommandDispatcher {
    this.handlers = handlers;
    return this;
  }

  registerMiddleware(middleware: CommandDispatcherMiddleware[]): CommandDispatcher {
    this.middleware = middleware;
    return this;
  }

  async dispatch<R>(command: Command): Promise<R> {
    if (this.handlers[command.label()] === undefined && !command.isPublic()) {
      throw new ErrorCommandNotRegistered();
    }

    for (const m of this.middleware) {
      await m.handle(command);
    }

    const handler = this.handlers[command.label()] || new DefaultCommandHandler();

    return this.transactionPerformer.process(() => handler.handle(command));
  }
}
