import { Command } from './Command';

export interface CommandHandler<R = void | any> {
  handle(command: Command): Promise<R>;
}
