import { Command } from '@/shared/command-dispatcher/Command';
import { CommandDispatcherMiddleware } from '@/shared/command-dispatcher/Command-dispatcher-middleware';
import { ApiCrypt } from '@/shared/domain/gateways/Api-crypt';

export class ProvideApiCryptCommandDispatcherMiddleware implements CommandDispatcherMiddleware {
  constructor(private apiCrypt: ApiCrypt) {}

  async handle(command: Command): Promise<void> {
    command.apiCrypt = this.apiCrypt;
  }
}
