import { Command } from '@/shared/command-dispatcher/Command';
import { CommandDispatcherMiddleware } from '@/shared/command-dispatcher/Command-dispatcher-middleware';
import { IdentityGenerator } from '@/shared/domain/gateways/Identity-generator';

export class AddCommandIdCommandDispatcherMiddleware implements CommandDispatcherMiddleware {
  constructor(private identityGenerator: IdentityGenerator) {}

  async handle(command: Command): Promise<void> {
    const commandId = command.serialize().metadata.id;
    command.messageId = commandId || this.identityGenerator.generate();
  }
}
