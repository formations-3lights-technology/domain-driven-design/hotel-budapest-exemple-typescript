import { Command } from '@/shared/command-dispatcher/Command';
import { CommandDispatcherMiddleware } from '@/shared/command-dispatcher/Command-dispatcher-middleware';
import { DateProvider } from '@/shared/domain/gateways/Date-provider';

export class AddTimestampCommandDispatcherMiddleware implements CommandDispatcherMiddleware {
  constructor(private dateProvider: DateProvider) {}

  async handle(command: Command): Promise<void> {
    const timestamp = command.serialize().metadata.timestamp;
    command.timestamp = timestamp || this.dateProvider.now().toISOString();
  }
}
