import { Command } from '@/shared/command-dispatcher/Command';
import { CommandDispatcherMiddleware } from '@/shared/command-dispatcher/Command-dispatcher-middleware';
import { ApiLogger } from '@/shared/domain/gateways/Api-logger';

export class LoggerCommandDispatcherMiddleware implements CommandDispatcherMiddleware {
  constructor(private logger: ApiLogger) {}

  async handle(command: Command): Promise<void> {
    this.logger.info(command.label(), command.serialize());
  }
}
