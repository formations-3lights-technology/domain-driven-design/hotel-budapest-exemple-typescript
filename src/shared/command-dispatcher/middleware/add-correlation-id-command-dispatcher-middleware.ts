import { Command } from '@/shared/command-dispatcher/Command';
import { CommandDispatcherMiddleware } from '@/shared/command-dispatcher/Command-dispatcher-middleware';
import { ContexteService } from '@/shared/domain/gateways/Contexte-service';

export class AddCorrelationIdCommandDispatcherMiddleware implements CommandDispatcherMiddleware {
  constructor(private contextService: ContexteService) {}

  async handle(command: Command): Promise<void> {
    const correlationId = command.serialize().metadata.correlation_id;
    const contexte = this.contextService.get();
    command.correlationId = correlationId || (contexte ? contexte.correlationId : '');
  }
}
