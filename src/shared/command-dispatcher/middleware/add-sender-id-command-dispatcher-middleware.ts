import { Command } from '@/shared/command-dispatcher/Command';
import { CommandDispatcherMiddleware } from '@/shared/command-dispatcher/Command-dispatcher-middleware';
import { EnvironmentVariables } from '@/shared/domain/gateways/Environment-variables';

export class AddSenderIdCommandDispatcherMiddleware implements CommandDispatcherMiddleware {
  constructor(private environmentVariables: EnvironmentVariables) {}

  async handle(command: Command): Promise<void> {
    const senderId = command.serialize().metadata.sender_id;
    command.senderId = senderId || this.environmentVariables.APPLICATION_ID;
  }
}
