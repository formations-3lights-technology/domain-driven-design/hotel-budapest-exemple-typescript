export class ErrorCommandNotRegistered extends Error {
  constructor() {
    super('The dispatched command is not registered');
  }
}
