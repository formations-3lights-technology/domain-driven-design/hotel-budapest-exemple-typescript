import { Message, SerializedMessage } from '@/shared/domain/message';

export abstract class Command<D = {}> extends Message<{}, D> {
  constructor() {
    super('command');
  }

  abstract label(): string;

  abstract isPublic(): boolean;

  serialize(): SerializedMessage<{}, D> {
    return {
      metadata: {
        id: this._messageId,
        type: this.messageType,
        label: this.label(),
        timestamp: this._timestamp,
        correlation_id: this._correlationId,
        sender_id: this._senderId,
        bounded_context: this.boundedContext
      },
      data: this.serializedData()
    };
  }

  protected abstract serializedData(): D;
}
