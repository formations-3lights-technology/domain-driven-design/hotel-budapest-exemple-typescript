import { Command } from './Command';

export interface CommandDispatcherMiddleware {
  handle(command: Command): Promise<void>;
}
