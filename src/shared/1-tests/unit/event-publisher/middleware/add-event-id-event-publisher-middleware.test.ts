import { expect, fakeIdentityGenerator } from '@/1-tests/utils';
import { IdentityGenerator } from '@/shared/domain/gateways/Identity-generator';
import { DomainEvent } from '@/shared/event-publisher/Domain-event';
import { AddEventIdEventPublisherMiddleware } from '@/shared/event-publisher/middleware/add-event-id-event-publisher-middleware';
import { StubbedType } from '@salesforce/ts-sinon';

describe("Unit | Shared | Event Ajout de l'event id Middleware", () => {
  it("Ajout de l'event id", () => {
    // GIVEN
    const identifiantGenerateur: StubbedType<IdentityGenerator> = fakeIdentityGenerator();
    identifiantGenerateur.generate.returns('event-id');
    const event = new MyEvent();
    const eventPublisherMiddleware = new AddEventIdEventPublisherMiddleware(identifiantGenerateur);

    // WHEN
    eventPublisherMiddleware.listen(event);

    // THEN
    expect(event.serialize()).to.eql({
      metadata: {
        id: 'event-id',
        version: 'v1',
        timestamp: '',
        sender_id: '',
        bounded_context: 'test',
        type: 'event',
        label: MyEvent.type,
        correlation_id: '',
        aggregate: 'test',
        aggregate_id: 'test-id'
      },
      data: {}
    });
  });
});

class MyEvent extends DomainEvent {
  static readonly type = 'MyEvent';
  protected boundedContext = 'test';
  protected aggregate = 'test';

  constructor() {
    super('test-id');
  }

  label(): string {
    return MyEvent.type;
  }

  version(): string {
    return 'v1';
  }

  isPublic(): boolean {
    return false;
  }

  protected serializedData() {
    return {};
  }
}
