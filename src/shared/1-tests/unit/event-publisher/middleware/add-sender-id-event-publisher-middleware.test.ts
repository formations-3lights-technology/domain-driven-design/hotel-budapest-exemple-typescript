import { expect } from '@/1-tests/utils';
import { DomainEvent } from '@/shared/event-publisher/Domain-event';
import { AddSenderIdEventPublisherMiddleware } from '@/shared/event-publisher/middleware/add-sender-id-event-publisher-middleware';
import { NodeEnvironmentVariables } from '@/shared/infrastructure/gateways/node-environment-variables';

describe("Unit | Shared | Event Ajout de l'id de l'émetteur Middleware", () => {
  it("Ajout de l'id de l'émetteur", async () => {
    // GIVEN
    const event = new MyEvent();
    const eventPublisherMiddleware = new AddSenderIdEventPublisherMiddleware(new NodeEnvironmentVariables());

    // WHEN
    await eventPublisherMiddleware.listen(event);

    // THEN
    expect(event.serialize()).to.eql({
      metadata: {
        id: '',
        version: 'v1',
        timestamp: '',
        sender_id: 'hotel-budapest-api',
        bounded_context: 'test',
        type: 'event',
        label: MyEvent.type,
        correlation_id: '',
        aggregate: 'test',
        aggregate_id: 'test-id'
      },
      data: {}
    });
  });
});

class MyEvent extends DomainEvent {
  static readonly type = 'MyEvent';
  protected boundedContext = 'test';
  protected aggregate = 'test';

  constructor() {
    super('test-id');
  }

  label(): string {
    return MyEvent.type;
  }

  version(): string {
    return 'v1';
  }

  isPublic(): boolean {
    return false;
  }

  protected serializedData() {
    return {};
  }
}
