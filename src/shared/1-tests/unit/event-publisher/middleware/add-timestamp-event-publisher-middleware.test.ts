import { expect, fakeDateProvider } from '@/1-tests/utils';
import { DateProvider } from '@/shared/domain/gateways/Date-provider';
import { DomainEvent } from '@/shared/event-publisher/Domain-event';
import { AddTimestampEventPublisherMiddleware } from '@/shared/event-publisher/middleware/add-timestamp-event-publisher-middleware';
import { StubbedType } from '@salesforce/ts-sinon';

describe('Unit | Shared | Event Ajout du timestamp Middleware', () => {
  it('Ajout du timestamp', () => {
    // GIVEN
    const dateProvider: StubbedType<DateProvider> = fakeDateProvider();
    dateProvider.now.returns(new Date('2020-12-02T06:33:00+01:00'));
    const event = new MyEvent();
    const eventPublisherMiddleware = new AddTimestampEventPublisherMiddleware(dateProvider);

    // WHEN
    eventPublisherMiddleware.listen(event);

    // THEN
    expect(event.serialize()).to.eql({
      metadata: {
        id: '',
        version: 'v1',
        timestamp: '2020-12-02T05:33:00.000Z',
        sender_id: '',
        bounded_context: 'test',
        type: 'event',
        label: MyEvent.type,
        correlation_id: '',
        aggregate: 'test',
        aggregate_id: 'test-id'
      },
      data: {}
    });
  });
});

class MyEvent extends DomainEvent {
  static readonly type = 'MyEvent';
  protected boundedContext = 'test';
  protected aggregate = 'test';

  constructor() {
    super('test-id');
  }

  label(): string {
    return MyEvent.type;
  }

  version(): string {
    return 'v1';
  }

  isPublic(): boolean {
    return false;
  }

  protected serializedData() {
    return {};
  }
}
