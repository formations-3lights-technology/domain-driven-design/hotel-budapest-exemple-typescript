import { expect, fakeContextService } from '@/1-tests/utils';
import { ContexteService } from '@/shared/domain/gateways/Contexte-service';
import { DomainEvent } from '@/shared/event-publisher/Domain-event';
import { EventPublisherMiddleware } from '@/shared/event-publisher/Event-publisher-middleware';
import { AddCorrelationIdEventPublisherMiddleware } from '@/shared/event-publisher/middleware/add-correlation-id-event-publisher-middleware';
import { StubbedType } from '@salesforce/ts-sinon';

describe('Unit | Shared | Event Ajout du correlation id Middleware', () => {
  let contextService: StubbedType<ContexteService>;
  let eventPublisherMiddleware: EventPublisherMiddleware;

  beforeEach(() => {
    contextService = fakeContextService();
    eventPublisherMiddleware = new AddCorrelationIdEventPublisherMiddleware(contextService);
  });

  it('Ajout du correlation id event', async () => {
    // GIVEN
    const event = new MyEvent();

    // WHEN
    await eventPublisherMiddleware.listen(event);

    // THEN
    expect(event.serialize()).to.eql({
      metadata: {
        id: '',
        version: 'v1',
        timestamp: '',
        sender_id: '',
        bounded_context: 'test',
        type: 'event',
        label: MyEvent.type,
        correlation_id: 'correlationId',
        aggregate: 'test',
        aggregate_id: 'test-id'
      },
      data: {}
    });
  });

  it('Correlation id introuvable', async () => {
    // GIVEN
    const event = new MyEvent();
    contextService.get.returns(null);
    const eventPublisherMiddleware = new AddCorrelationIdEventPublisherMiddleware(contextService);

    // WHEN
    await eventPublisherMiddleware.listen(event);

    // THEN
    expect(event.serialize()).to.eql({
      metadata: {
        id: '',
        version: 'v1',
        timestamp: '',
        sender_id: '',
        bounded_context: 'test',
        type: 'event',
        label: MyEvent.type,
        correlation_id: '',
        aggregate: 'test',
        aggregate_id: 'test-id'
      },
      data: {}
    });
  });
});

class MyEvent extends DomainEvent {
  static readonly type = 'MyEvent';
  protected boundedContext = 'test';
  protected aggregate = 'test';

  constructor() {
    super('test-id');
  }

  label(): string {
    return MyEvent.type;
  }

  version(): string {
    return 'v1';
  }

  isPublic(): boolean {
    return false;
  }

  protected serializedData() {
    return {};
  }
}
