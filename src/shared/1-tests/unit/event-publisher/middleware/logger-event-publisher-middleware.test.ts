import { expect, fakeApiLogger } from '@/1-tests/utils';
import { ApiLogger } from '@/shared/domain/gateways/Api-logger';
import { DomainEvent } from '@/shared/event-publisher/Domain-event';
import { LoggerEventPublisherMiddleware } from '@/shared/event-publisher/middleware/logger-event-publisher-middleware';
import { StubbedType } from '@salesforce/ts-sinon';

describe('Unit | Shared | Event logger Middleware', () => {
  it('Log event', () => {
    const consoleSpy: StubbedType<ApiLogger> = fakeApiLogger();
    const eventPublisherMiddleware = new LoggerEventPublisherMiddleware(consoleSpy);

    eventPublisherMiddleware.listen(new MyEvent(1, 'two', true));

    expect(consoleSpy.info).to.have.been.calledOnceWithExactly(MyEvent.type, {
      metadata: {
        id: '',
        version: 'v1',
        timestamp: '',
        sender_id: '',
        bounded_context: 'test',
        type: 'event',
        label: MyEvent.type,
        correlation_id: '',
        aggregate: 'test',
        aggregate_id: 'test-id'
      },
      data: {
        field1: 1,
        field2: 'two',
        field3: true
      }
    });
  });
});

interface MyEventSerialized {
  field1: number;
  field2: string;
  field3: boolean;
}

class MyEvent extends DomainEvent<MyEventSerialized> {
  static readonly type = 'MyEvent';
  protected boundedContext = 'test';
  protected aggregate = 'test';

  constructor(private field1: number, private field2: string, private field3: boolean) {
    super('test-id');
  }

  label(): string {
    return MyEvent.type;
  }

  version(): string {
    return 'v1';
  }

  isPublic(): boolean {
    return false;
  }

  protected serializedData(): MyEventSerialized {
    return {
      field1: this.field1,
      field2: this.field2,
      field3: this.field3
    };
  }
}
