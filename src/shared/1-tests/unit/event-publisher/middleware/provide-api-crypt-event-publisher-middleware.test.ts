import { expect, fakeApiCrypt } from '@/1-tests/utils';
import { DomainEvent } from '@/shared/event-publisher/Domain-event';
import { ProvideApiCryptEventPublisherMiddleware } from '@/shared/event-publisher/middleware/provide-api-crypt-event-publisher-middleware';

describe('Unit | Shared | Event Provide api crypt Middleware', () => {
  it('Provide api crypt', () => {
    const apiCrypt = fakeApiCrypt;
    const event = new MyEvent(1, 'two', true);
    const eventPublisherMiddleware = new ProvideApiCryptEventPublisherMiddleware(apiCrypt);

    eventPublisherMiddleware.listen(event);

    expect(event.serialize()).to.eql({
      metadata: {
        id: '',
        version: 'v1',
        timestamp: '',
        sender_id: '',
        bounded_context: 'test',
        type: 'event',
        label: MyEvent.type,
        correlation_id: '',
        aggregate: 'test',
        aggregate_id: 'test-id'
      },
      data: {
        field1: '#1#',
        field2: '#two#',
        field3: '#true#'
      }
    });
  });
});

interface MyEventSerialized {
  field1: string;
  field2: string;
  field3: string;
}

class MyEvent extends DomainEvent<MyEventSerialized> {
  static readonly type = 'MyEvent';
  protected boundedContext = 'test';
  protected aggregate = 'test';

  constructor(private field1: number, private field2: string, private field3: boolean) {
    super('test-id');
  }

  label(): string {
    return MyEvent.type;
  }

  version(): string {
    return 'v1';
  }

  isPublic(): boolean {
    return false;
  }

  protected serializedData(): MyEventSerialized {
    return {
      field1: this._apiCrypt.encrypt(this.field1),
      field2: this._apiCrypt.encrypt(this.field2),
      field3: this._apiCrypt.encrypt(this.field3)
    };
  }
}
