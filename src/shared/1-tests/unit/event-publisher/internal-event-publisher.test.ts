import { expect, fakeApiLogger } from '@/1-tests/utils';
import { ApiLogger } from '@/shared/domain/gateways/Api-logger';
import { DomainEvent } from '@/shared/event-publisher/Domain-event';
import { EventListener } from '@/shared/event-publisher/Event-listener';
import { EventPublisherMiddleware } from '@/shared/event-publisher/Event-publisher-middleware';
import { EventPublisher, InternalEventPublisher } from '@/shared/event-publisher/internal-event-publisher';
import { StubbedType, stubInterface } from '@salesforce/ts-sinon';
import sinon from 'sinon';

describe('Unit | Shared | internal event publisher', () => {
  let apiLogger: StubbedType<ApiLogger>;
  let eventPublisherMiddleWare: StubbedType<EventPublisherMiddleware>;
  let eventPublisher: EventPublisher;

  beforeEach(() => {
    apiLogger = fakeApiLogger();
    eventPublisherMiddleWare = stubInterface<EventPublisherMiddleware>(sinon, { label: 'eventPublisherMiddleWare' });
    eventPublisherMiddleWare.isSilent.returns(true);
    eventPublisher = new InternalEventPublisher(apiLogger);
  });

  describe('Publier les événements aux bons listeners', () => {
    it('Tout se passe bien', async () => {
      // GIVEN
      const event = new MyEvent();
      const event2 = new MyEvent2('Event create');
      const unknownEvent = new UnknownEvent();
      const eventListener: StubbedType<EventListener> = stubInterface<EventListener>(sinon, { label: 'eventListener' });
      const eventListener2: StubbedType<EventListener> = stubInterface<EventListener>(sinon, {
        label: 'eventListener2'
      });
      const eventListener3: StubbedType<EventListener> = stubInterface<EventListener>(sinon, {
        label: 'eventListener3'
      });
      const eventListener4: StubbedType<EventListener> = stubInterface<EventListener>(sinon, {
        label: 'eventListener4'
      });
      const eventListener5: StubbedType<EventListener> = stubInterface<EventListener>(sinon, {
        label: 'eventListener5'
      });
      eventPublisher.registerListeners(
        { [MyEvent.type]: [eventListener, eventListener3] },
        { [MyEvent2.type]: [eventListener4] },
        {
          [MyEvent2.type]: [eventListener2],
          [MyEvent.type]: [eventListener5]
        }
      );
      // WHEN
      await eventPublisher.publish([event, event2]);
      await eventPublisher.publish([unknownEvent]);
      // THEN
      expect(eventListener.listen).to.have.been.calledOnceWithExactly(event);
      expect(eventListener2.listen).to.have.been.calledOnceWithExactly(event2);
      expect(eventListener3.listen).to.have.been.calledOnceWithExactly(event);
      expect(eventListener4.listen).to.have.been.calledOnceWithExactly(event2);
      expect(eventListener5.listen).to.have.been.calledOnceWithExactly(event);
    });

    it('Un listener fait une erreur', async () => {
      // GIVEN
      const event = new MyEvent();
      const eventListener: StubbedType<EventListener> = stubInterface<EventListener>(sinon, { label: 'eventListener' });
      eventListener.listen.rejects(new Error('erreur'));
      const eventListener2: StubbedType<EventListener> = stubInterface<EventListener>(sinon, {
        label: 'eventListener'
      });
      eventPublisher.registerListeners({ [MyEvent.type]: [eventListener, eventListener2] });
      // WHEN
      await eventPublisher.publish([event]);
      // THEN
      expect(eventListener.listen).to.have.been.calledOnceWithExactly(event);
      expect(eventListener2.listen).to.have.been.calledOnceWithExactly(event);
      expect(apiLogger.error).to.have.been.calledOnceWithExactly('eventListener: erreur');
    });
  });

  it('Evènement consommé par le middleware', async () => {
    // GIVEN
    const event = new MyEvent();
    const eventListener: StubbedType<EventListener> = stubInterface<EventListener>(sinon, { label: 'eventListener' });
    eventPublisher
      .registerListeners({
        [MyEvent.type]: [eventListener]
      })
      .registerMiddleware([eventPublisherMiddleWare]);
    // WHEN
    await eventPublisher.publish([event]);
    // THEN
    expect(eventPublisherMiddleWare.listen).to.have.been.calledOnceWithExactly(event);
  });

  describe('Un middleware fait une erreur', () => {
    it('Un middleware standard fait une erreur', async () => {
      // GIVEN
      const event = new MyEvent();
      const eventListener: StubbedType<EventListener> = stubInterface<EventListener>(sinon, { label: 'eventListener' });
      const eventPublisherMiddleWareErreur = stubInterface<EventPublisherMiddleware>(sinon, {
        label: 'eventPublisherMiddleWareErreur'
      });
      eventPublisherMiddleWareErreur.isSilent.returns(true);
      eventPublisherMiddleWareErreur.listen.rejects(new Error('erreur'));
      eventPublisher
        .registerListeners({ [MyEvent.type]: [eventListener] })
        .registerMiddleware([eventPublisherMiddleWareErreur, eventPublisherMiddleWare]);
      // WHEN
      await eventPublisher.publish([event]);
      // THEN
      expect(eventPublisherMiddleWare.listen).to.have.been.calledOnceWithExactly(event);
      expect(apiLogger.error).to.have.been.calledOnceWithExactly('eventPublisherMiddleWareErreur: erreur');
    });

    it('Un middleware cassant le flow fait une erreur', async () => {
      // GIVEN
      const event = new MyEvent();
      const eventListener: StubbedType<EventListener> = stubInterface<EventListener>(sinon, { label: 'eventListener' });
      const erreur = new Error('erreur');
      const eventPublisherMiddleWareErreur = stubInterface<EventPublisherMiddleware>(sinon, {
        label: 'eventPublisherMiddleWareErreur'
      });
      eventPublisherMiddleWare.isSilent.returns(false);
      eventPublisherMiddleWare.listen.rejects(erreur);
      eventPublisher
        .registerListeners({ [MyEvent.type]: [eventListener] })
        .registerMiddleware([eventPublisherMiddleWareErreur, eventPublisherMiddleWare]);

      // WHEN THEN
      await expect(eventPublisher.publish([event])).to.be.rejectedWith(erreur);
    });
  });
});

export class MyEvent extends DomainEvent {
  static readonly type = 'MyEvent';
  protected boundedContext = 'test';
  protected aggregate = 'test';

  constructor() {
    super('test-id');
  }

  label(): string {
    return MyEvent.type;
  }

  version(): string {
    return 'v1';
  }

  isPublic(): boolean {
    return false;
  }

  protected serializedData(): {} {
    return {};
  }
}

export class MyEvent2 extends DomainEvent {
  static readonly type = 'MyEvent2';
  protected boundedContext = 'test';
  protected aggregate = 'test';

  constructor(readonly title: string) {
    super('test-id');
  }

  label(): string {
    return MyEvent2.type;
  }

  version(): string {
    return 'v1';
  }

  isPublic(): boolean {
    return false;
  }

  protected serializedData(): {} {
    return {};
  }
}

export class UnknownEvent extends DomainEvent {
  static readonly type = 'UnknownEvent';
  protected boundedContext = 'test';
  protected aggregate = 'test';

  constructor() {
    super('test-id');
  }

  label(): string {
    return UnknownEvent.type;
  }

  version(): string {
    return 'v1';
  }

  isPublic(): boolean {
    return false;
  }

  protected serializedData(): {} {
    return {};
  }
}
