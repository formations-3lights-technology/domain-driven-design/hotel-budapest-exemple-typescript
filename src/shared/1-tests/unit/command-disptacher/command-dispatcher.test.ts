import { expect, fakeTransactionPerformer } from '@/1-tests/utils';
import { Command } from '@/shared/command-dispatcher/Command';
import { CommandDispatcherMiddleware } from '@/shared/command-dispatcher/Command-dispatcher-middleware';
import { CommandHandler } from '@/shared/command-dispatcher/Command-handler';
import { ErrorCommandNotRegistered } from '@/shared/command-dispatcher/errors/error-command-not-registered';
import { CommandDispatcher, InternalCommandDispatcher } from '@/shared/command-dispatcher/internal-command-dispatcher';
import { StubbedType, stubInterface } from '@salesforce/ts-sinon';
import sinon from 'sinon';

describe('Unit | Shared | Commandes dispatcher', () => {
  let commandDispatcher: CommandDispatcher;

  beforeEach(() => {
    commandDispatcher = new InternalCommandDispatcher(fakeTransactionPerformer());
  });

  it('Dispatch des commandes enregistrées dans le bus', async () => {
    // GIVEN
    const command = new MyCommand();
    const publicCommand = new MyPublicCommand();
    const command2 = new MyCommand2('hello world');
    const commandHandler: StubbedType<CommandHandler> = stubInterface<CommandHandler>(sinon);
    const commandHandler2 = new CommandHandler2();
    commandDispatcher.registerHandlers({
      [MyCommand.type]: commandHandler,
      [MyCommand2.type]: commandHandler2
    });

    // WHEN
    await commandDispatcher.dispatch(command);
    await commandDispatcher.dispatch(publicCommand);
    const result = await commandDispatcher.dispatch<string>(command2);

    // THEN
    expect(commandHandler.handle).to.have.been.calledOnceWithExactly(command);
    expect(commandHandler2.isCalledWithMyTest2Command).to.be.true();
    expect(result).to.eql('hello world');
  });

  it('Dispatch une commande non enregistrée dans le bus', async () => {
    // GIVEN
    const unknownCommand = new UnknownCommand();

    // WHEN THEN
    await expect(commandDispatcher.dispatch(unknownCommand)).to.be.rejectedWith(
      ErrorCommandNotRegistered,
      'The dispatched command is not registered'
    );
  });

  it('Commande consommée par un middleware', async () => {
    // GIVEN
    const command = new MyCommand();
    const commandHandler: CommandHandler = {
      handle: sinon.spy()
    };
    const commandDispatcherMiddleware: CommandDispatcherMiddleware = {
      handle: sinon.spy()
    };
    commandDispatcher
      .registerHandlers({ [MyCommand.type]: commandHandler })
      .registerMiddleware([commandDispatcherMiddleware]);

    // WHEN
    await commandDispatcher.dispatch(command);

    // THEN
    expect(commandDispatcherMiddleware.handle).to.have.been.calledOnceWithExactly(command);
  });

  it('Un middleware fait une erreur', async () => {
    // GIVEN
    const command = new MyCommand();
    const commandHandler: CommandHandler = {
      handle: sinon.spy()
    };
    const commandDispatcherMiddleware: CommandDispatcherMiddleware = {
      handle: sinon.stub().rejects(new Error())
    };
    commandDispatcher
      .registerHandlers({ [MyCommand.type]: commandHandler })
      .registerMiddleware([commandDispatcherMiddleware]);

    // WHEN THEN
    await expect(commandDispatcher.dispatch(command)).to.be.rejectedWith(Error);
  });
});

class MyCommand extends Command {
  static readonly type = 'MyCommand';
  protected boundedContext = 'test';

  label(): string {
    return MyCommand.type;
  }

  isPublic(): boolean {
    return true;
  }

  protected serializedData(): {} {
    return {};
  }
}

class MyCommand2 extends Command {
  static readonly type = 'MyCommand2';
  protected boundedContext = 'test';

  constructor(readonly title: string) {
    super();
  }

  label(): string {
    return MyCommand2.type;
  }

  isPublic(): boolean {
    return false;
  }

  protected serializedData(): {} {
    return {};
  }
}

class MyPublicCommand extends Command {
  static readonly type = 'MyPublicCommand';
  protected boundedContext = 'test';

  label(): string {
    return MyPublicCommand.type;
  }

  isPublic(): boolean {
    return true;
  }

  protected serializedData(): {} {
    return {};
  }
}

class CommandHandler2 implements CommandHandler<string> {
  isCalledWithMyTest2Command = false;

  async handle(command: MyCommand2): Promise<string> {
    this.isCalledWithMyTest2Command = true;
    return command.title;
  }
}

class UnknownCommand extends Command {
  static readonly type = 'UnknownCommand';
  protected boundedContext = 'test';

  label(): string {
    return UnknownCommand.type;
  }

  isPublic(): boolean {
    return false;
  }

  protected serializedData(): {} {
    return {};
  }
}
