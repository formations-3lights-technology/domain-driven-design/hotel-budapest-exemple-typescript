import { expect, fakeIdentityGenerator } from '@/1-tests/utils';
import { Command } from '@/shared/command-dispatcher/Command';
import { AddCommandIdCommandDispatcherMiddleware } from '@/shared/command-dispatcher/middleware/add-command-id-command-dispatcher-middleware';
import { IdentityGenerator } from '@/shared/domain/gateways/Identity-generator';
import { StubbedType } from '@salesforce/ts-sinon';

describe('Unit | Shared | Command Ajout de la command id Middleware', () => {
  it('Ajout de la command id', () => {
    // GIVEN
    const identifiantGenerateur: StubbedType<IdentityGenerator> = fakeIdentityGenerator();
    identifiantGenerateur.generate.returns('command-id');
    const command = new MyCommand();
    const commandDispatcherMiddleware = new AddCommandIdCommandDispatcherMiddleware(identifiantGenerateur);
    // WHEN
    commandDispatcherMiddleware.handle(command);
    // THEN
    expect(command.serialize()).to.eql({
      metadata: {
        id: 'command-id',
        timestamp: '',
        sender_id: '',
        bounded_context: 'test',
        type: 'command',
        label: MyCommand.label,
        correlation_id: ''
      },
      data: {}
    });
  });

  it('Ne pas ajouter si la command id est déjà renseignée', () => {
    // GIVEN
    const identifiantGenerateur: StubbedType<IdentityGenerator> = fakeIdentityGenerator();
    identifiantGenerateur.generate.returns('command-id');
    const command = new MyCommand();
    command.messageId = 'message-id';
    const commandDispatcherMiddleware = new AddCommandIdCommandDispatcherMiddleware(identifiantGenerateur);
    // WHEN
    commandDispatcherMiddleware.handle(command);
    // THEN
    expect(command.serialize()).to.eql({
      metadata: {
        id: 'message-id',
        timestamp: '',
        sender_id: '',
        bounded_context: 'test',
        type: 'command',
        label: MyCommand.label,
        correlation_id: ''
      },
      data: {}
    });
  });
});

class MyCommand extends Command {
  static readonly label = 'MyCommand';
  protected boundedContext = 'test';

  label(): string {
    return MyCommand.label;
  }

  isPublic(): boolean {
    return false;
  }

  protected serializedData() {
    return {};
  }
}
