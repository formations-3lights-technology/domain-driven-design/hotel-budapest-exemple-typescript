import { expect, fakeDateProvider } from '@/1-tests/utils';
import { Command } from '@/shared/command-dispatcher/Command';
import { AddTimestampCommandDispatcherMiddleware } from '@/shared/command-dispatcher/middleware/add-timestamp-command-dispatcher-middleware';
import { DateProvider } from '@/shared/domain/gateways/Date-provider';
import { StubbedType } from '@salesforce/ts-sinon';

describe('Unit | Shared | Command Ajout du timestamp Middleware', () => {
  it('Ajout du timestamp', () => {
    // GIVEN
    const dateProvider: StubbedType<DateProvider> = fakeDateProvider();
    dateProvider.now.returns(new Date('2020-12-02T06:33:00+01:00'));
    const command = new MyCommand();
    const commandDispatcherMiddleware = new AddTimestampCommandDispatcherMiddleware(dateProvider);

    // WHEN
    commandDispatcherMiddleware.handle(command);

    // THEN
    expect(command.serialize()).to.eql({
      metadata: {
        id: '',
        timestamp: '2020-12-02T05:33:00.000Z',
        sender_id: '',
        bounded_context: 'test',
        type: 'command',
        label: MyCommand.label,
        correlation_id: ''
      },
      data: {}
    });
  });

  it('Ne pas ajouter si le timestamp est déjà renseigné', () => {
    // GIVEN
    const dateProvider: StubbedType<DateProvider> = fakeDateProvider();
    dateProvider.now.returns(new Date('2020-12-02T06:33:00+01:00'));
    const command = new MyCommand();
    command.timestamp = '2020-12-03T05:33:00.000Z';
    const commandDispatcherMiddleware = new AddTimestampCommandDispatcherMiddleware(dateProvider);

    // WHEN
    commandDispatcherMiddleware.handle(command);

    // THEN
    expect(command.serialize()).to.eql({
      metadata: {
        id: '',
        timestamp: '2020-12-03T05:33:00.000Z',
        sender_id: '',
        bounded_context: 'test',
        type: 'command',
        label: MyCommand.label,
        correlation_id: ''
      },
      data: {}
    });
  });
});

class MyCommand extends Command {
  static readonly label = 'MyCommand';
  protected boundedContext = 'test';

  label(): string {
    return MyCommand.label;
  }

  isPublic(): boolean {
    return false;
  }

  protected serializedData() {
    return {};
  }
}
