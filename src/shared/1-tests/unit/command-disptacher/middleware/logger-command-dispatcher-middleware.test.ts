import { expect, fakeApiLogger } from '@/1-tests/utils';
import { Command } from '@/shared/command-dispatcher/Command';
import { LoggerCommandDispatcherMiddleware } from '@/shared/command-dispatcher/middleware/logger-command-dispatcher-middleware';
import { ApiLogger } from '@/shared/domain/gateways/Api-logger';
import { StubbedType } from '@salesforce/ts-sinon';

describe('Unit | Shared | Command logger Middleware', () => {
  it('Log command', () => {
    const consoleSpy: StubbedType<ApiLogger> = fakeApiLogger();
    const commandDispatcherMiddleware = new LoggerCommandDispatcherMiddleware(consoleSpy);

    commandDispatcherMiddleware.handle(new MyCommand(1, 'two', true));

    expect(consoleSpy.info).to.have.been.calledOnceWithExactly(MyCommand.label, {
      metadata: {
        id: '',
        timestamp: '',
        sender_id: '',
        bounded_context: 'test',
        type: 'command',
        label: MyCommand.label,
        correlation_id: ''
      },
      data: {
        field1: 1,
        field2: 'two',
        field3: true
      }
    });
  });
});

interface MyCommandSerialized {
  field1: number;
  field2: string;
  field3: boolean;
}

class MyCommand extends Command<MyCommandSerialized> {
  static readonly label = 'MyCommand';
  protected boundedContext = 'test';

  constructor(private field1: number, private field2: string, private field3: boolean) {
    super();
  }

  label(): string {
    return MyCommand.label;
  }

  isPublic(): boolean {
    return false;
  }

  protected serializedData(): MyCommandSerialized {
    return {
      field1: this.field1,
      field2: this.field2,
      field3: this.field3
    };
  }
}
