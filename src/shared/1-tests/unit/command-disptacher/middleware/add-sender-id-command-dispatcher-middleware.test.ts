import { expect } from '@/1-tests/utils';
import { Command } from '@/shared/command-dispatcher/Command';
import { AddSenderIdCommandDispatcherMiddleware } from '@/shared/command-dispatcher/middleware/add-sender-id-command-dispatcher-middleware';
import { NodeEnvironmentVariables } from '@/shared/infrastructure/gateways/node-environment-variables';

describe("Unit | Shared | Command Ajout de l'id de l'émetteur Middleware", () => {
  it("Ajout de l'id de l'émetteur", async () => {
    // GIVEN
    const command = new MyCommand();
    const commandDispatcherMiddleware = new AddSenderIdCommandDispatcherMiddleware(new NodeEnvironmentVariables());
    // WHEN
    await commandDispatcherMiddleware.handle(command);
    // THEN
    expect(command.serialize()).to.eql({
      metadata: {
        id: '',
        timestamp: '',
        sender_id: 'hotel-budapest-api',
        bounded_context: 'test',
        type: 'command',
        label: MyCommand.label,
        correlation_id: ''
      },
      data: {}
    });
  });

  it("Ne pas ajouter si l'id de l'émetteur est déjà renseigné", async () => {
    // GIVEN
    const command = new MyCommand();
    command.senderId = 'sender-id';
    const commandDispatcherMiddleware = new AddSenderIdCommandDispatcherMiddleware(new NodeEnvironmentVariables());
    // WHEN
    await commandDispatcherMiddleware.handle(command);
    // THEN
    expect(command.serialize()).to.eql({
      metadata: {
        id: '',
        timestamp: '',
        sender_id: 'sender-id',
        bounded_context: 'test',
        type: 'command',
        label: MyCommand.label,
        correlation_id: ''
      },
      data: {}
    });
  });
});

class MyCommand extends Command {
  static readonly label = 'MyCommand';
  protected boundedContext = 'test';

  label(): string {
    return MyCommand.label;
  }

  isPublic(): boolean {
    return false;
  }

  protected serializedData() {
    return {};
  }
}
