import { expect, fakeApiCrypt } from '@/1-tests/utils';
import { Command } from '@/shared/command-dispatcher/Command';
import { ProvideApiCryptCommandDispatcherMiddleware } from '@/shared/command-dispatcher/middleware/provide-api-crypt-command-dispatcher-middleware';

describe('Unit | Shared | Command Provide api crypt Middleware', () => {
  it('Provide api crypt', () => {
    const apiCrypt = fakeApiCrypt;
    const command = new MyCommand(1, 'two', true);
    const commandDispatcherMiddleware = new ProvideApiCryptCommandDispatcherMiddleware(apiCrypt);

    commandDispatcherMiddleware.handle(command);

    expect(command.serialize()).to.eql({
      metadata: {
        id: '',
        timestamp: '',
        sender_id: '',
        bounded_context: 'test',
        type: 'command',
        label: MyCommand.label,
        correlation_id: ''
      },
      data: {
        field1: '#1#',
        field2: '#two#',
        field3: '#true#'
      }
    });
  });
});

interface MyCommandSerialized {
  field1: string;
  field2: string;
  field3: string;
}

class MyCommand extends Command<MyCommandSerialized> {
  static readonly label = 'MyCommand';
  protected boundedContext = 'test';

  constructor(private field1: number, private field2: string, private field3: boolean) {
    super();
  }

  label(): string {
    return MyCommand.label;
  }

  isPublic(): boolean {
    return false;
  }

  protected serializedData(): MyCommandSerialized {
    return {
      field1: this._apiCrypt.encrypt(this.field1),
      field2: this._apiCrypt.encrypt(this.field2),
      field3: this._apiCrypt.encrypt(this.field3)
    };
  }
}
