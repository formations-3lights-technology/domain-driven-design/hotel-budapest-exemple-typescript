import { expect, fakeContextService } from '@/1-tests/utils';
import { Command } from '@/shared/command-dispatcher/Command';
import { CommandDispatcherMiddleware } from '@/shared/command-dispatcher/Command-dispatcher-middleware';
import { AddCorrelationIdCommandDispatcherMiddleware } from '@/shared/command-dispatcher/middleware/add-correlation-id-command-dispatcher-middleware';
import { ContexteService } from '@/shared/domain/gateways/Contexte-service';
import { StubbedType } from '@salesforce/ts-sinon';

describe('Unit | Shared | Command Ajout du correlation id Middleware', () => {
  let contextService: StubbedType<ContexteService>;
  let commandDispatcherMiddleware: CommandDispatcherMiddleware;

  beforeEach(() => {
    contextService = fakeContextService();
    commandDispatcherMiddleware = new AddCorrelationIdCommandDispatcherMiddleware(contextService);
  });

  it('Ajout du correlation id command', async () => {
    // GIVEN
    const command = new MyCommand();

    // WHEN
    await commandDispatcherMiddleware.handle(command);

    // THEN
    expect(command.serialize()).to.eql({
      metadata: {
        id: '',
        timestamp: '',
        sender_id: '',
        bounded_context: 'test',
        type: 'command',
        label: MyCommand.label,
        correlation_id: 'correlationId'
      },
      data: {}
    });
  });

  it('Ne pas ajouter si le correlation id est déjà renseigné', async () => {
    // GIVEN
    const command = new MyCommand();
    command.correlationId = 'correlation-id';

    // WHEN
    await commandDispatcherMiddleware.handle(command);

    // THEN
    expect(command.serialize()).to.eql({
      metadata: {
        id: '',
        timestamp: '',
        sender_id: '',
        bounded_context: 'test',
        type: 'command',
        label: MyCommand.label,
        correlation_id: 'correlation-id'
      },
      data: {}
    });
  });

  it('Correlation id introuvable', async () => {
    // GIVEN
    const command = new MyCommand();
    contextService.get.returns(null);

    // WHEN
    await commandDispatcherMiddleware.handle(command);

    // THEN
    expect(command.serialize()).to.eql({
      metadata: {
        id: '',
        timestamp: '',
        sender_id: '',
        bounded_context: 'test',
        type: 'command',
        label: MyCommand.label,
        correlation_id: ''
      },
      data: {}
    });
  });
});

class MyCommand extends Command {
  static readonly label = 'MyCommand';
  protected boundedContext = 'test';

  label(): string {
    return MyCommand.label;
  }

  isPublic(): boolean {
    return false;
  }

  protected serializedData(): {} {
    return {};
  }
}
