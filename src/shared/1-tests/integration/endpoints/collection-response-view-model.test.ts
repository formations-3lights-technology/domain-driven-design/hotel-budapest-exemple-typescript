import { CollectionResponseViewModel } from '@/shared/infrastructure/endpoints/collection-response/collection-response-view-model';
import { PaginationParameter } from '@/shared/infrastructure/endpoints/collection-response/Pagination-parameter';
import { expect } from 'chai';

describe('Integration | Shared | Collection response view model', () => {
  describe("il existe plus d'items que le nombre listé", () => {
    it('retourne le code 206 et un content-range partiel', () => {
      // GIVEN
      const collection = [{ id: 'id', name: 'name' }];
      const allItemsCount = 90;
      const collectionName = 'collectionName';
      const paginationParameter: PaginationParameter = { limit: 20, page: 4 };
      // WHEN
      const viewModel = new CollectionResponseViewModel(collectionName, allItemsCount, collection, paginationParameter);
      // THEN
      expect(viewModel.statusCode).to.eql(206);
      expect(viewModel.contentRange).to.eql('collectionName 61-80/90');
      expect(viewModel.data).to.eql(collection);
    });

    it('retourne le code 206 et un content-range partiel de la dernière page', () => {
      // GIVEN
      const collection = [{ id: 'id', name: 'name' }];
      const allItemsCount = 90;
      const collectionName = 'collectionName';
      const paginationParameter: PaginationParameter = { limit: 20, page: 5 };
      // WHEN
      const viewModel = new CollectionResponseViewModel(collectionName, allItemsCount, collection, paginationParameter);
      // THEN
      expect(viewModel.statusCode).to.eql(206);
      expect(viewModel.contentRange).to.eql('collectionName 81-90/90');
      expect(viewModel.data).to.eql(collection);
    });
  });

  describe("il y a autant d'items que le nombre listé (4 items)", () => {
    it('retourne le code 200 et un content-range associé', () => {
      // GIVEN
      const collection = [
        { id: 'id', name: 'name' },
        { id: 'i2', name: 'name2' },
        { id: 'i3', name: 'name3' },
        { id: 'i4', name: 'name4' }
      ];
      const allItemsCount = 4;
      const collectionName = 'collectionName';
      const paginationParameter: PaginationParameter = { limit: 100, page: 1 };
      // WHEN
      const viewModel = new CollectionResponseViewModel(collectionName, allItemsCount, collection, paginationParameter);
      // THEN
      expect(viewModel.statusCode).to.eql(200);
      expect(viewModel.contentRange).to.eql('collectionName 1-4/4');
      expect(viewModel.data).to.eql(collection);
    });

    it("retourne le code 206 et un content-range associé pour au-delà du nombre d'éléments", () => {
      // GIVEN
      const collection = [
        { id: 'id', name: 'name' },
        { id: 'i2', name: 'name2' },
        { id: 'i3', name: 'name3' },
        { id: 'i4', name: 'name4' }
      ];
      const allItemsCount = 10;
      const collectionName = 'collectionName';
      const paginationParameter: PaginationParameter = { limit: 100, page: 2 };
      // WHEN
      const viewModel = new CollectionResponseViewModel(collectionName, allItemsCount, collection, paginationParameter);
      // THEN
      expect(viewModel.statusCode).to.eql(206);
      expect(viewModel.contentRange).to.eql('collectionName 0/10');
      expect(viewModel.data).to.eql(collection);
    });
  });

  describe("il y a autant d'items que le nombre listé (1 item)", () => {
    it('retourne le code 200 et un content-range 1/1', () => {
      // GIVEN
      const collection = [{ id: 'id', name: 'name' }];
      const allItemsCount = 1;
      const collectionName = 'collectionName';
      const paginationParameter: PaginationParameter = { limit: 1, page: 1 };
      // WHEN
      const viewModel = new CollectionResponseViewModel(collectionName, allItemsCount, collection, paginationParameter);
      // THEN
      expect(viewModel.statusCode).to.eql(200);
      expect(viewModel.contentRange).to.eql('collectionName 1/1');
      expect(viewModel.data).to.eql(collection);
    });
  });

  describe('il y a 0 item', () => {
    it('retourne le code 200 et un content-range vide', () => {
      // GIVEN
      const collection = [];
      const allItemsCount = 0;
      const collectionName = 'collectionName';
      const paginationParameter: PaginationParameter = { limit: 1, page: 11 };
      // WHEN
      const viewModel = new CollectionResponseViewModel(collectionName, allItemsCount, collection, paginationParameter);
      // THEN
      expect(viewModel.statusCode).to.eql(200);
      expect(viewModel.contentRange).to.eql('collectionName 0/0');
      expect(viewModel.data).to.eql(collection);
    });
  });
});
