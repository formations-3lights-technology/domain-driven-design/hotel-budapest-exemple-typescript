import { expect } from '@/1-tests/utils';
import { ContexteService } from '@/shared/domain/gateways/Contexte-service';
import { AsyncLocalContexteService } from '@/shared/infrastructure/gateways/async-local-context-service';

describe('Integration | Shared | Async local context service', () => {
  let contextService: ContexteService;

  beforeEach(() => {
    contextService = new AsyncLocalContexteService();
  });

  it('Par défaut pas de session', () => {
    // THEN
    expect(contextService.get()).to.be.null();
  });

  it('Session initialisée', () => {
    // GIVEN
    const correlationId = 'correlationId';
    const callback = () => {
      expect(contextService.get()).to.eql({
        correlationId,
        userId: 'user-id',
        authorization: 'authorisation',
        scopes: ['scope']
      });
    };

    // WHEN
    contextService.store(correlationId, 'user-id', 'authorisation', ['scope'], callback);
  });
});
