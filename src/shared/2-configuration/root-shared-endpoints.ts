import { getHealthCheckEndpoint } from '@/shared/infrastructure/endpoints/get-health-check-endpoint';
import { getMetricsEndpoint } from '@/shared/infrastructure/endpoints/get-metrics-endpoint';
import { Express } from 'express';

export const rootSharedEndpoints = (server: Express): void => {
  getHealthCheckEndpoint(server);
  getMetricsEndpoint(server);
};
