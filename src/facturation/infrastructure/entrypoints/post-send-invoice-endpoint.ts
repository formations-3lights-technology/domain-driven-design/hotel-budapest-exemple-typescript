import { EnvoyerUneFactureCommand } from '@/facturation/use-cases/envoyer-une-facture/envoyer-une-facture-command';
import { CommandDispatcher } from '@/shared/command-dispatcher/internal-command-dispatcher';
import { Request, RequestHandler, Response } from 'express';
import { checkSchema, ValidationChain } from 'express-validator';

export const postSendInvoiceValidator = (): ValidationChain[] => {
  return checkSchema({
    invoiceEmail: { in: 'body', isEmail: true, notEmpty: true, errorMessage: 'is required' },
    amount: { in: 'body', isNumeric: true, notEmpty: true, errorMessage: 'is required' }
  });
};

export const postSendInvoice = (commandDispatcher: CommandDispatcher): RequestHandler => {
  return async (request: Request, response: Response) => {
    const { invoiceEmail, amount } = request.body;

    await commandDispatcher.dispatch(new EnvoyerUneFactureCommand(invoiceEmail, +amount));

    response.status(201).send();
  };
};
