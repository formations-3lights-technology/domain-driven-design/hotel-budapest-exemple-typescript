import { DependenciesContainer } from '@/2-configuration/dependencies-container';
import { Mail } from '@/facturation/domain/mail';
import { MailSender } from '@/facturation/domain/Mail-sender';
import { EnvoyerUneFactureCommand } from '@/facturation/use-cases/envoyer-une-facture/envoyer-une-facture-command';
import { EnvoyerUneFactureCommandHandler } from '@/facturation/use-cases/envoyer-une-facture/envoyer-une-facture-command-handler';
import { CommandDispatcherMiddleware } from '@/shared/command-dispatcher/Command-dispatcher-middleware';
import { InternalCommandDispatcher } from '@/shared/command-dispatcher/internal-command-dispatcher';
import { ApiLogger } from '@/shared/domain/gateways/Api-logger';
import { TransactionPerformer } from '@/shared/domain/gateways/Transaction-performer';
import { EventPublisherMiddleware } from '@/shared/event-publisher/Event-publisher-middleware';
import { InternalEventPublisher } from '@/shared/event-publisher/internal-event-publisher';

export type FacturationDependenciesContainer = DependenciesContainer<{}>;

export const rootFacturationDependenciesContainer = (
  apiLogger: ApiLogger,
  transactionPerformer: TransactionPerformer,
  middlewares: { commandDispatcher: CommandDispatcherMiddleware[]; eventPublisher: EventPublisherMiddleware[] }
): FacturationDependenciesContainer => {
  const mailSender: MailSender = {
    async send(mail: Mail): Promise<void> {
      apiLogger.info(JSON.stringify(mail));
    }
  };

  const eventPublisher = new InternalEventPublisher(apiLogger).registerMiddleware(middlewares.eventPublisher);
  const commandDispatcher = new InternalCommandDispatcher(transactionPerformer)
    .registerMiddleware(middlewares.commandDispatcher)
    .registerHandlers({
      [EnvoyerUneFactureCommand.label]: new EnvoyerUneFactureCommandHandler(mailSender)
    });

  return {
    commandDispatcher,
    eventPublisher,
    queries: {}
  };
};
