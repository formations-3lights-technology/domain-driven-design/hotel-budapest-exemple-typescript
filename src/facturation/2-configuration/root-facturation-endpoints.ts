import { expressMiddlewares } from '@/2-configuration/express-middlewares';
import {
  postSendInvoice,
  postSendInvoiceValidator
} from '@/facturation/infrastructure/entrypoints/post-send-invoice-endpoint';
import { Router } from 'express';
import asyncHandler from 'express-async-handler';
import { FacturationDependenciesContainer } from './root-facturation-dependencies-container';

export interface RootEndpoints {
  unprotected: (router: Router, dependencies: FacturationDependenciesContainer) => void;
}

export const rootFacturationEndpoints: RootEndpoints = {
  unprotected: (router: Router, { commandDispatcher }: FacturationDependenciesContainer): void => {
    /* WRITE */
    router.post(
      '/invoices/send',
      postSendInvoiceValidator(),
      expressMiddlewares.preErrorHandling(),
      asyncHandler(postSendInvoice(commandDispatcher))
    );
  }
};
