export const defaultConfiguration = {
  boundedContextName: 'Invoicing',
  mail: {
    serviceFacturation: 'service-facturation@email.com'
  }
};
