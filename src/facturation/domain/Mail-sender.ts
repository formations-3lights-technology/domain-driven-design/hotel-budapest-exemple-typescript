import { Mail } from '@/facturation/domain/mail';

export interface MailSender {
  send(mail: Mail): Promise<void>;
}
