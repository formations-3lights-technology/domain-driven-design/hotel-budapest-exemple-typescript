export type Mail = Readonly<{
  from: string;
  to: string;
  message: string;
}>;
