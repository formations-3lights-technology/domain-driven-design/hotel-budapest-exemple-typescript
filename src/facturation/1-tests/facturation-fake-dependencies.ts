import { fakeEventPublisher } from '@/1-tests/utils';
import { FacturationDependenciesContainer } from '@/facturation/2-configuration/root-facturation-dependencies-container';
import { MailSender } from '@/facturation/domain/Mail-sender';
import { CommandDispatcher } from '@/shared/command-dispatcher/internal-command-dispatcher';
import { StubbedType, stubInterface } from '@salesforce/ts-sinon';
import sinon from 'sinon';

export const fakeMailSender: () => StubbedType<MailSender> = () => stubInterface<MailSender>(sinon);

export const facturationFakeDependencies = (): FacturationDependenciesContainer => ({
  commandDispatcher: stubInterface<CommandDispatcher>(sinon),
  eventPublisher: fakeEventPublisher(),
  queries: {}
});
