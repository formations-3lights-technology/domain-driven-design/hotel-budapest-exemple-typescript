import { expect } from '@/1-tests/utils';
import { fakeMailSender } from '@/facturation/1-tests/facturation-fake-dependencies';
import { MailSender } from '@/facturation/domain/Mail-sender';
import { EnvoyerUneFactureCommand } from '@/facturation/use-cases/envoyer-une-facture/envoyer-une-facture-command';
import { EnvoyerUneFactureCommandHandler } from '@/facturation/use-cases/envoyer-une-facture/envoyer-une-facture-command-handler';
import { CommandHandler } from '@/shared/command-dispatcher/Command-handler';
import { StubbedType } from '@salesforce/ts-sinon';

describe('Unit | Facturation | Envoyer une facture', () => {
  let mailSender: StubbedType<MailSender>;
  let underTest: CommandHandler;

  beforeEach(() => {
    mailSender = fakeMailSender();
    underTest = new EnvoyerUneFactureCommandHandler(mailSender);
  });

  it('Tout se passe bien', async () => {
    // GIVEN
    const command = new EnvoyerUneFactureCommand('facturation@email.com', 200);

    // WHEN
    await underTest.handle(command);

    // THEN
    expect(mailSender.send).to.have.been.calledWith({
      from: 'service-facturation@email.com',
      to: 'facturation@email.com',
      message: "Facture d'un montant de : 200 EUR"
    });
  });
});
