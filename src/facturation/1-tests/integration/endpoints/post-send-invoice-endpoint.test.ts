import { expect, fakeDependencies, FakeExpressServer } from '@/1-tests/utils';
import { EnvoyerUneFactureCommand } from '@/facturation/use-cases/envoyer-une-facture/envoyer-une-facture-command';
import { CommandDispatcher } from '@/shared/command-dispatcher/internal-command-dispatcher';
import supertest from 'supertest';

describe('Integration | Facturation | POST /invoices/send', () => {
  let fakeApi: supertest.SuperTest<supertest.Test>;
  let commandDispatcher: CommandDispatcher;
  const baseUrl = '/v1/invoices/send';

  beforeEach(async () => {
    const dependencies = fakeDependencies();
    commandDispatcher = dependencies.facturationDependenciesContainer.commandDispatcher;
    const fakeServer = await new FakeExpressServer(dependencies).create();
    fakeApi = fakeServer.api;
  });

  describe('Retourne 201', () => {
    it('Tout est renseigné', () => {
      // WHEN
      return fakeApi
        .post(baseUrl)
        .send({
          invoiceEmail: 'email@test.com',
          amount: 2
        })
        .expect(201)
        .then((_) => {
          // THEN
          expect(commandDispatcher.dispatch).to.have.been.calledOnceWithExactly(
            new EnvoyerUneFactureCommand('email@test.com', 2)
          );
        });
    });
  });
});
