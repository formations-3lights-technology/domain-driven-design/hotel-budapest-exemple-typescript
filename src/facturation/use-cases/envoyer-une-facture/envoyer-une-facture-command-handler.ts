import { defaultConfiguration } from '@/facturation/2-configuration/default-configuration';
import { MailSender } from '@/facturation/domain/Mail-sender';
import { EnvoyerUneFactureCommand } from '@/facturation/use-cases/envoyer-une-facture/envoyer-une-facture-command';
import { CommandHandler } from '@/shared/command-dispatcher/Command-handler';

export class EnvoyerUneFactureCommandHandler implements CommandHandler {
  constructor(private readonly mailSender: MailSender) {}

  async handle(command: EnvoyerUneFactureCommand): Promise<void> {
    await this.mailSender.send({
      from: defaultConfiguration.mail.serviceFacturation,
      to: command.mailDeFacturation,
      message: `Facture d'un montant de : ${command.montant} EUR`
    });
  }
}
