import { defaultConfiguration } from '@/facturation/2-configuration/default-configuration';
import { Command } from '@/shared/command-dispatcher/Command';

type CommandSerialized = Readonly<{
  invoiceEmail: string;
  amount: number;
}>;

export class EnvoyerUneFactureCommand extends Command<CommandSerialized> {
  static readonly label = 'SendInvoiceCommand';
  protected readonly boundedContext = defaultConfiguration.boundedContextName;

  constructor(readonly mailDeFacturation: string, readonly montant: number) {
    super();
  }

  isPublic(): boolean {
    return false;
  }

  label(): string {
    return EnvoyerUneFactureCommand.label;
  }

  protected serializedData(): CommandSerialized {
    return {
      invoiceEmail: this._apiCrypt.encrypt(this.mailDeFacturation),
      amount: this.montant
    };
  }
}
