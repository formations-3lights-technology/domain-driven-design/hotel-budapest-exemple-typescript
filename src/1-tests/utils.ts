import { Dependencies } from '@/2-configuration/dependencies-container';
import { ExpressMiddlewares, expressMiddlewares } from '@/2-configuration/express-middlewares';
import { chambreReservationFakeDependencies } from '@/chambre-reservation/1-tests/chambre-reservation-fake-dependencies';
import { ReservationTypeOrmEntity } from '@/chambre-reservation/infrastructure/gateways/reservation-type-orm-entity';
import { deroulementSejourFakeDependencies } from '@/deroulement-sejour/1-tests/deroulement-sejour-fake-dependencies';
import { SejourTypeOrmEntity } from '@/deroulement-sejour/infrastructure/gateways/sejour-type-orm-entity';
import { ExpressServer } from '@/express-server';
import { facturationFakeDependencies } from '@/facturation/1-tests/facturation-fake-dependencies';
import { readinessFakeDependencies } from '@/readiness/1-tests/readiness-fake-dependencies';
import { ApiCrypt } from '@/shared/domain/gateways/Api-crypt';
import { ApiLogger } from '@/shared/domain/gateways/Api-logger';
import { ContexteService } from '@/shared/domain/gateways/Contexte-service';
import { DateProvider } from '@/shared/domain/gateways/Date-provider';
import { HttpClient } from '@/shared/domain/gateways/Http-client';
import { IdentityGenerator } from '@/shared/domain/gateways/Identity-generator';
import { TransactionPerformer } from '@/shared/domain/gateways/Transaction-performer';
import { EventPublisher } from '@/shared/event-publisher/internal-event-publisher';
import { NodeEnvironmentVariables } from '@/shared/infrastructure/gateways/node-environment-variables';
import { StubbedType, stubInterface } from '@salesforce/ts-sinon';
import chai, { expect as chaiExpect } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import deepEqualInAnyOrder from 'deep-equal-in-any-order';
import dirtyChai from 'dirty-chai';
import { Request, Response } from 'express';
import { NextFunction } from 'express-serve-static-core';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import superAgentDefaults from 'superagent-defaults';
import supertest from 'supertest';
import { Connection } from 'typeorm';

chai.use(sinonChai);
chai.use(dirtyChai);
chai.use(deepEqualInAnyOrder);
chai.use(chaiAsPromised);

export const fakeDependencies = (dependencies: Partial<Dependencies> = {}): Dependencies => {
  sinon.restore();
  // @ts-ignore
  return {
    environmentVariables: new NodeEnvironmentVariables(),
    apiLogger: fakeApiLogger(),
    dateProvider: fakeDateProvider(),
    apiCrypt: fakeApiCrypt,
    identityGenerator: fakeIdentityGenerator(),
    contexteService: fakeContextService(),
    transactionPerformer: fakeTransactionPerformer(),

    readinessDependenciesContainer: readinessFakeDependencies(),
    chambreReservationDependenciesContainer: chambreReservationFakeDependencies(),
    deroulementSejourDependenciesContainer: deroulementSejourFakeDependencies(),
    facturationDependenciesContainer: facturationFakeDependencies(),
    ...dependencies
  };
};

export const fakeApiLogger: () => StubbedType<ApiLogger> = () =>
  stubInterface<ApiLogger>(sinon, { loggerOptions: undefined });

export const fakeIdentityGenerator: () => StubbedType<IdentityGenerator> = () =>
  stubInterface<IdentityGenerator>(sinon, { generate: sinon.stub().returns('cee76b05-f9dd-4231-91ed-4604b3d64c4c') });

export const fakeContextService: () => StubbedType<ContexteService> = () =>
  stubInterface<ContexteService>(sinon, {
    get: sinon.stub().returns({
      source: 'appel-medical',
      brandCode: '307AM',
      correlationId: 'correlationId'
    })
  });

export const fakeHttpClient = (): StubbedType<HttpClient> => stubInterface<HttpClient>(sinon);

export const fakeTransactionPerformer = (): StubbedType<TransactionPerformer> =>
  stubInterface<TransactionPerformer>(sinon, {
    process: <R>(transactionToProcess: () => Promise<R>): Promise<R> => transactionToProcess()
  });

export const fakeEventPublisher = (): StubbedType<EventPublisher> => stubInterface<EventPublisher>(sinon);

export const fakeDateProvider = (): StubbedType<DateProvider> => stubInterface<DateProvider>(sinon);

export const fakeApiCrypt: ApiCrypt = {
  encrypt: (value: string | number | boolean) => `#${value.toString()}#`,
  decrypt: (value: string) => value.toString().replace(/#/g, '')
};

export const dropAllTableTypeORM = async (dbClient: Promise<Connection>) => {
  await (await dbClient).createQueryBuilder().delete().from(ReservationTypeOrmEntity).execute();

  await (await dbClient).createQueryBuilder().delete().from(SejourTypeOrmEntity).execute();
};

export const fakeExpressMiddlewares = (): ExpressMiddlewares => {
  const fakeMiddleware = (request: Request, _: Response, next: NextFunction): void => {
    next();
  };
  return {
    ...expressMiddlewares,
    initContext: (_) => fakeMiddleware
  };
};

export class FakeExpressServer {
  constructor(private dependencies: Dependencies) {}

  async create() {
    const server = new ExpressServer(this.dependencies, fakeExpressMiddlewares()).create();

    const fakeApi = superAgentDefaults(supertest(server));
    return { server, api: fakeApi };
  }
}

export const expect = chaiExpect;
