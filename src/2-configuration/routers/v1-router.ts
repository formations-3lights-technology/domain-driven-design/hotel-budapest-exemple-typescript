import { Dependencies } from '@/2-configuration/dependencies-container';
import { ExpressMiddlewares } from '@/2-configuration/express-middlewares';
import { expressPlugins } from '@/2-configuration/express-plugins';
import { rootChambreReservationEndpoints } from '@/chambre-reservation/2-configuration/root-chambre-reservation-endpoints';
import { rootDeroulementSejourEndpoints } from '@/deroulement-sejour/2-configuration/root-deroulement-sejour-endpoints';
import { rootFacturationEndpoints } from '@/facturation/2-configuration/root-facturation-endpoints';
import { rootReadinessEndpoints } from '@/readiness/2-configuration/root-readiness-endpoints';
import { Router } from 'express';

export const v1Router = (dependencies: Dependencies, middlewares: ExpressMiddlewares): Router => {
  const router = Router();

  expressPlugins.apiDocumentation(router, dependencies);

  rootReadinessEndpoints.unprotected(router, dependencies.readinessDependenciesContainer);

  router.use(middlewares.initContext(dependencies.contexteService));

  rootChambreReservationEndpoints.unprotected(router, dependencies.chambreReservationDependenciesContainer);
  rootDeroulementSejourEndpoints.unprotected(router, dependencies.deroulementSejourDependenciesContainer);
  rootFacturationEndpoints.unprotected(router, dependencies.facturationDependenciesContainer);

  return router;
};
