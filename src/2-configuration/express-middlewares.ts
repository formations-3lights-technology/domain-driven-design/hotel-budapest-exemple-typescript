import { ActionNonAutorisee } from '@/shared/domain/erreurs/action-non-autorisee';
import { EchecDependance } from '@/shared/domain/erreurs/echec-dependance';
import { ElementEnConflit } from '@/shared/domain/erreurs/element-en-conflit';
import { ElementNonTrouve } from '@/shared/domain/erreurs/element-non-trouve';
import { ErreurDeTraitement } from '@/shared/domain/erreurs/erreur-de-traitement';
import { ErreurDeValidation } from '@/shared/domain/erreurs/erreur-de-validation';
import { LimiteAppel } from '@/shared/domain/erreurs/limite-appel';
import { NonAutorisation } from '@/shared/domain/erreurs/non-autorisation';
import { ServiceIndisponible } from '@/shared/domain/erreurs/service-indisponible';
import { ContexteService } from '@/shared/domain/gateways/Contexte-service';
import { IdentityGenerator } from '@/shared/domain/gateways/Identity-generator';
import { MetricsCollector } from '@/shared/infrastructure/gateways/metrics-collector/Metrics-collector';
import { ErrorRequestHandler, Request, RequestHandler, Response } from 'express';
import { UnauthorizedError } from 'express-jwt';
import expressPino from 'express-pino-logger';
import { NextFunction } from 'express-serve-static-core';
import { validationResult } from 'express-validator';
import { IncomingHttpHeaders } from 'http';
import createError from 'http-errors';
import { Dependencies } from './dependencies-container';

export interface ExpressMiddlewares {
  addXReqStartHeader: () => RequestHandler;

  addCorrelationId: (identityGenerator: IdentityGenerator) => RequestHandler;

  collectMetrics: (metricsCollector: MetricsCollector) => RequestHandler;
  addHeaders: () => RequestHandler;

  initContext: (contexteService: ContexteService) => RequestHandler;
  preErrorHandling: () => RequestHandler;

  logger: (dependencies: Dependencies) => RequestHandler;
  postErrorHandling: (dependencies: Dependencies) => ErrorRequestHandler;
}

export const expressMiddlewares: ExpressMiddlewares = {
  addXReqStartHeader: (): RequestHandler => {
    return (request: Request, response: Response, next: NextFunction): void => {
      request.headers['x-req-start'] = new Date().getTime().toString();
      next();
    };
  },

  addCorrelationId: (identityGenerator: IdentityGenerator): RequestHandler => {
    return (request: Request, response: Response, next: NextFunction): void => {
      const correlationId = request.header('x-correlation-id') || identityGenerator.generate();
      request.headers['x-correlation-id'] = correlationId;
      response.setHeader('x-correlation-id', correlationId);
      next();
    };
  },

  collectMetrics: (metricsCollector: MetricsCollector): RequestHandler => {
    return (request: Request, response: Response, next: NextFunction): void => {
      request.on('close', () => {
        const { method, path, headers } = request;
        const statusCode = response.statusCode;
        const elapsedTime = computeElapsedTime(headers);

        metricsCollector.collectMetrics(method, path, statusCode, elapsedTime);
      });
      next();
    };
  },

  addHeaders: (): RequestHandler => {
    return (request: Request, response: Response, next: NextFunction): void => {
      const SECURITY_HEADERS = [
        {
          header: 'strict-transport-security',
          value: 'max-age=31536000; includeSubDomains'
        },
        {
          header: 'x-xss-protection',
          value: '1; mode=block'
        },
        {
          header: 'x-content-type-options',
          value: 'nosniff'
        },
        {
          header: 'x-frame-options',
          value: 'deny'
        },
        {
          header: 'access-control-expose-headers',
          value: 'content-range, content-location'
        }
      ];
      SECURITY_HEADERS.forEach(({ header, value }) => {
        response.setHeader(header, value);
      });
      next();
    };
  },

  initContext: (contexteService: ContexteService): RequestHandler => {
    return (request: Request, response: Response, next: NextFunction): void => {
      contexteService.store(
        request.headers['x-correlation-id'] as string,
        request['auth'] ? request['auth'].userId : '',
        request.get('authorization'),
        request['auth'] ? request['auth'].scopes : [],
        next
      );
    };
  },

  preErrorHandling: (): RequestHandler => {
    return (request: Request, response: Response, next: NextFunction): void => {
      const errors = validationResult(request);
      if (!errors.isEmpty()) {
        throw new ErreurDeValidation(
          errors
            .array()
            .map((e) => `${e.param} - ${e.msg}`)
            .join(' | ')
        );
      }
      next();
    };
  },

  logger: (dependencies: Dependencies): ((req: Request, res: Response, next: NextFunction) => void) => {
    return (request: Request, response: Response, next: NextFunction): void => {
      expressPino({
        ...dependencies.apiLogger.loggerOptions,
        customSuccessMessage: (res: Response) => log(dependencies, request, res),
        customErrorMessage: (_, res: Response) => logServerError(dependencies, request, res),
        enabled: false
      })(request, response, next);
    };
  },

  postErrorHandling: (dependencies: Dependencies): ErrorRequestHandler => {
    return (error: any, request: Request, response: Response, _: NextFunction): void => {
      let httpError: createError.HttpError = new createError.InternalServerError(error.message);

      if (error instanceof ErreurDeValidation) {
        httpError = new createError.BadRequest(error.message);
      }
      if (error instanceof UnauthorizedError || error instanceof NonAutorisation) {
        httpError = new createError.Unauthorized(error.message);
      }
      if (error instanceof ActionNonAutorisee) {
        httpError = new createError.Forbidden(error.message);
      }
      if (error instanceof ElementNonTrouve) {
        httpError = new createError.NotFound(error.message);
      }
      if (error instanceof ElementEnConflit) {
        httpError = new createError.Conflict(error.message);
      }
      if (error instanceof ErreurDeTraitement) {
        httpError = new createError.UnprocessableEntity(error.message);
      }
      if (error instanceof EchecDependance) {
        switch (error.statusCode) {
          case 400:
            httpError = new createError.BadRequest(error.message);
            break;
          case 404:
            httpError = new createError.NotFound(error.message);
            break;
          case 429:
            httpError = new createError.TooManyRequests(error.message);
            break;
          default:
            httpError = new createError.FailedDependency(error.message);
            break;
        }
      }
      if (error instanceof LimiteAppel) {
        httpError = new createError.TooManyRequests(error.message);
      }
      if (error instanceof ServiceIndisponible) {
        httpError = new createError.ServiceUnavailable(error.message);
      }

      const errorBody = {
        statusCode: httpError.statusCode,
        error: httpError.name.replace('Error', ''),
        message: httpError.message
      };

      response.status(httpError.statusCode).send(errorBody);
      log(dependencies, request, response, errorBody);
    };
  }
};

function log(dependencies: Dependencies, request: Request, response: Response, errorBody: any = null) {
  const metadata = logBase(request, response.statusCode);
  const header = `Request completed ${metadata.method} ${metadata.statusCode} ${metadata.path}`;

  if (response.statusCode.toString().startsWith('2')) {
    dependencies.apiLogger.infoWithMetadata(metadata, header);
  }
  if (errorBody) {
    dependencies.apiLogger.errorWithMetadata(metadata, header, errorContent(errorBody));
  }

  function errorContent(errorBody: any) {
    return {
      request: {
        'x-resource-version': response.req.header('x-resource-version'),
        body: response.req.body
      },
      response: {
        body: errorBody
      }
    };
  }
}

function logServerError(dependencies: Dependencies, request: Request, response: Response) {
  const metadata = logBase(request, response.statusCode);
  const header = `Request completed ${metadata.method} ${metadata.statusCode} ${metadata.path}`;

  dependencies.apiLogger.errorWithMetadata(metadata, header);
}

function logBase(request: Request, statusCode: number) {
  const responseTime = computeElapsedTime(request.headers);
  return {
    correlationId: getStringOrDefault(request.headers['x-correlation-id'], ''),
    method: getStringOrDefault(request.method, ''),
    path: getStringOrDefault(request.path, ''),
    statusCode: getNumberOrDefault(statusCode, 0),
    responseTime
  };
}

function computeElapsedTime(headers: IncomingHttpHeaders): number {
  const startTime = Number(headers['x-req-start']);
  const endTime = new Date().getTime();

  return endTime - startTime;
}

function getStringOrDefault<T>(value: T, defaultValue: string): string {
  return String(value) || defaultValue;
}

function getNumberOrDefault<T>(value: T, defaultValue: number): number {
  return Number(value) || defaultValue;
}
