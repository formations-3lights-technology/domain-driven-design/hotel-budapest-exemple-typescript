import {
  ChambreReservationDependenciesContainer,
  rootChambreReservationDependenciesContainer
} from '@/chambre-reservation/2-configuration/root-chambre-reservation-dependencies-container';
import {
  DeroulementSejourDependenciesContainer,
  rootDeroulementSejourDependenciesContainer
} from '@/deroulement-sejour/2-configuration/root-deroulement-sejour-dependencies-container';
import {
  FacturationDependenciesContainer,
  rootFacturationDependenciesContainer
} from '@/facturation/2-configuration/root-facturation-dependencies-container';
import {
  ReadinessDependenciesContainer,
  rootReadinessDependenciesContainer
} from '@/readiness/2-configuration/root-readiness-dependencies-container';
import { CommandDispatcher } from '@/shared/command-dispatcher/internal-command-dispatcher';
import { AddCommandIdCommandDispatcherMiddleware } from '@/shared/command-dispatcher/middleware/add-command-id-command-dispatcher-middleware';
import { AddCorrelationIdCommandDispatcherMiddleware } from '@/shared/command-dispatcher/middleware/add-correlation-id-command-dispatcher-middleware';
import { AddSenderIdCommandDispatcherMiddleware } from '@/shared/command-dispatcher/middleware/add-sender-id-command-dispatcher-middleware';
import { AddTimestampCommandDispatcherMiddleware } from '@/shared/command-dispatcher/middleware/add-timestamp-command-dispatcher-middleware';
import { LoggerCommandDispatcherMiddleware } from '@/shared/command-dispatcher/middleware/logger-command-dispatcher-middleware';
import { ProvideApiCryptCommandDispatcherMiddleware } from '@/shared/command-dispatcher/middleware/provide-api-crypt-command-dispatcher-middleware';
import { ApiCrypt } from '@/shared/domain/gateways/Api-crypt';
import { ApiLogger } from '@/shared/domain/gateways/Api-logger';
import { ContexteService } from '@/shared/domain/gateways/Contexte-service';
import { DateProvider } from '@/shared/domain/gateways/Date-provider';
import { EnvironmentVariables } from '@/shared/domain/gateways/Environment-variables';
import { IdentityGenerator } from '@/shared/domain/gateways/Identity-generator';
import { TransactionPerformer } from '@/shared/domain/gateways/Transaction-performer';
import { EventPublisher } from '@/shared/event-publisher/internal-event-publisher';
import { AddCorrelationIdEventPublisherMiddleware } from '@/shared/event-publisher/middleware/add-correlation-id-event-publisher-middleware';
import { AddEventIdEventPublisherMiddleware } from '@/shared/event-publisher/middleware/add-event-id-event-publisher-middleware';
import { AddSenderIdEventPublisherMiddleware } from '@/shared/event-publisher/middleware/add-sender-id-event-publisher-middleware';
import { AddTimestampEventPublisherMiddleware } from '@/shared/event-publisher/middleware/add-timestamp-event-publisher-middleware';
import { LoggerEventPublisherMiddleware } from '@/shared/event-publisher/middleware/logger-event-publisher-middleware';
import { ProvideApiCryptEventPublisherMiddleware } from '@/shared/event-publisher/middleware/provide-api-crypt-event-publisher-middleware';
import { AsyncLocalContexteService } from '@/shared/infrastructure/gateways/async-local-context-service';
import { CryptoJsApiCrypt } from '@/shared/infrastructure/gateways/crypto-js-api-crypt';
import { TypeORMClient } from '@/shared/infrastructure/gateways/db-client/type-orm/type-orm-client';
import { typeORMDbClient } from '@/shared/infrastructure/gateways/db-client/type-orm/type-orm-db-client';
import { TypeORMTransactionPerformer } from '@/shared/infrastructure/gateways/db-client/type-orm/type-orm-transaction-performer';
import { ChambreReservationGotHttpClient } from '@/shared/infrastructure/gateways/http-client/chambre-reservation-got-http-client';
import { FacturationGotHttpClient } from '@/shared/infrastructure/gateways/http-client/facturation-got-http-client';
import { ChambreReservationServiceMetricsCollector } from '@/shared/infrastructure/gateways/metrics-collector/metrics-collectors/chambre-reservation-service-metrics-collector';
import { FacturationServiceMetricsCollector } from '@/shared/infrastructure/gateways/metrics-collector/metrics-collectors/facturation-service-metrics-collector';
import { NodeEnvironmentVariables } from '@/shared/infrastructure/gateways/node-environment-variables';
import { PinoApiLogger } from '@/shared/infrastructure/gateways/pino-api-logger';
import { RealDateProvider } from '@/shared/infrastructure/gateways/real-date-provider';
import { UuidIdentityGenerator } from '@/shared/infrastructure/gateways/uuid-identity-generator';

export type DependenciesContainerWrite = Readonly<{
  commandDispatcher: CommandDispatcher;
  eventPublisher: EventPublisher;
}>;
export type DependenciesContainerRead<Q> = Readonly<{ queries: Q }>;
export type DependenciesContainer<Q> = DependenciesContainerWrite & DependenciesContainerRead<Q>;

export type Dependencies = Readonly<{
  environmentVariables: EnvironmentVariables;
  apiLogger: ApiLogger;
  apiCrypt: ApiCrypt;
  identityGenerator: IdentityGenerator;
  contexteService: ContexteService;
  dateProvider: DateProvider;
  typeORMClient: TypeORMClient;
  transactionPerformer: TransactionPerformer;

  readinessDependenciesContainer: ReadinessDependenciesContainer;
  chambreReservationDependenciesContainer: ChambreReservationDependenciesContainer;
  deroulementSejourDependenciesContainer: DeroulementSejourDependenciesContainer;
  facturationDependenciesContainer: FacturationDependenciesContainer;
}>;

export const dependenciesContainer = (): Dependencies => {
  const environmentVariables = new NodeEnvironmentVariables();
  const apiLogger = new PinoApiLogger(environmentVariables);
  const apiCrypt = new CryptoJsApiCrypt(environmentVariables);
  const identityGenerator = new UuidIdentityGenerator();
  const contexteService = new AsyncLocalContexteService();
  const dateProvider = new RealDateProvider();

  const chambreReservationHttpClient = new ChambreReservationGotHttpClient(
    apiLogger,
    contexteService,
    new ChambreReservationServiceMetricsCollector(),
    environmentVariables
  );
  const facturationHttpClient = new FacturationGotHttpClient(
    apiLogger,
    contexteService,
    new FacturationServiceMetricsCollector(),
    environmentVariables
  );

  const typeORMClient = new TypeORMClient(
    typeORMDbClient(environmentVariables),
    apiLogger,
    contexteService,
    environmentVariables
  );
  const transactionPerformer = new TypeORMTransactionPerformer(typeORMClient);

  const middlewares = {
    commandDispatcher: [
      new AddSenderIdCommandDispatcherMiddleware(environmentVariables),
      new AddCorrelationIdCommandDispatcherMiddleware(contexteService),
      new AddCommandIdCommandDispatcherMiddleware(identityGenerator),
      new AddTimestampCommandDispatcherMiddleware(dateProvider),
      new ProvideApiCryptCommandDispatcherMiddleware(apiCrypt),
      new LoggerCommandDispatcherMiddleware(apiLogger)
    ],
    eventPublisher: [
      new AddSenderIdEventPublisherMiddleware(environmentVariables),
      new AddCorrelationIdEventPublisherMiddleware(contexteService),
      new AddEventIdEventPublisherMiddleware(identityGenerator),
      new AddTimestampEventPublisherMiddleware(dateProvider),
      new ProvideApiCryptEventPublisherMiddleware(apiCrypt),
      new LoggerEventPublisherMiddleware(apiLogger)
    ]
  };

  const readinessDependenciesContainer = rootReadinessDependenciesContainer(typeORMClient);
  const chambreReservationDependenciesContainer = rootChambreReservationDependenciesContainer(
    apiLogger,
    apiCrypt,
    identityGenerator,
    transactionPerformer,
    middlewares,
    typeORMClient
  );
  const deroulementSejourDependenciesContainer = rootDeroulementSejourDependenciesContainer(
    apiLogger,
    identityGenerator,
    transactionPerformer,
    middlewares,
    typeORMClient,
    chambreReservationHttpClient,
    facturationHttpClient
  );
  const facturationDependenciesContainer = rootFacturationDependenciesContainer(
    apiLogger,
    transactionPerformer,
    middlewares
  );

  return {
    environmentVariables,
    apiLogger,
    apiCrypt,
    identityGenerator,
    contexteService,
    dateProvider,
    typeORMClient,
    transactionPerformer,

    readinessDependenciesContainer,
    chambreReservationDependenciesContainer,
    deroulementSejourDependenciesContainer,
    facturationDependenciesContainer
  };
};
