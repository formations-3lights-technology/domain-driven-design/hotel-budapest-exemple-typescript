import { Dependencies } from './dependencies-container';

type DefaultConfigurationData = Readonly<{}>;

export const defaultConfiguration = (_: Dependencies): DefaultConfigurationData => ({});
