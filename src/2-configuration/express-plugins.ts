import { NonAutorisation } from '@/shared/domain/erreurs/non-autorisation';
import { Request, Response, Router } from 'express';
import { IBasicAuthedRequest } from 'express-basic-auth';
import fs from 'fs';
import redoc from 'redoc-express';
import swaggerJsdoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';
import packageJson from '../../package.json';
import { Dependencies } from './dependencies-container';

export interface ExpressPlugins {
  getUnauthorizedResponse: (req: IBasicAuthedRequest) => any;
  apiDocumentation: (router: Router, dependencies: Dependencies) => void;
}

export const expressPlugins: ExpressPlugins = {
  getUnauthorizedResponse: (req: IBasicAuthedRequest): any => {
    throw req.auth
      ? new NonAutorisation(`Credentials ${req.auth.user}:${req.auth.password} rejected`)
      : new NonAutorisation('No credentials provided');
  },

  apiDocumentation: (router: Router, dependencies: Dependencies): void => {
    const API_NAME = 'Hotel Budapest API';
    const { API_CURRENT_VERSION } = dependencies.environmentVariables;
    const swaggerDescription = fs.readFileSync(`./documentation/${API_CURRENT_VERSION}/swagger-description.md`, 'utf8');

    const swaggerDefinition = {
      info: {
        title: API_NAME,
        version: packageJson.version,
        description: swaggerDescription,
        contact: {
          name: 'API Support',
          email: ''
        }
      },
      components: {
        securitySchemes: {
          basicAuth: {
            type: 'http',
            scheme: 'basic'
          },
          bearerAuth: {
            type: 'http',
            scheme: 'bearer'
          }
        },
        schemas: {
          ...getCommonErrorSchemas()
        }
      },
      openapi: '3.0.0'
    };
    const swaggerJSDocOptions = {
      swaggerDefinition,
      apis: ['./src/**/*-swagger.yml']
    };
    const swaggerSpec = swaggerJsdoc(swaggerJSDocOptions);

    const swaggerUiOptions = {
      customSiteTitle: API_NAME,
      swaggerOptions: {
        basicAuth: {
          name: 'Authorization',
          schema: {
            type: 'basic',
            in: 'header'
          },
          value: 'Basic <user:password>'
        },
        bearerAuth: {
          name: 'Authorization',
          schema: {
            type: 'bearer',
            in: 'header'
          },
          value: 'Bearer <token>'
        }
      }
    };

    router.get('/swagger.json', (_: Request, response: Response) => {
      response.setHeader('Content-Type', 'application/json');
      response.send(swaggerSpec);
    });
    router.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerSpec, swaggerUiOptions));
    router.use(
      '/redoc',
      redoc({
        title: API_NAME,
        specUrl: `/${dependencies.environmentVariables.API_CURRENT_VERSION}/swagger.json`
      })
    );
  }
};

function getCommonErrorSchemas() {
  return {
    Error400: constructError(400, 'Bad Request'),
    Error403: constructError(403, 'Access forbidden'),
    Error404: constructError(404, 'Not Found'),
    Error409: constructError(409, 'Conflict'),
    Error422: constructError(422, 'Unprocessable Entity'),
    Error424: constructError(424, 'Failed Dependency'),
    Error429: constructError(429, 'Too Many Requests'),
    Error500: constructError(500, 'Internal Server')
  };

  function constructError(statusCode: number, statusLabel: string) {
    const properties = {
      statusCode: { type: 'number' },
      message: { type: 'string' },
      error: { type: 'string' }
    };

    return {
      description: statusLabel,
      content: {
        'application/json': {
          schema: {
            type: 'object',
            properties,
            example: {
              statusCode,
              error: statusLabel,
              message: 'error'
            }
          }
        }
      }
    };
  }
}
