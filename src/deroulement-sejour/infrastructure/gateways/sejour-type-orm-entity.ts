import { defaultConfiguration } from '@/deroulement-sejour/2-configuration/default-configuration';
import { SejourSnapshot } from '@/deroulement-sejour/domain/sejour/sejour';
import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity({
  schema: defaultConfiguration.db.schema,
  name: defaultConfiguration.db.sejour
})
export class SejourTypeOrmEntity {
  @PrimaryColumn()
  readonly id: string;

  @Column('varchar', { name: 'reservation_id' })
  readonly reservationId: string;

  @Column('integer', { name: 'prix_par_nuit' })
  readonly prixParNuit: number;

  @Column('timestamp with time zone', { name: 'date_d_enregistrement' })
  readonly dateDEnregistrement: Date;

  @Column('timestamp with time zone', { name: 'date_de_fin_prevue' })
  readonly dateDeFinPrevue: Date;

  @Column('timestamp with time zone', { name: 'date_de_fin' })
  readonly dateDeFin: Nullable<Date>;

  @Column('integer', { name: 'montant_paye' })
  readonly montantPaye: Nullable<number>;

  constructor(
    private readonly _id: string,
    private readonly _reservationId: string,
    private readonly _prixParNuit: number,
    private readonly _dateDEnregistrement: Date,
    private readonly _dateDeFinPrevue: Date,
    private readonly _dateDeFin: Nullable<Date>,
    private readonly _montantPaye: Nullable<number>
  ) {
    this.id = this._id;
    this.reservationId = this._reservationId;
    this.prixParNuit = this._prixParNuit;
    this.dateDEnregistrement = this._dateDEnregistrement;
    this.dateDeFinPrevue = this._dateDeFinPrevue;
    this.dateDeFin = this._dateDeFin;
    this.montantPaye = this._montantPaye;
  }

  static mapDepuisSnapshot(snapshot: SejourSnapshot): SejourTypeOrmEntity {
    return new SejourTypeOrmEntity(
      snapshot.id,
      snapshot.reservationId,
      snapshot.prixParNuit,
      snapshot.dateDEnregistrement,
      snapshot.dateDeFinPrevue,
      snapshot.dateDeFin,
      snapshot.montantPaye
    );
  }

  mapVersSnapshot(): SejourSnapshot {
    return {
      id: this.id,
      reservationId: this.reservationId,
      prixParNuit: this.prixParNuit,
      dateDEnregistrement: this.dateDEnregistrement,
      dateDeFinPrevue: this.dateDeFinPrevue,
      dateDeFin: this.dateDeFin,
      montantPaye: this.montantPaye
    };
  }
}
