import { Erreur } from '@/deroulement-sejour/domain/erreurs';
import { Reservation } from '@/deroulement-sejour/domain/reservation/reservation';
import { ReservationLoader } from '@/deroulement-sejour/domain/reservation/Reservation-loader';
import { HttpClient } from '@/shared/domain/gateways/Http-client';
import { ApiResponse } from '@/shared/infrastructure/gateways/http-client/base/base-http-client';

type GetReservationParIdResponse = ApiResponse<{
  data: Readonly<{
    id: string;
    bookingEmail: string;
    roomNumber: string;
    totalPerson: number;
    priceByNight: number;
    startDate: string;
    endDate: string;
  }>;
}>;

export class HttpReservationLoader implements ReservationLoader {
  constructor(private readonly httpClient: HttpClient) {}

  async getParIdOuErreur(id: string): Promise<Reservation> {
    try {
      const { response } = await this.httpClient.get<GetReservationParIdResponse>(`/booking/rooms/${id}`);
      return {
        id: response.data.id,
        mailDeReservation: response.data.bookingEmail,
        prixParNuit: response.data.priceByNight,
        dateDeDebut: new Date(response.data.startDate),
        dateDeFin: new Date(response.data.endDate)
      };
    } catch (_) {
      throw new Erreur.Reservation.NonTrouvee();
    }
  }
}
