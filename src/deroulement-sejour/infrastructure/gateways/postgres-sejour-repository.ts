import { Erreur } from '@/deroulement-sejour/domain/erreurs';
import { SejourSnapshot } from '@/deroulement-sejour/domain/sejour/sejour';
import { SejourRepository } from '@/deroulement-sejour/domain/sejour/Sejour-repository';
import { SejourTypeOrmEntity } from '@/deroulement-sejour/infrastructure/gateways/sejour-type-orm-entity';
import { IdentityGenerator } from '@/shared/domain/gateways/Identity-generator';
import { TypeORMClient } from '@/shared/infrastructure/gateways/db-client/type-orm/type-orm-client';
import { endOfDay, startOfDay } from 'date-fns';
import { Between } from 'typeorm';

export class PostgresSejourRepository implements SejourRepository {
  constructor(private readonly typeORMClient: TypeORMClient, private readonly identityGenerator: IdentityGenerator) {}

  async nextId(): Promise<string> {
    return this.identityGenerator.generate();
  }

  async save(sejour: SejourSnapshot): Promise<void> {
    await this.typeORMClient.executeTransaction((q) =>
      q.getRepository(SejourTypeOrmEntity).save(SejourTypeOrmEntity.mapDepuisSnapshot(sejour))
    );
  }

  async getParIdOuErreur(id: string): Promise<SejourSnapshot> {
    try {
      const sejour = await this.typeORMClient.executeQuery((q) =>
        q.getRepository(SejourTypeOrmEntity).findOneByOrFail({ id })
      );
      return sejour.mapVersSnapshot();
    } catch (_) {
      throw new Erreur.Sejour.NonTrouve();
    }
  }

  async getParDateDeFin(date: Date): Promise<SejourSnapshot[]> {
    const result = await this.typeORMClient.executeQuery((q) =>
      q.getRepository(SejourTypeOrmEntity).findBy({
        dateDeFin: Between(startOfDay(date), endOfDay(date))
      })
    );
    return result.map((r) => r.mapVersSnapshot());
  }
}
