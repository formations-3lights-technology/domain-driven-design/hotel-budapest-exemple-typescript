import { DemarrerUnSejourCommand } from '@/deroulement-sejour/use-cases/demarrer-sejour/demarrer-un-sejour-command';
import { CommandDispatcher } from '@/shared/command-dispatcher/internal-command-dispatcher';
import { Request, RequestHandler, Response } from 'express';
import { checkSchema, ValidationChain } from 'express-validator';

export const putTripCheckInValidator = (): ValidationChain[] => {
  return checkSchema({
    bookingId: { in: 'body', isString: true, notEmpty: true, errorMessage: 'is required' },
    startDate: { in: 'body', isDate: true, notEmpty: true, errorMessage: 'is required' }
  });
};

export const putTripCheckIn = (commandDispatcher: CommandDispatcher): RequestHandler => {
  return async (request: Request, response: Response) => {
    const { bookingId, startDate } = request.body;

    await commandDispatcher.dispatch(new DemarrerUnSejourCommand(bookingId, new Date(startDate)));

    response.status(201).send();
  };
};
