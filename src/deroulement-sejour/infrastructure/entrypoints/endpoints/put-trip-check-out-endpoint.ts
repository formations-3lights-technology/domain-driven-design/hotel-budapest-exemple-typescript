import { FinirUnSejourCommand } from '@/deroulement-sejour/use-cases/finir-sejour/command-handler/finir-un-sejour-command';
import { CommandDispatcher } from '@/shared/command-dispatcher/internal-command-dispatcher';
import { Request, RequestHandler, Response } from 'express';
import { checkSchema, ValidationChain } from 'express-validator';

export const putTripCheckOutValidator = (): ValidationChain[] => {
  return checkSchema({
    id: { in: 'params', isString: true, notEmpty: true, errorMessage: 'is required' },
    endDate: { in: 'body', isDate: true, notEmpty: true, errorMessage: 'is required' },
    amount: { in: 'body', isNumeric: true, notEmpty: true, errorMessage: 'is required' },
    invoiceEmail: { in: 'body', isEmail: true, optional: true }
  });
};

export const putTripCheckOut = (commandDispatcher: CommandDispatcher): RequestHandler => {
  return async (request: Request, response: Response) => {
    const { id } = request.params;
    const { endDate, amount, invoiceEmail } = request.body;

    await commandDispatcher.dispatch(
      new FinirUnSejourCommand(id, new Date(endDate), +amount, invoiceEmail || null)
    );

    response.status(201).send();
  };
};
