import { SejourSnapshot } from '@/deroulement-sejour/domain/sejour/sejour';
import { GetSejourParId } from '@/deroulement-sejour/use-cases/get-sejour-par-id';
import { Request, RequestHandler, Response } from 'express';
import { checkSchema, ValidationChain } from 'express-validator';

export const getTripByIdValidator = (): ValidationChain[] => {
  return checkSchema({
    id: { in: 'params', isString: true, errorMessage: 'is required' }
  });
};

export const getTripById = (query: GetSejourParId): RequestHandler => {
  return async (request: Request, response: Response) => {
    const { id } = request.params;

    const result = await query.handle(id);

    response.status(200).send({ data: format(result) });
  };
};

type ViewModel = Readonly<{
  id: string;
  bookingId: string;
  priceByNight: number;
  checkInDate: string;
  expectedEndDate: string;
  endDate: Nullable<string>;
  amount: Nullable<number>;
}>;

const format = (viewModel: SejourSnapshot): ViewModel => ({
  id: viewModel.id,
  bookingId: viewModel.reservationId,
  priceByNight: viewModel.prixParNuit,
  checkInDate: viewModel.dateDEnregistrement.toISOString(),
  expectedEndDate: viewModel.dateDeFinPrevue.toISOString(),
  endDate: viewModel.dateDeFin ? viewModel.dateDeFin.toISOString() : null,
  amount: viewModel.montantPaye ? +viewModel.montantPaye : null
});
