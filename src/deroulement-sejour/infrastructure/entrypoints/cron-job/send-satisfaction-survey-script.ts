import { Dependencies, dependenciesContainer } from '@/2-configuration/dependencies-container';
import { defaultConfiguration } from '@/deroulement-sejour/2-configuration/default-configuration';
import { EnvoyerQuestionnaireDeSatisfactionCommand } from '@/deroulement-sejour/use-cases/envoyer-questionnaire-de-satisfaction/envoyer-questionnaire-de-satisfaction-command';
import dotenv from 'dotenv-defaults';

dotenv.config();

const dependencies = dependenciesContainer();
const scriptName = 'Send Satisfaction Survey';
const wording = {
  succeeded: `[CRON JOB][${defaultConfiguration.boundedContextName}]: ${scriptName} succeeded`,
  failed: `[CRON JOB][${defaultConfiguration.boundedContextName}]: ${scriptName} failed: [FAILED_MESSAGE]`
};

const script = async (dependencies: Dependencies) => {
  await dependencies.deroulementSejourDependenciesContainer.commandDispatcher.dispatch(
    new EnvoyerQuestionnaireDeSatisfactionCommand(new Date())
  );
};

script(dependencies)
  .then(() => {
    dependencies.apiLogger.info(wording.succeeded);
    process.exit(0);
  })
  .catch((e) => {
    dependencies.apiLogger.error(wording.failed.replace('[FAILED_MESSAGE]', e.message));
    process.exit(1);
  });
