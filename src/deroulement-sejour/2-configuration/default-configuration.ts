export const defaultConfiguration = {
  boundedContextName: 'TripProgress',
  aggregatesName: {
    sejour: 'Trip'
  },
  db: {
    schema: 'deroulement_sejour',
    sejour: 'sejour'
  },
  mail: {
    relationClient: 'relation-client@email.com'
  }
};
