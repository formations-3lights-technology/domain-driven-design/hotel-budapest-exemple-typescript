import { expressMiddlewares } from '@/2-configuration/express-middlewares';
import { DeroulementSejourDependenciesContainer } from '@/deroulement-sejour/2-configuration/root-deroulement-sejour-dependencies-container';
import {
  getTripById,
  getTripByIdValidator
} from '@/deroulement-sejour/infrastructure/entrypoints/endpoints/get-trip-by-id-endpoint';
import {
  putTripCheckIn,
  putTripCheckInValidator
} from '@/deroulement-sejour/infrastructure/entrypoints/endpoints/put-trip-check-in-endpoint';
import {
  putTripCheckOut,
  putTripCheckOutValidator
} from '@/deroulement-sejour/infrastructure/entrypoints/endpoints/put-trip-check-out-endpoint';
import { Router } from 'express';
import asyncHandler from 'express-async-handler';

export interface RootEndpoints {
  unprotected: (router: Router, dependencies: DeroulementSejourDependenciesContainer) => void;
}

export const rootDeroulementSejourEndpoints: RootEndpoints = {
  unprotected: (router: Router, { commandDispatcher, queries }: DeroulementSejourDependenciesContainer): void => {
    /* READ */
    router.get(
      '/trips/:id',
      getTripByIdValidator(),
      expressMiddlewares.preErrorHandling(),
      asyncHandler(getTripById(queries.getSejourParId))
    );

    /* WRITE */
    router.put(
      '/trips/check-in',
      putTripCheckInValidator(),
      expressMiddlewares.preErrorHandling(),
      asyncHandler(putTripCheckIn(commandDispatcher))
    );

    router.put(
      '/trips/:id/check-out',
      putTripCheckOutValidator(),
      expressMiddlewares.preErrorHandling(),
      asyncHandler(putTripCheckOut(commandDispatcher))
    );
  }
};
