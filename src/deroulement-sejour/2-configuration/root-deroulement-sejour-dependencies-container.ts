import { DependenciesContainer } from '@/2-configuration/dependencies-container';
import { Mail } from '@/deroulement-sejour/domain/mail/mail';
import { MailSender } from '@/deroulement-sejour/domain/mail/Mail-sender';
import { EnvoiFactureDemandeEvent } from '@/deroulement-sejour/domain/sejour/envoi-facture-demande-event';
import { SejourTermineEvent } from '@/deroulement-sejour/domain/sejour/sejour-termine-event';
import { HttpReservationLoader } from '@/deroulement-sejour/infrastructure/gateways/http-reservation-loader';
import { PostgresSejourRepository } from '@/deroulement-sejour/infrastructure/gateways/postgres-sejour-repository';
import { DemarrerUnSejourCommand } from '@/deroulement-sejour/use-cases/demarrer-sejour/demarrer-un-sejour-command';
import { DemarrerUnSejourCommandHandler } from '@/deroulement-sejour/use-cases/demarrer-sejour/demarrer-un-sejour-command-handler';
import { EnvoyerQuestionnaireDeSatisfactionCommand } from '@/deroulement-sejour/use-cases/envoyer-questionnaire-de-satisfaction/envoyer-questionnaire-de-satisfaction-command';
import { EnvoyerQuestionnaireDeSatisfactionCommandHandler } from '@/deroulement-sejour/use-cases/envoyer-questionnaire-de-satisfaction/envoyer-questionnaire-de-satisfaction-command-handler';
import { FinirUnSejourCommand } from '@/deroulement-sejour/use-cases/finir-sejour/command-handler/finir-un-sejour-command';
import { FinirUnSejourCommandHandler } from '@/deroulement-sejour/use-cases/finir-sejour/command-handler/finir-un-sejour-command-handler';
import { EnvoyerUneFactureEventListener } from '@/deroulement-sejour/use-cases/finir-sejour/event-listeners/envoyer-une-facture-event-listener';
import { NotifierServiceDEntretienEventListener } from '@/deroulement-sejour/use-cases/finir-sejour/event-listeners/notifier-service-d-entretien-event-listener';
import { GetSejourParId } from '@/deroulement-sejour/use-cases/get-sejour-par-id';
import { CommandDispatcherMiddleware } from '@/shared/command-dispatcher/Command-dispatcher-middleware';
import { InternalCommandDispatcher } from '@/shared/command-dispatcher/internal-command-dispatcher';
import { ApiLogger } from '@/shared/domain/gateways/Api-logger';
import { HttpClient } from '@/shared/domain/gateways/Http-client';
import { IdentityGenerator } from '@/shared/domain/gateways/Identity-generator';
import { TransactionPerformer } from '@/shared/domain/gateways/Transaction-performer';
import { EventPublisherMiddleware } from '@/shared/event-publisher/Event-publisher-middleware';
import { InternalEventPublisher } from '@/shared/event-publisher/internal-event-publisher';
import { TypeORMClient } from '@/shared/infrastructure/gateways/db-client/type-orm/type-orm-client';

export type DeroulementSejourDependenciesContainer = DependenciesContainer<{
  getSejourParId: GetSejourParId;
}>;

export const rootDeroulementSejourDependenciesContainer = (
  apiLogger: ApiLogger,
  identityGenerator: IdentityGenerator,
  transactionPerformer: TransactionPerformer,
  middlewares: { commandDispatcher: CommandDispatcherMiddleware[]; eventPublisher: EventPublisherMiddleware[] },
  typeORMClient: TypeORMClient,
  chambreReservationHttpClient: HttpClient,
  facturationHttpClient: HttpClient
): DeroulementSejourDependenciesContainer => {
  const sejourRepository = new PostgresSejourRepository(typeORMClient, identityGenerator);
  const reservationLoader = new HttpReservationLoader(chambreReservationHttpClient);
  const mailSender: MailSender = {
    async send(mail: Mail): Promise<void> {
      apiLogger.info(JSON.stringify(mail));
    }
  };

  const eventPublisher = new InternalEventPublisher(apiLogger)
    .registerMiddleware(middlewares.eventPublisher)
    .registerListeners({
      [SejourTermineEvent.label]: [new NotifierServiceDEntretienEventListener(apiLogger)],
      [EnvoiFactureDemandeEvent.label]: [new EnvoyerUneFactureEventListener(reservationLoader, facturationHttpClient)]
    });
  const commandDispatcher = new InternalCommandDispatcher(transactionPerformer)
    .registerMiddleware(middlewares.commandDispatcher)
    .registerHandlers({
      [DemarrerUnSejourCommand.label]: new DemarrerUnSejourCommandHandler(
        sejourRepository,
        reservationLoader,
        eventPublisher
      ),
      [FinirUnSejourCommand.label]: new FinirUnSejourCommandHandler(sejourRepository, eventPublisher),
      [EnvoyerQuestionnaireDeSatisfactionCommand.label]: new EnvoyerQuestionnaireDeSatisfactionCommandHandler(
        sejourRepository,
        reservationLoader,
        mailSender
      )
    });

  return {
    commandDispatcher,
    eventPublisher,
    queries: {
      getSejourParId: new GetSejourParId(sejourRepository)
    }
  };
};
