import { SejourTypeOrmEntity } from '@/deroulement-sejour/infrastructure/gateways/sejour-type-orm-entity';

export const rootDeroulementSejourTypeOrmEntities = [SejourTypeOrmEntity];
