import { expect, fakeHttpClient } from '@/1-tests/utils';
import { fakeReservationLoader } from '@/deroulement-sejour/1-tests/deroulement-sejour-fake-dependencies';
import { ReservationLoader } from '@/deroulement-sejour/domain/reservation/Reservation-loader';
import { EnvoiFactureDemandeEvent } from '@/deroulement-sejour/domain/sejour/envoi-facture-demande-event';
import { EnvoyerUneFactureEventListener } from '@/deroulement-sejour/use-cases/finir-sejour/event-listeners/envoyer-une-facture-event-listener';
import { HttpClient } from '@/shared/domain/gateways/Http-client';
import { EventListener } from '@/shared/event-publisher/Event-listener';
import { StubbedType } from '@salesforce/ts-sinon';

describe('Unit | Déroulement séjour | Envoyer une facture', () => {
  let reservationLoader: StubbedType<ReservationLoader>;
  let httpClient: StubbedType<HttpClient>;
  let underTest: EventListener;

  beforeEach(() => {
    reservationLoader = fakeReservationLoader();
    httpClient = fakeHttpClient();
    underTest = new EnvoyerUneFactureEventListener(reservationLoader, httpClient);
  });

  describe('Tout se passe bien', () => {
    it('Mail spécifié', async () => {
      // GIVEN
      const event = new EnvoiFactureDemandeEvent('sejour-id', 'reservation-id', 'facture@email.com', 200);

      // WHEN
      await underTest.listen(event);

      // THEN
      expect(reservationLoader.getParIdOuErreur).to.not.have.been.called();
      expect(httpClient.post).to.have.been.calledOnceWithExactly('/invoices/send', {
        invoiceEmail: 'facture@email.com',
        amount: 200
      });
    });

    it('Mail non spécifié', async () => {
      // GIVEN
      reservationLoader.getParIdOuErreur.resolves({
        id: 'reservation-id',
        mailDeReservation: 'reservation@email.com',
        prixParNuit: 50,
        dateDeDebut: new Date('2023-10-16T09:18:00+02:00'),
        dateDeFin: new Date('2023-10-17T09:18:00+02:00')
      });
      const event = new EnvoiFactureDemandeEvent('sejour-id', 'reservation-id', null, 200);

      // WHEN
      await underTest.listen(event);

      // THEN
      expect(reservationLoader.getParIdOuErreur).to.have.been.calledOnceWithExactly('reservation-id');
      expect(httpClient.post).to.have.been.calledOnceWithExactly('/invoices/send', {
        invoiceEmail: 'reservation@email.com',
        amount: 200
      });
    });
  });
});
