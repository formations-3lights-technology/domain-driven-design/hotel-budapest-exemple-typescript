import { expect } from '@/1-tests/utils';
import {
  fakeMailSender,
  fakeReservationLoader,
  fakeSejourRepository
} from '@/deroulement-sejour/1-tests/deroulement-sejour-fake-dependencies';
import { MailSender } from '@/deroulement-sejour/domain/mail/Mail-sender';
import { ReservationLoader } from '@/deroulement-sejour/domain/reservation/Reservation-loader';
import { SejourRepository } from '@/deroulement-sejour/domain/sejour/Sejour-repository';
import { EnvoyerQuestionnaireDeSatisfactionCommand } from '@/deroulement-sejour/use-cases/envoyer-questionnaire-de-satisfaction/envoyer-questionnaire-de-satisfaction-command';
import { EnvoyerQuestionnaireDeSatisfactionCommandHandler } from '@/deroulement-sejour/use-cases/envoyer-questionnaire-de-satisfaction/envoyer-questionnaire-de-satisfaction-command-handler';
import { CommandHandler } from '@/shared/command-dispatcher/Command-handler';
import { StubbedType } from '@salesforce/ts-sinon';

describe('Unit | Déroulement séjour | Envoyer un questionnaire de satisfaction', () => {
  let sejourRepository: StubbedType<SejourRepository>;
  let reservationLoader: StubbedType<ReservationLoader>;
  let mailSender: StubbedType<MailSender>;
  let underTest: CommandHandler;

  beforeEach(() => {
    sejourRepository = fakeSejourRepository();
    reservationLoader = fakeReservationLoader();
    mailSender = fakeMailSender();
    underTest = new EnvoyerQuestionnaireDeSatisfactionCommandHandler(sejourRepository, reservationLoader, mailSender);
  });

  it('Tout se passe bien', async () => {
    // GIVEN
    sejourRepository.getParDateDeFin.resolves([
      {
        id: 'sejour-id',
        reservationId: 'reservation-id',
        prixParNuit: 50,
        dateDEnregistrement: new Date('2023-10-16T09:18:00+02:00'),
        dateDeFinPrevue: new Date('2023-10-20T09:18:00+02:00'),
        dateDeFin: new Date('2023-10-19T09:18:00+02:00'),
        montantPaye: 100
      },
      {
        id: 'sejour-id-2',
        reservationId: 'reservation-id-2',
        prixParNuit: 50,
        dateDEnregistrement: new Date('2023-10-16T09:18:00+02:00'),
        dateDeFinPrevue: new Date('2023-10-20T09:18:00+02:00'),
        dateDeFin: new Date('2023-10-19T09:18:00+02:00'),
        montantPaye: 100
      }
    ]);
    reservationLoader.getParIdOuErreur
      .onCall(0)
      .resolves({
        id: 'reservation-id',
        mailDeReservation: 'reservation@email.com',
        prixParNuit: 50,
        dateDeDebut: new Date('2023-10-16T09:18:00+02:00'),
        dateDeFin: new Date('2023-10-17T09:18:00+02:00')
      })
      .onCall(1)
      .resolves({
        id: 'reservation-id-2',
        mailDeReservation: 'reservation-2@email.com',
        prixParNuit: 50,
        dateDeDebut: new Date('2023-10-16T09:18:00+02:00'),
        dateDeFin: new Date('2023-10-17T09:18:00+02:00')
      });
    const command = new EnvoyerQuestionnaireDeSatisfactionCommand(new Date('2023-10-19T09:18:00+02:00'));

    // WHEN
    await underTest.handle(command);

    // THEN
    expect(sejourRepository.getParDateDeFin).to.have.been.calledWith(new Date('2023-10-19T09:18:00+02:00'));
    expect(reservationLoader.getParIdOuErreur).to.have.been.callCount(2);
    expect(reservationLoader.getParIdOuErreur.getCall(0).args).to.eql(['reservation-id']);
    expect(reservationLoader.getParIdOuErreur.getCall(1).args).to.eql(['reservation-id-2']);

    expect(mailSender.send).to.have.been.callCount(2);
    expect(mailSender.send.getCall(0).args).to.eql([
      {
        from: 'relation-client@email.com',
        to: 'reservation@email.com',
        message: 'Questionnaire de satisfaction'
      }
    ]);
    expect(mailSender.send.getCall(1).args).to.eql([
      {
        from: 'relation-client@email.com',
        to: 'reservation-2@email.com',
        message: 'Questionnaire de satisfaction'
      }
    ]);
  });
});
