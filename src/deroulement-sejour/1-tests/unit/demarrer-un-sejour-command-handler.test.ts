import { expect, fakeEventPublisher } from '@/1-tests/utils';
import {
  fakeReservationLoader,
  fakeSejourRepository
} from '@/deroulement-sejour/1-tests/deroulement-sejour-fake-dependencies';
import { Erreur } from '@/deroulement-sejour/domain/erreurs';
import { ReservationLoader } from '@/deroulement-sejour/domain/reservation/Reservation-loader';
import { SejourEnregistreEvent } from '@/deroulement-sejour/domain/sejour/sejour-enregistre-event';
import { SejourRepository } from '@/deroulement-sejour/domain/sejour/Sejour-repository';
import { DemarrerUnSejourCommand } from '@/deroulement-sejour/use-cases/demarrer-sejour/demarrer-un-sejour-command';
import { DemarrerUnSejourCommandHandler } from '@/deroulement-sejour/use-cases/demarrer-sejour/demarrer-un-sejour-command-handler';
import { CommandHandler } from '@/shared/command-dispatcher/Command-handler';
import { EventPublisher } from '@/shared/event-publisher/internal-event-publisher';
import { StubbedType } from '@salesforce/ts-sinon';
import assert from 'assert';

describe('Unit | Déroulement séjour | Démarrer un séjour', () => {
  let sejourRepository: StubbedType<SejourRepository>;
  let reservationLoader: StubbedType<ReservationLoader>;
  let eventPublisher: StubbedType<EventPublisher>;
  let underTest: CommandHandler;

  beforeEach(() => {
    sejourRepository = fakeSejourRepository();
    reservationLoader = fakeReservationLoader();
    eventPublisher = fakeEventPublisher();
    underTest = new DemarrerUnSejourCommandHandler(sejourRepository, reservationLoader, eventPublisher);
  });

  it('Tout se passe bien', async () => {
    // GIVEN
    sejourRepository.nextId.resolves('next-id');
    reservationLoader.getParIdOuErreur.resolves({
      id: 'reservation-id',
      mailDeReservation: 'reservation@email.com',
      prixParNuit: 50,
      dateDeDebut: new Date('2023-10-16T09:18:00+02:00'),
      dateDeFin: new Date('2023-10-17T09:18:00+02:00')
    });
    const command = new DemarrerUnSejourCommand('reservation-id', new Date('2023-10-16T09:18:00+02:00'));

    // WHEN
    await underTest.handle(command);

    // THEN
    expect(reservationLoader.getParIdOuErreur).to.have.been.calledWith('reservation-id');
    expect(sejourRepository.save).to.have.been.calledWith({
      id: 'next-id',
      reservationId: 'reservation-id',
      prixParNuit: 50,
      dateDEnregistrement: new Date('2023-10-16T09:18:00+02:00'),
      dateDeFinPrevue: new Date('2023-10-17T09:18:00+02:00'),
      dateDeFin: null,
      montantPaye: null
    });
    expect(eventPublisher.publish).to.have.been.calledWith([
      new SejourEnregistreEvent('next-id', 'reservation-id', new Date('2023-10-16T09:18:00+02:00'))
    ]);
  });

  describe('Informer quand il y a une erreur', () => {
    beforeEach(() => {
      reservationLoader.getParIdOuErreur.resolves({
        id: 'reservation-id',
        mailDeReservation: 'reservation@email.com',
        prixParNuit: 50,
        dateDeDebut: new Date('2023-10-16T09:18:00+02:00'),
        dateDeFin: new Date('2023-10-30T09:18:00+02:00')
      });
    });

    it("La réservation n'existe pas", async () => {
      // GIVEN
      reservationLoader.getParIdOuErreur.rejects(new Erreur.Reservation.NonTrouvee());
      const command = new DemarrerUnSejourCommand('reservation-id', new Date('2023-10-16T09:18:00+02:00'));

      try {
        // WHEN
        await underTest.handle(command);
        assert.fail();
      } catch (e) {
        // THEN
        expect(e).to.instanceof(Erreur.Reservation.NonTrouvee);
        expect(e.message).to.eql('Booking is not found');
      }
    });

    it('Il est trop tard', async () => {
      // GIVEN
      const command = new DemarrerUnSejourCommand('reservation-id', new Date('2023-10-18T09:18:00+02:00'));

      try {
        // WHEN
        await underTest.handle(command);
        assert.fail();
      } catch (e) {
        // THEN
        expect(e).to.instanceof(Erreur.Sejour.EnregistrementTropTard);
        expect(e.message).to.eql('Check in is too late');
      }
    });
  });
});
