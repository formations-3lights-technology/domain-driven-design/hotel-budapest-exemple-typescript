import { expect, fakeEventPublisher } from '@/1-tests/utils';
import { fakeSejourRepository } from '@/deroulement-sejour/1-tests/deroulement-sejour-fake-dependencies';
import { Erreur } from '@/deroulement-sejour/domain/erreurs';
import { EnvoiFactureDemandeEvent } from '@/deroulement-sejour/domain/sejour/envoi-facture-demande-event';
import { SejourRepository } from '@/deroulement-sejour/domain/sejour/Sejour-repository';
import { SejourTermineEvent } from '@/deroulement-sejour/domain/sejour/sejour-termine-event';
import { FinirUnSejourCommand } from '@/deroulement-sejour/use-cases/finir-sejour/command-handler/finir-un-sejour-command';
import { FinirUnSejourCommandHandler } from '@/deroulement-sejour/use-cases/finir-sejour/command-handler/finir-un-sejour-command-handler';
import { CommandHandler } from '@/shared/command-dispatcher/Command-handler';
import { EventPublisher } from '@/shared/event-publisher/internal-event-publisher';
import { StubbedType } from '@salesforce/ts-sinon';
import assert from 'assert';

describe('Unit | Déroulement séjour | Finir un séjour', () => {
  let sejourRepository: StubbedType<SejourRepository>;
  let eventPublisher: StubbedType<EventPublisher>;
  let underTest: CommandHandler;

  beforeEach(() => {
    sejourRepository = fakeSejourRepository();
    eventPublisher = fakeEventPublisher();
    underTest = new FinirUnSejourCommandHandler(sejourRepository, eventPublisher);
  });

  it('Tout se passe bien', async () => {
    // GIVEN
    sejourRepository.getParIdOuErreur.resolves({
      id: 'sejour-id',
      reservationId: 'reservation-id',
      prixParNuit: 50,
      dateDEnregistrement: new Date('2023-10-16T09:18:00+02:00'),
      dateDeFinPrevue: new Date('2023-10-20T09:18:00+02:00'),
      dateDeFin: null,
      montantPaye: null
    });
    const command = new FinirUnSejourCommand(
      'sejour-id',
      new Date('2023-10-19T09:18:00+02:00'),
      200,
      'facture@email.com'
    );

    // WHEN
    await underTest.handle(command);

    // THEN
    expect(sejourRepository.getParIdOuErreur).to.have.been.calledWith('sejour-id');
    expect(sejourRepository.save).to.have.been.calledWith({
      id: 'sejour-id',
      reservationId: 'reservation-id',
      prixParNuit: 50,
      dateDEnregistrement: new Date('2023-10-16T09:18:00+02:00'),
      dateDeFinPrevue: new Date('2023-10-20T09:18:00+02:00'),
      dateDeFin: new Date('2023-10-19T09:18:00+02:00'),
      montantPaye: 200
    });
    expect(eventPublisher.publish).to.have.been.calledWith([
      new SejourTermineEvent('sejour-id', new Date('2023-10-19T09:18:00+02:00')),
      new EnvoiFactureDemandeEvent('sejour-id', 'reservation-id', 'facture@email.com', 200)
    ]);
  });

  describe('Informer quand il y a une erreur', () => {
    it("Le séjour n'existe pas", async () => {
      // GIVEN
      sejourRepository.getParIdOuErreur.rejects(new Erreur.Sejour.NonTrouve());
      const command = new FinirUnSejourCommand(
        'sejour-id',
        new Date('2023-10-19T09:18:00+02:00'),
        100,
        'facture@email.com'
      );

      try {
        // WHEN
        await underTest.handle(command);
        assert.fail();
      } catch (e) {
        // THEN
        expect(e).to.instanceof(Erreur.Sejour.NonTrouve);
        expect(e.message).to.eql('Trip is not found');
      }
    });

    it('Le montant payé ne correspond pas', async () => {
      // GIVEN
      sejourRepository.getParIdOuErreur.resolves({
        id: 'sejour-id',
        reservationId: 'reservation-id',
        prixParNuit: 50,
        dateDEnregistrement: new Date('2023-10-16T09:18:00+02:00'),
        dateDeFinPrevue: new Date('2023-10-20T09:18:00+02:00'),
        dateDeFin: null,
        montantPaye: null
      });
      const command = new FinirUnSejourCommand(
        'sejour-id',
        new Date('2023-10-19T09:18:00+02:00'),
        100,
        'facture@email.com'
      );

      try {
        // WHEN
        await underTest.handle(command);
        assert.fail();
      } catch (e) {
        // THEN
        expect(e).to.instanceof(Erreur.Sejour.MontantPayeNonConforme);
        expect(e.message).to.eql('Amount is not good');
      }
    });
  });
});
