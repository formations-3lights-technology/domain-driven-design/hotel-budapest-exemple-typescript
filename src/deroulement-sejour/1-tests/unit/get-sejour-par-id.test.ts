import { expect } from '@/1-tests/utils';
import { fakeSejourRepository } from '@/deroulement-sejour/1-tests/deroulement-sejour-fake-dependencies';
import { Erreur } from '@/deroulement-sejour/domain/erreurs';
import { SejourRepository } from '@/deroulement-sejour/domain/sejour/Sejour-repository';
import { GetSejourParId } from '@/deroulement-sejour/use-cases/get-sejour-par-id';
import { StubbedType } from '@salesforce/ts-sinon';
import assert from 'assert';

describe('Unit | Déroulement séjour | Get séjour par id', () => {
  let sejourRepository: StubbedType<SejourRepository>;
  let underTest: GetSejourParId;

  beforeEach(() => {
    sejourRepository = fakeSejourRepository();
    underTest = new GetSejourParId(sejourRepository);
  });

  it('Tout se passe bien', async () => {
    // GIVEN
    sejourRepository.getParIdOuErreur.resolves({
      id: 'sejour-id',
      reservationId: 'reservation-id',
      prixParNuit: 50,
      dateDEnregistrement: new Date('2023-10-16T09:18:00+02:00'),
      dateDeFinPrevue: new Date('2023-10-20T09:18:00+02:00'),
      dateDeFin: null,
      montantPaye: null
    });

    // WHEN
    const result = await underTest.handle('sejour-id');

    // THEN
    expect(sejourRepository.getParIdOuErreur).to.have.been.calledWith('sejour-id');
    expect(result).to.eql({
      id: 'sejour-id',
      reservationId: 'reservation-id',
      prixParNuit: 50,
      dateDEnregistrement: new Date('2023-10-16T09:18:00+02:00'),
      dateDeFinPrevue: new Date('2023-10-20T09:18:00+02:00'),
      dateDeFin: null,
      montantPaye: null
    });
  });

  describe('Informer quand il y a une erreur', () => {
    it("La réservation n'existe pas", async () => {
      // GIVEN
      sejourRepository.getParIdOuErreur.rejects(new Erreur.Sejour.NonTrouve());

      try {
        // WHEN
        await underTest.handle('inconnu');
        assert.fail();
      } catch (e) {
        // THEN
        expect(e).to.instanceof(Erreur.Sejour.NonTrouve);
        expect(e.message).to.eql('Trip is not found');
      }
    });
  });
});
