import { expect, fakeApiLogger } from '@/1-tests/utils';
import { SejourTermineEvent } from '@/deroulement-sejour/domain/sejour/sejour-termine-event';
import { NotifierServiceDEntretienEventListener } from '@/deroulement-sejour/use-cases/finir-sejour/event-listeners/notifier-service-d-entretien-event-listener';
import { ApiLogger } from '@/shared/domain/gateways/Api-logger';
import { EventListener } from '@/shared/event-publisher/Event-listener';
import { StubbedType } from '@salesforce/ts-sinon';

describe("Unit | Déroulement séjour | Notifier service d'entretien", () => {
  let apiLogger: StubbedType<ApiLogger>;
  let underTest: EventListener;

  beforeEach(() => {
    apiLogger = fakeApiLogger();
    underTest = new NotifierServiceDEntretienEventListener(apiLogger);
  });

  it('Tout se passe bien', async () => {
    // GIVEN
    const event = new SejourTermineEvent('sejour-id', new Date('2023-10-19T09:18:00+02:00'));

    // WHEN
    await underTest.listen(event);

    // THEN
    expect(apiLogger.info).to.have.been.calledWith("Service d'entretien notifié");
  });
});
