import { expect, fakeHttpClient } from '@/1-tests/utils';
import { Erreur } from '@/deroulement-sejour/domain/erreurs';
import { ReservationLoader } from '@/deroulement-sejour/domain/reservation/Reservation-loader';
import { HttpReservationLoader } from '@/deroulement-sejour/infrastructure/gateways/http-reservation-loader';
import { HttpClient } from '@/shared/domain/gateways/Http-client';
import { StubbedType } from '@salesforce/ts-sinon';
import assert from 'assert';

describe('Integration | Déroulement séjour | HTTP Réservation loader', () => {
  let httpClient: StubbedType<HttpClient>;
  let underTest: ReservationLoader;

  beforeEach(() => {
    httpClient = fakeHttpClient();
    underTest = new HttpReservationLoader(httpClient);
  });

  describe('Get', () => {
    it('Tout se passe bien', async () => {
      // GIVEN
      httpClient.get.resolves({
        response: {
          data: {
            id: 'reservation-id',
            bookingEmail: 'email@reservation.com',
            roomNumber: '11',
            totalPerson: 2,
            priceByNight: 10,
            startDate: '2023-10-15T07:18:00.000Z',
            endDate: '2023-10-16T07:18:00.000Z'
          }
        }
      });

      // WHEN
      const result = await underTest.getParIdOuErreur('reservation-id');

      // THEN
      expect(httpClient.get).to.have.been.calledOnceWithExactly('/booking/rooms/reservation-id');
      expect(result).to.eql({
        id: 'reservation-id',
        mailDeReservation: 'email@reservation.com',
        prixParNuit: 10,
        dateDeDebut: new Date('2023-10-15T07:18:00.000Z'),
        dateDeFin: new Date('2023-10-16T07:18:00.000Z')
      });
    });

    it('Non trouvé', async () => {
      // GIVEN
      httpClient.get.rejects();

      try {
        // WHEN
        await underTest.getParIdOuErreur('inconnu');
        assert.fail();
      } catch (e) {
        // THEN
        expect(e).to.instanceof(Erreur.Reservation.NonTrouvee);
        expect(e.message).to.eql('Booking is not found');
      }
    });
  });
});
