import { dropAllTableTypeORM, expect, fakeDependencies, fakeIdentityGenerator } from '@/1-tests/utils';
import { Erreur } from '@/deroulement-sejour/domain/erreurs';
import { SejourSnapshot } from '@/deroulement-sejour/domain/sejour/sejour';
import { SejourRepository } from '@/deroulement-sejour/domain/sejour/Sejour-repository';
import { PostgresSejourRepository } from '@/deroulement-sejour/infrastructure/gateways/postgres-sejour-repository';
import { SejourTypeOrmEntity } from '@/deroulement-sejour/infrastructure/gateways/sejour-type-orm-entity';
import { IdentityGenerator } from '@/shared/domain/gateways/Identity-generator';
import { TypeORMClient } from '@/shared/infrastructure/gateways/db-client/type-orm/type-orm-client';
import { typeORMDbClient } from '@/shared/infrastructure/gateways/db-client/type-orm/type-orm-db-client';
import { NodeEnvironmentVariables } from '@/shared/infrastructure/gateways/node-environment-variables';
import { StubbedType } from '@salesforce/ts-sinon';
import assert from 'assert';

describe('Integration | Déroulement séjour | Postgres Séjour repository', () => {
  const environmentVariables = new NodeEnvironmentVariables();
  const dbClient = typeORMDbClient(environmentVariables);
  let identityGenerator: StubbedType<IdentityGenerator>;
  let underTest: SejourRepository;

  beforeEach(() => {
    const typeORMClient = new TypeORMClient(
      dbClient,
      fakeDependencies().apiLogger,
      fakeDependencies().contexteService,
      environmentVariables
    );
    identityGenerator = fakeIdentityGenerator();
    underTest = new PostgresSejourRepository(typeORMClient, identityGenerator);
  });

  afterEach(async () => {
    await dropAllTableTypeORM(dbClient);
  });

  describe('Next id', () => {
    it('Tout se passe bien', async () => {
      // GIVEN
      identityGenerator.generate.returns('sejour-id');

      // WHEN
      const result = await underTest.nextId();

      // THEN
      expect(result).to.eql('sejour-id');
    });
  });

  describe('Save', () => {
    it('Tout se passe bien', async () => {
      // GIVEN
      const sejour: SejourSnapshot = {
        id: 'sejour-id',
        reservationId: 'reservation-id',
        prixParNuit: 50,
        dateDEnregistrement: new Date('2023-10-16T09:18:00+02:00'),
        dateDeFinPrevue: new Date('2023-10-17T09:18:00+02:00'),
        dateDeFin: null,
        montantPaye: null
      };

      // WHEN
      await underTest.save(sejour);

      // THEN
      const result = await (await dbClient).createQueryBuilder(SejourTypeOrmEntity, 's').select().getRawMany();
      expect(result).to.eql([
        {
          s_id: 'sejour-id',
          s_reservation_id: 'reservation-id',
          s_prix_par_nuit: 50,
          s_date_d_enregistrement: new Date('2023-10-16T09:18:00+02:00'),
          s_date_de_fin_prevue: new Date('2023-10-17T09:18:00+02:00'),
          s_date_de_fin: null,
          s_montant_paye: null
        }
      ]);
    });
  });

  describe('Get', () => {
    it('Tout se passe bien', async () => {
      // GIVEN
      const sejour: SejourSnapshot = {
        id: 'sejour-id',
        reservationId: 'reservation-id',
        prixParNuit: 50,
        dateDEnregistrement: new Date('2023-10-16T09:18:00+02:00'),
        dateDeFinPrevue: new Date('2023-10-17T09:18:00+02:00'),
        dateDeFin: new Date('2023-10-17T09:18:00+02:00'),
        montantPaye: 10
      };
      await underTest.save(sejour);

      // WHEN
      const result = await underTest.getParIdOuErreur('sejour-id');

      // THEN
      expect(result).to.eql(sejour);
    });

    it('Non trouvé', async () => {
      try {
        // WHEN
        await underTest.getParIdOuErreur('inconnu');
        assert.fail();
      } catch (e) {
        // THEN
        expect(e).to.instanceof(Erreur.Sejour.NonTrouve);
        expect(e.message).to.eql('Trip is not found');
      }
    });
  });

  describe('Get par date de fin', () => {
    it('Tout se passe bien', async () => {
      // GIVEN
      const sejourKo: SejourSnapshot = {
        id: 'sejour-id',
        reservationId: 'reservation-id',
        prixParNuit: 50,
        dateDEnregistrement: new Date('2023-10-16T09:18:00+02:00'),
        dateDeFinPrevue: new Date('2023-10-17T09:18:00+02:00'),
        dateDeFin: new Date('2023-10-17T09:18:00+02:00'),
        montantPaye: 10
      };
      const sejourOk: SejourSnapshot = {
        id: 'sejour-id-2',
        reservationId: 'reservation-id-2',
        prixParNuit: 50,
        dateDEnregistrement: new Date('2023-10-16T09:18:00+02:00'),
        dateDeFinPrevue: new Date('2023-10-17T09:18:00+02:00'),
        dateDeFin: new Date('2023-10-22T09:18:00+02:00'),
        montantPaye: 10
      };
      const sejourNonFini: SejourSnapshot = {
        id: 'sejour-id-3',
        reservationId: 'reservation-id-3',
        prixParNuit: 50,
        dateDEnregistrement: new Date('2023-10-16T09:18:00+02:00'),
        dateDeFinPrevue: new Date('2023-10-17T09:18:00+02:00'),
        dateDeFin: null,
        montantPaye: null
      };
      await underTest.save(sejourKo);
      await underTest.save(sejourOk);
      await underTest.save(sejourNonFini);

      // WHEN
      const result = await underTest.getParDateDeFin(new Date('2023-10-22'));

      // THEN
      expect(result).to.eql([sejourOk]);
    });

    it('Non trouvé', async () => {
      // WHEN
      const result = await underTest.getParDateDeFin(new Date('2023-10-22'));

      // THEN
      expect(result).to.eql([]);
    });
  });
});
