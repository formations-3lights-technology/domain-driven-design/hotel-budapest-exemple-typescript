import { expect, fakeDependencies, FakeExpressServer } from '@/1-tests/utils';
import { FinirUnSejourCommand } from '@/deroulement-sejour/use-cases/finir-sejour/command-handler/finir-un-sejour-command';
import { CommandDispatcher } from '@/shared/command-dispatcher/internal-command-dispatcher';
import supertest from 'supertest';

describe('Integration | Déroulement séjour | PUT /trips/check-out', () => {
  let fakeApi: supertest.SuperTest<supertest.Test>;
  let commandDispatcher: CommandDispatcher;
  const baseUrl = '/v1/trips/trip-id/check-out';

  beforeEach(async () => {
    const dependencies = fakeDependencies();
    commandDispatcher = dependencies.deroulementSejourDependenciesContainer.commandDispatcher;
    const fakeServer = await new FakeExpressServer(dependencies).create();
    fakeApi = fakeServer.api;
  });

  describe('Retourne 201', () => {
    it('Email non renseigné', () => {
      // WHEN
      return fakeApi
        .put(baseUrl)
        .send({
          endDate: '2022-11-16',
          amount: 123
        })
        .expect(201)
        .then((_) => {
          // THEN
          expect(commandDispatcher.dispatch).to.have.been.calledOnceWithExactly(
            new FinirUnSejourCommand('trip-id', new Date('2022-11-16'), 123, null)
          );
        });
    });

    it('Tout est renseigné', () => {
      // WHEN
      return fakeApi
        .put(baseUrl)
        .send({
          endDate: '2022-11-16',
          amount: 123,
          invoiceEmail: 'facture@email.com'
        })
        .expect(201)
        .then((_) => {
          // THEN
          expect(commandDispatcher.dispatch).to.have.been.calledOnceWithExactly(
            new FinirUnSejourCommand('booking-id', new Date('2022-11-16'), 123, 'facture@email.com')
          );
        });
    });
  });
});
