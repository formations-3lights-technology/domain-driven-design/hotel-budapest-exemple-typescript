import { expect, fakeDependencies, FakeExpressServer } from '@/1-tests/utils';
import { DemarrerUnSejourCommand } from '@/deroulement-sejour/use-cases/demarrer-sejour/demarrer-un-sejour-command';
import { CommandDispatcher } from '@/shared/command-dispatcher/internal-command-dispatcher';
import supertest from 'supertest';

describe('Integration | Déroulement séjour | PUT /trips/check-in', () => {
  let fakeApi: supertest.SuperTest<supertest.Test>;
  let commandDispatcher: CommandDispatcher;
  const baseUrl = '/v1/trips/check-in';

  beforeEach(async () => {
    const dependencies = fakeDependencies();
    commandDispatcher = dependencies.deroulementSejourDependenciesContainer.commandDispatcher;
    const fakeServer = await new FakeExpressServer(dependencies).create();
    fakeApi = fakeServer.api;
  });

  describe('Retourne 201', () => {
    it('Tout est renseigné', () => {
      // WHEN
      return fakeApi
        .put(baseUrl)
        .send({
          bookingId: 'booking-id',
          startDate: '2022-11-16'
        })
        .expect(201)
        .then((_) => {
          // THEN
          expect(commandDispatcher.dispatch).to.have.been.calledOnceWithExactly(
            new DemarrerUnSejourCommand('booking-id', new Date('2022-11-16'))
          );
        });
    });
  });
});
