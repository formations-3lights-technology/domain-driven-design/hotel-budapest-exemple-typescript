import { expect, fakeDependencies, FakeExpressServer } from '@/1-tests/utils';
import supertest from 'supertest';

describe('Integration | Déroulement séjour | GET /trips/:id', () => {
  let fakeApi: supertest.SuperTest<supertest.Test>;
  let query;
  const baseUrl = '/v1/trips/3';

  beforeEach(async () => {
    const dependencies = fakeDependencies();
    query = dependencies.deroulementSejourDependenciesContainer.queries.getSejourParId;
    const fakeServer = await new FakeExpressServer(dependencies).create();
    fakeApi = fakeServer.api;
  });

  it('Retourne 200', () => {
    // GIVEN
    // @ts-ignore
    query.handle.resolves({
      id: 'sejour-id',
      reservationId: 'reservation-id',
      prixParNuit: 50,
      dateDEnregistrement: new Date('2023-10-16T09:18:00+02:00'),
      dateDeFinPrevue: new Date('2023-10-20T09:18:00+02:00'),
      dateDeFin: null,
      montantPaye: null
    });

    // WHEN
    return fakeApi
      .get(baseUrl)
      .expect(200)
      .then((response) => {
        // THEN
        expect(query.handle).to.have.been.calledOnceWithExactly('3');
        expect(response.body).to.eql({
          data: {
            id: 'sejour-id',
            bookingId: 'reservation-id',
            priceByNight: 50,
            checkInDate: '2023-10-16T07:18:00.000Z',
            expectedEndDate: '2023-10-20T07:18:00.000Z',
            endDate: null,
            amount: null
          }
        });
      });
  });
});
