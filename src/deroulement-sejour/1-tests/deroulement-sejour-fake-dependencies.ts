import { fakeEventPublisher } from '@/1-tests/utils';
import { DeroulementSejourDependenciesContainer } from '@/deroulement-sejour/2-configuration/root-deroulement-sejour-dependencies-container';
import { MailSender } from '@/deroulement-sejour/domain/mail/Mail-sender';
import { ReservationLoader } from '@/deroulement-sejour/domain/reservation/Reservation-loader';
import { SejourRepository } from '@/deroulement-sejour/domain/sejour/Sejour-repository';
import { GetSejourParId } from '@/deroulement-sejour/use-cases/get-sejour-par-id';
import { CommandDispatcher } from '@/shared/command-dispatcher/internal-command-dispatcher';
import { StubbedType, stubInterface } from '@salesforce/ts-sinon';
import sinon from 'sinon';

export const fakeSejourRepository: () => StubbedType<SejourRepository> = () => stubInterface<SejourRepository>(sinon);

export const fakeReservationLoader: () => StubbedType<ReservationLoader> = () =>
  stubInterface<ReservationLoader>(sinon);

export const fakeMailSender: () => StubbedType<MailSender> = () => stubInterface<MailSender>(sinon);

export const deroulementSejourFakeDependencies = (): DeroulementSejourDependenciesContainer => ({
  commandDispatcher: stubInterface<CommandDispatcher>(sinon),
  eventPublisher: fakeEventPublisher(),
  queries: {
    // @ts-ignore
    getSejourParId: stubInterface<GetSejourParId>(sinon)
  }
});
