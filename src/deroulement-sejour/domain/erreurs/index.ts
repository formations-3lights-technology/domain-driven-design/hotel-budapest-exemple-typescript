import { ElementNonTrouve } from '@/shared/domain/erreurs/element-non-trouve';
import { ErreurDeValidation } from '@/shared/domain/erreurs/erreur-de-validation';

export namespace Erreur {
  export namespace Sejour {
    export class NonTrouve extends ElementNonTrouve {
      constructor() {
        super('Trip is not found');
      }
    }

    export class EnregistrementTropTard extends ErreurDeValidation {
      constructor() {
        super('Check in is too late');
      }
    }

    export class MontantPayeNonConforme extends ErreurDeValidation {
      constructor() {
        super('Amount is not good');
      }
    }
  }

  export namespace Reservation {
    export class NonTrouvee extends ElementNonTrouve {
      constructor() {
        super('Booking is not found');
      }
    }
  }
}
