export type Reservation = Readonly<{
  id: string;
  mailDeReservation: string;
  prixParNuit: number;
  dateDeDebut: Date;
  dateDeFin: Date;
}>;
