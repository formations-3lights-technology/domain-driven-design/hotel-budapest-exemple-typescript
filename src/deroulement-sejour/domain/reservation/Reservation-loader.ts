import { Reservation } from '@/deroulement-sejour/domain/reservation/reservation';

export interface ReservationLoader {
  getParIdOuErreur(id: string): Promise<Reservation>;
}
