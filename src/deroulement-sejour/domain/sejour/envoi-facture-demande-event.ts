import { defaultConfiguration } from '@/deroulement-sejour/2-configuration/default-configuration';
import { DomainEvent } from '@/shared/event-publisher/Domain-event';

type EventSerialized = Readonly<{
  tripId: string;
  reservationId: string;
  invoiceMail: Nullable<string>;
  amount: number;
}>;

export class EnvoiFactureDemandeEvent extends DomainEvent<EventSerialized> {
  static readonly label = 'SendInvoiceAskedEvent';
  protected boundedContext = defaultConfiguration.boundedContextName;
  protected aggregate = defaultConfiguration.aggregatesName.sejour;

  constructor(
    readonly sejourId: string,
    readonly reservationId: string,
    readonly mailDeFacturation: Nullable<string>,
    readonly amount: number
  ) {
    super(sejourId);
  }

  isPublic(): boolean {
    return false;
  }

  label(): string {
    return EnvoiFactureDemandeEvent.label;
  }

  version(): string {
    return 'v1';
  }

  protected serializedData(): EventSerialized {
    return {
      tripId: this.sejourId,
      reservationId: this.reservationId,
      invoiceMail: this.mailDeFacturation ? this._apiCrypt.encrypt(this.mailDeFacturation) : null,
      amount: this.amount
    };
  }
}
