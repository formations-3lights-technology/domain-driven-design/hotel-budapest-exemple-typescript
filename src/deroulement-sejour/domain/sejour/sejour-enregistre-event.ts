import { defaultConfiguration } from '@/deroulement-sejour/2-configuration/default-configuration';
import { DomainEvent } from '@/shared/event-publisher/Domain-event';

type EventSerialized = Readonly<{
  id: string;
  reservationId: string;
  checkInDate: string;
}>;

export class SejourEnregistreEvent extends DomainEvent<EventSerialized> {
  static readonly label = 'TripCheckInEvent';
  protected boundedContext = defaultConfiguration.boundedContextName;
  protected aggregate = defaultConfiguration.aggregatesName.sejour;

  constructor(readonly id: string, readonly reservationId: string, readonly dateDEnregistrement: Date) {
    super(id);
  }

  isPublic(): boolean {
    return false;
  }

  label(): string {
    return SejourEnregistreEvent.label;
  }

  version(): string {
    return 'v1';
  }

  protected serializedData(): EventSerialized {
    return {
      id: this.id,
      reservationId: this.reservationId,
      checkInDate: this.dateDEnregistrement.toISOString()
    };
  }
}
