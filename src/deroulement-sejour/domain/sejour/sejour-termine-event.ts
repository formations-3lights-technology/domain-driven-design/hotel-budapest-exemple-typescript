import { defaultConfiguration } from '@/deroulement-sejour/2-configuration/default-configuration';
import { DomainEvent } from '@/shared/event-publisher/Domain-event';

type EventSerialized = Readonly<{
  id: string;
  checkOutDate: string;
}>;

export class SejourTermineEvent extends DomainEvent<EventSerialized> {
  static readonly label = 'TripCheckOutEvent';
  protected boundedContext = defaultConfiguration.boundedContextName;
  protected aggregate = defaultConfiguration.aggregatesName.sejour;

  constructor(readonly id: string, readonly dateDeFin: Date) {
    super(id);
  }

  isPublic(): boolean {
    return false;
  }

  label(): string {
    return SejourTermineEvent.label;
  }

  version(): string {
    return 'v1';
  }

  protected serializedData(): EventSerialized {
    return {
      id: this.id,
      checkOutDate: this.dateDeFin.toISOString()
    };
  }
}
