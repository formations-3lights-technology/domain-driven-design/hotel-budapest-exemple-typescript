import { SejourSnapshot } from '@/deroulement-sejour/domain/sejour/sejour';

export interface SejourRepository {
  nextId(): Promise<string>;

  save(sejour: SejourSnapshot): Promise<void>;

  getParIdOuErreur(id: string): Promise<SejourSnapshot>;

  getParDateDeFin(date: Date): Promise<SejourSnapshot[]>;
}
