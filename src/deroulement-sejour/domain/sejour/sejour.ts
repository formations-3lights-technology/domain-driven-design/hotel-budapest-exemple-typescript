import { Erreur } from '@/deroulement-sejour/domain/erreurs';
import { EnvoiFactureDemandeEvent } from '@/deroulement-sejour/domain/sejour/envoi-facture-demande-event';
import { SejourEnregistreEvent } from '@/deroulement-sejour/domain/sejour/sejour-enregistre-event';
import { SejourTermineEvent } from '@/deroulement-sejour/domain/sejour/sejour-termine-event';
import { Aggregate } from '@/shared/domain/aggregate';
import { differenceInDays } from 'date-fns';

export type SejourSnapshot = Readonly<{
  id: string;
  reservationId: string;
  prixParNuit: number;
  dateDEnregistrement: Date;
  dateDeFinPrevue: Date;
  dateDeFin: Nullable<Date>;
  montantPaye: Nullable<number>;
}>;

export class Sejour extends Aggregate {
  private constructor(
    private readonly id: string,
    private readonly reservationId: string,
    private readonly prixParNuit: number,
    private readonly dateDEnregistrement: Date,
    private readonly dateDeFinPrevue: Date,
    private dateDeFin: Nullable<Date>,
    private montantPaye: Nullable<number>
  ) {
    super();
  }

  get snapshot(): SejourSnapshot {
    return {
      id: this.id,
      reservationId: this.reservationId,
      prixParNuit: this.prixParNuit,
      dateDEnregistrement: this.dateDEnregistrement,
      dateDeFinPrevue: this.dateDeFinPrevue,
      dateDeFin: this.dateDeFin,
      montantPaye: this.montantPaye
    };
  }

  static enregistrement(
    id: string,
    reservationId: string,
    prixParNuit: number,
    dateDEnregistrement: Date,
    dateDeDebutDeLaReservation: Date,
    dateDeFinDeLaReservation: Date
  ): Sejour {
    const differenceEnJours = differenceInDays(dateDEnregistrement, dateDeDebutDeLaReservation);
    if (differenceEnJours > 1) {
      throw new Erreur.Sejour.EnregistrementTropTard();
    }

    const sejour = new Sejour(
      id,
      reservationId,
      prixParNuit,
      dateDEnregistrement,
      dateDeFinDeLaReservation,
      null,
      null
    );

    sejour.apply(new SejourEnregistreEvent(id, reservationId, dateDEnregistrement));

    return sejour;
  }

  static restore(snapshot: SejourSnapshot): Sejour {
    return new Sejour(
      snapshot.id,
      snapshot.reservationId,
      snapshot.prixParNuit,
      snapshot.dateDEnregistrement,
      snapshot.dateDeFinPrevue,
      snapshot.dateDeFin,
      snapshot.montantPaye
    );
  }

  terminer(dateDeFin: Date, mailDeFacturation: Nullable<string>, montantPaye: number): void {
    const dureeDuSejour = Math.abs(differenceInDays(this.dateDEnregistrement, this.dateDeFinPrevue));
    const montantDuSejour = dureeDuSejour * this.prixParNuit;
    if (montantDuSejour !== montantPaye) {
      throw new Erreur.Sejour.MontantPayeNonConforme();
    }

    this.dateDeFin = dateDeFin;
    this.montantPaye = montantPaye;

    this.apply(new SejourTermineEvent(this.id, this.dateDeFin));
    this.apply(new EnvoiFactureDemandeEvent(this.id, this.reservationId, mailDeFacturation, this.montantPaye));
  }
}
