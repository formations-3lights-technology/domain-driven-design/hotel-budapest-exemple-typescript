import { defaultConfiguration } from '@/deroulement-sejour/2-configuration/default-configuration';
import { Command } from '@/shared/command-dispatcher/Command';

type CommandSerialized = Readonly<{
  tripId: string;
  endDate: string;
  amount: number;
  invoiceEmail: Nullable<string>;
}>;

export class FinirUnSejourCommand extends Command<CommandSerialized> {
  static readonly label = 'FinishTripCommand';
  protected readonly boundedContext = defaultConfiguration.boundedContextName;

  constructor(
    readonly sejourId: string,
    readonly dateDeFin: Date,
    readonly montant: number,
    readonly mailDeFacturation: Nullable<string>
  ) {
    super();
  }

  isPublic(): boolean {
    return false;
  }

  label(): string {
    return FinirUnSejourCommand.label;
  }

  protected serializedData(): CommandSerialized {
    return {
      tripId: this.sejourId,
      endDate: this.dateDeFin.toISOString(),
      amount: this.montant,
      invoiceEmail: this.mailDeFacturation ? this._apiCrypt.encrypt(this.mailDeFacturation) : null
    };
  }
}
