import { Sejour } from '@/deroulement-sejour/domain/sejour/sejour';
import { SejourRepository } from '@/deroulement-sejour/domain/sejour/Sejour-repository';
import { FinirUnSejourCommand } from '@/deroulement-sejour/use-cases/finir-sejour/command-handler/finir-un-sejour-command';
import { CommandHandler } from '@/shared/command-dispatcher/Command-handler';
import { EventPublisher } from '@/shared/event-publisher/internal-event-publisher';

export class FinirUnSejourCommandHandler implements CommandHandler {
  constructor(private readonly sejourRepository: SejourRepository, private readonly eventPublisher: EventPublisher) {}

  async handle(command: FinirUnSejourCommand): Promise<void> {
    const sejourSnapshot = await this.sejourRepository.getParIdOuErreur(command.sejourId);
    const sejour = Sejour.restore(sejourSnapshot);

    sejour.terminer(command.dateDeFin, command.mailDeFacturation, command.montant);

    await this.sejourRepository.save(sejour.snapshot);
    await this.eventPublisher.publish(sejour.raisedEvents);
  }
}
