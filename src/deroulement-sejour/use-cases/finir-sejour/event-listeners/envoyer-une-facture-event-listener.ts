import { ReservationLoader } from '@/deroulement-sejour/domain/reservation/Reservation-loader';
import { EnvoiFactureDemandeEvent } from '@/deroulement-sejour/domain/sejour/envoi-facture-demande-event';
import { HttpClient } from '@/shared/domain/gateways/Http-client';
import { EventListener } from '@/shared/event-publisher/Event-listener';

export class EnvoyerUneFactureEventListener implements EventListener {
  readonly label = EnvoyerUneFactureEventListener.name;

  constructor(
    private readonly reservationLoader: ReservationLoader,
    private readonly facturationHttpClient: HttpClient
  ) {}

  async listen(event: EnvoiFactureDemandeEvent): Promise<void> {
    const mailDeFacturation = await this.getMailDeFacturation(event);

    await this.facturationHttpClient.post('/invoices/send', {
      invoiceEmail: mailDeFacturation,
      amount: 200
    });
  }

  private async getMailDeFacturation(event: EnvoiFactureDemandeEvent) {
    if (!event.mailDeFacturation) {
      const reservation = await this.reservationLoader.getParIdOuErreur(event.reservationId);
      return reservation.mailDeReservation;
    }

    return event.mailDeFacturation;
  }
}
