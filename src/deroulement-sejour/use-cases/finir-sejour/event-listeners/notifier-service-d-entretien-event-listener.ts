import { SejourTermineEvent } from '@/deroulement-sejour/domain/sejour/sejour-termine-event';
import { ApiLogger } from '@/shared/domain/gateways/Api-logger';
import { EventListener } from '@/shared/event-publisher/Event-listener';

export class NotifierServiceDEntretienEventListener implements EventListener {
  readonly label = NotifierServiceDEntretienEventListener.name;

  constructor(private readonly apiLogger: ApiLogger) {}

  async listen(_: SejourTermineEvent): Promise<void> {
    this.apiLogger.info("Service d'entretien notifié");
  }
}
