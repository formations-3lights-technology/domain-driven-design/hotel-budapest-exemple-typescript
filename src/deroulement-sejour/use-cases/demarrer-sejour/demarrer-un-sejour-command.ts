import { defaultConfiguration } from '@/deroulement-sejour/2-configuration/default-configuration';
import { Command } from '@/shared/command-dispatcher/Command';

type CommandSerialized = Readonly<{
  bookingId: string;
  startDate: string;
}>;

export class DemarrerUnSejourCommand extends Command<CommandSerialized> {
  static readonly label = 'StartTripCommand';
  protected readonly boundedContext = defaultConfiguration.boundedContextName;

  constructor(readonly reservationId: string, readonly dateDeDebut: Date) {
    super();
  }

  isPublic(): boolean {
    return false;
  }

  label(): string {
    return DemarrerUnSejourCommand.label;
  }

  protected serializedData(): CommandSerialized {
    return {
      bookingId: this.reservationId,
      startDate: this.dateDeDebut.toISOString()
    };
  }
}
