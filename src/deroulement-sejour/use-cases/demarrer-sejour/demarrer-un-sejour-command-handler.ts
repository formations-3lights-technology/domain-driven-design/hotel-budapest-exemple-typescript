import { ReservationLoader } from '@/deroulement-sejour/domain/reservation/Reservation-loader';
import { Sejour } from '@/deroulement-sejour/domain/sejour/sejour';
import { SejourRepository } from '@/deroulement-sejour/domain/sejour/Sejour-repository';
import { DemarrerUnSejourCommand } from '@/deroulement-sejour/use-cases/demarrer-sejour/demarrer-un-sejour-command';
import { CommandHandler } from '@/shared/command-dispatcher/Command-handler';
import { EventPublisher } from '@/shared/event-publisher/internal-event-publisher';

export class DemarrerUnSejourCommandHandler implements CommandHandler {
  constructor(
    private readonly sejourRepository: SejourRepository,
    private readonly reservationLoader: ReservationLoader,
    private readonly eventPublisher: EventPublisher
  ) {}

  async handle(command: DemarrerUnSejourCommand): Promise<void> {
    const reservation = await this.reservationLoader.getParIdOuErreur(command.reservationId);

    const sejour = Sejour.enregistrement(
      await this.sejourRepository.nextId(),
      reservation.id,
      reservation.prixParNuit,
      command.dateDeDebut,
      reservation.dateDeDebut,
      reservation.dateDeFin
    );

    await this.sejourRepository.save(sejour.snapshot);
    await this.eventPublisher.publish(sejour.raisedEvents);
  }
}
