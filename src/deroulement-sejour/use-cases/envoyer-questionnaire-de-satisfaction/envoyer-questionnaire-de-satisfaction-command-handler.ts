import { defaultConfiguration } from '@/deroulement-sejour/2-configuration/default-configuration';
import { MailSender } from '@/deroulement-sejour/domain/mail/Mail-sender';
import { ReservationLoader } from '@/deroulement-sejour/domain/reservation/Reservation-loader';
import { SejourSnapshot } from '@/deroulement-sejour/domain/sejour/sejour';
import { SejourRepository } from '@/deroulement-sejour/domain/sejour/Sejour-repository';
import { EnvoyerQuestionnaireDeSatisfactionCommand } from '@/deroulement-sejour/use-cases/envoyer-questionnaire-de-satisfaction/envoyer-questionnaire-de-satisfaction-command';
import { CommandHandler } from '@/shared/command-dispatcher/Command-handler';

export class EnvoyerQuestionnaireDeSatisfactionCommandHandler implements CommandHandler {
  constructor(
    private readonly sejourRepository: SejourRepository,
    private readonly reservationLoader: ReservationLoader,
    private readonly mailSender: MailSender
  ) {}

  async handle(command: EnvoyerQuestionnaireDeSatisfactionCommand): Promise<void> {
    const sejours = await this.sejourRepository.getParDateDeFin(command.date);

    await Promise.all(sejours.map((s) => this.envoyerQuestionnaire(s)));
  }

  private async envoyerQuestionnaire(sejour: SejourSnapshot): Promise<void> {
    const reservation = await this.reservationLoader.getParIdOuErreur(sejour.reservationId);
    await this.mailSender.send({
      from: defaultConfiguration.mail.relationClient,
      to: reservation.mailDeReservation,
      message: 'Questionnaire de satisfaction'
    });
  }
}
