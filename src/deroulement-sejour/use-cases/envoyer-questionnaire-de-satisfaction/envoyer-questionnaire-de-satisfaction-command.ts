import { defaultConfiguration } from '@/deroulement-sejour/2-configuration/default-configuration';
import { Command } from '@/shared/command-dispatcher/Command';

type CommandSerialized = Readonly<{
  date: string;
}>;

export class EnvoyerQuestionnaireDeSatisfactionCommand extends Command<CommandSerialized> {
  static readonly label = 'SendSatisfactionSurveyCommand';
  protected readonly boundedContext = defaultConfiguration.boundedContextName;

  constructor(readonly date: Date) {
    super();
  }

  isPublic(): boolean {
    return false;
  }

  label(): string {
    return EnvoyerQuestionnaireDeSatisfactionCommand.label;
  }

  protected serializedData(): CommandSerialized {
    return {
      date: this.date.toISOString()
    };
  }
}
