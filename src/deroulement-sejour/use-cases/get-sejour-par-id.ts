import { SejourSnapshot } from '@/deroulement-sejour/domain/sejour/sejour';
import { SejourRepository } from '@/deroulement-sejour/domain/sejour/Sejour-repository';

export class GetSejourParId {
  constructor(private readonly sejourRepository: SejourRepository) {}

  handle(id: string): Promise<SejourSnapshot> {
    return this.sejourRepository.getParIdOuErreur(id);
  }
}
