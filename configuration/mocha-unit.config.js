module.exports = {
  diff: true,
  extension: ['ts'],
  opts: false,
  package: './package.json',
  reporter: 'spec',
  slow: 75,
  timeout: 2000,
  sort: true,
  require: ['ts-node/register', 'configuration/mocha.setup.js'],
  spec: ['tests/**/unit/**/*.test.ts'],
  ui: 'bdd',
  exit: true
};
