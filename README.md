# Hotel Budapest Exemple TypeScript

Merci de remonter les éventuels bugs ou dysfonctionnements à l'adresse : [contact@3lights-technology.com](mailto:contact@3lights-technology.com?subject=%5BFormation%5D%5BDDD%5D%20Probl%C3%A8me%20sur%20le%20projet%20Hotel%20Budapest%20Exemple%20TypeScript)

## NVM

Le starter a un fichier `.nvmrc` permettant d'utiliser `nvm`.

Si `nvm` ne s'est pas mis automatiquement sur la bonne version, faire `nvm use` et suivre les instructions si la version de Node n'est pas présente sur l'ordinateur.

## Makefile

Le starter a un fichier `Makefile` permettant d'utiliser la commande `make`.

Pour connaitre les commandes disponibles, taper `make`. Il faut installer `make` au préalable.

NB : Les commandes les plus importantes peuvent être lancées avec `yarn`.

## Lancer le projet (via make)

1. Installer les dépendances : `make install`
2. Lancer le projet : `make start`
3. Vérifier que le projet est bien démarré :
    - `http://localhost:8001/health` doit afficher : `"status":"up"`
    - ou `http://localhost:8001/v1/ready` doit afficher : `"apiStatus":"up"`
4. S'il y a besoin d'une base de données, un Docker compose est présent avec un service Postgres configuré.
    - Pour le démarrer : `make start-dep`
      <br> ⚠️ il n'y a pas de volumes pour persister les données
    - Les identifiants utilisés pour configurer Postgres sont dans le fichier `.env.docker`

## Autres commandes (via make)

- Pour lancer les tests : `make test`
  <br> ⚠️ les tests qui sont ajoutés doivent suivre cette règle : `src/**/1-tests/**/*.test.ts`
- Pour linter le projet : `make fix`

# Projet

- Une collection Postman est à disposition dans le répertoire documentation
- Il existe les routes suivantes :
  - `GET /search/rooms/:startDate/:endDate/:capacity`
  - `POST /booking/rooms`
  - `GET /booking/rooms/:id`
  - `PUT /trips/check-in`
  - `PUT /trips/:id/check-out`
  - `GET /trips/:id`
  - `POST /invoices/send`
- Il existe les commandes suivantes :
  - `make cron-deroulement-sejour-sss`
- Pour chaque Bounded Context est associé à la racine sa documentation : `[NOM DU BOUNDED CONTEXT] - Documentation.html`
