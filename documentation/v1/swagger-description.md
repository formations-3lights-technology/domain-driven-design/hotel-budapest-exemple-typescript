[Access to changelog](/documentation/changelog)

This documentation aims to describe available routes of Hotel Budapest API.

On version is available:

- v1: [Swagger](/v1/swagger) or [Redoc](/v1/redoc)
